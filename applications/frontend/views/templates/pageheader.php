<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=250351148360375";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="container">

<header id="branding">
<div class="oneup">
		<div class="adleft">
			<?php if($ad_page_banner['ad_type'] == 'images'):?>
    			<?php if($ad_page_banner['ad_effects'] == 'single'):?>
    				<a href="#"><img src="<?php echo base_url()?>assets/uploads/ads/<?php echo $ad_page_banner['ad_image_file']?>" class="headerimage" alt="advertisement"></a>
    			<?php else:?>
    				<div id="ad_page_banner_pics" class="pics"> 
      				<?php foreach($ad_page_banner['ad_images'] as $image):?>
      					<img src="<?php echo base_url()?>assets/uploads/ads/<?php echo $image['ad_image_file']?>" class="rightadcontainer" width="728">
      				<?php endforeach;?>
    				</div>
    				
    				<script type="text/javascript">
    					<?php ?>
              $(document).ready(function(){
            		$('#ad_page_banner_pics').cycle('<?php echo $ad_page_banner['ad_effects'] == 'fade' ? 'fade' : 'blindX';?>');
              });
            </script>
    			<?php endif;?>
    		<?php elseif($ad_page_banner['ad_type'] == 'flash'):?>
    			<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="728" height="90" id="ad_page_banner" align="middle">
              <param name="movie" value="<?php echo base_url()?>assets/uploads/flash/<?php echo $ad_page_banner['ad_flash_file']?>"/>
              <!--[if !IE]>-->
              <object type="application/x-shockwave-flash" data="<?php echo base_url()?>assets/uploads/flash/<?php echo $ad_page_banner['ad_flash_file']?>" width="728" height="90">
                  <param name="movie" value="<?php echo base_url()?>assets/uploads/flash/<?php echo $ad_page_banner['ad_flash_file']?>"/>
              <!--<![endif]-->
                  <a href="http://www.adobe.com/go/getflash">
                      <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player"/>
                  </a>
              <!--[if !IE]>-->
              </object>
              <!--<![endif]-->
          </object>
    		<?php endif;?>
			
		</div>
        <div class="magz">
        	<a href="#"><img src="<?php echo base_url()?>assets/img/baypop_thumb.jpg" ></a>
            <ul>
            <li><a href="<?php echo base_url('/pages/view/Current-Issue')?>">CURRENT ISSUE</a></li>
            <li><a href="<?php echo base_url('/pages/view/Advertise')?>">ADVERTISE</a></li>
            <li><a href="<?php echo base_url('/pages/view/Contact')?>">CONTACT</a></li>
            </ul>
      </div>
        <div class="clearbox"></div>
   	</div>
<div class="logocontainerhide">
	<a href="index.html">
		<?php if(isset($section_logo)):?>
			<img src="<?php echo base_url()?>assets/uploads/logo/<?php echo $section_logo?>" class="headerimage" alt="logo">
		<?php else:?>
			<img src="<?php echo base_url()?>assets/img/baypop_logo.png" alt="logo" class="headerimage">
		<?php endif;?>
	</a><!-- end headerimage -->
</div>
	<div class="clearbox"></div>
	<div class="top-info">
    	<div class="datenow"><?php echo strtoupper(date('M d, Y'))?></div>
        <div class="socialboxtop"><div><a href="#"><img src="<?php echo base_url()?>assets/img/fb.png" alt="facebook" class="soci-ryt"></a> <a href="#"><img src="<?php echo base_url()?>assets/img/twitter.png" alt="twitter" class="soci-ryt"></a> <a href="#"><img src="<?php echo base_url()?>assets/img/pinterest.png" alt="pinterest" class="soci-ryt"></a> <a href="#"><img src="<?php echo base_url()?>assets/img/in.png" class="soci-ryt"></a><div class="clearbox"></div></div>
        
        	<div><!--div id="searchwrapper"><form action="">
				<input type="text" class="searchbox" name="s" value="" />
				<input type="image" src="<?php echo base_url()?>assets/img/submit_blank.gif" class="searchbox_submit" value="" />
				</form>
            </div--></div>
        </div>
        <div class="searchmid">
				<div class="logocontainer980">
					<a href="index.html">
						<?php if(isset($section_logo)):?>
        			<img src="<?php echo base_url()?>assets/uploads/logo/<?php echo $section_logo?>" class="headerimage" alt="logo">
        		<?php else:?>
        			<img src="<?php echo base_url()?>assets/img/baypop_logo.png" alt="logo" class="headerimage">
        		<?php endif;?>
					</a>
					<!-- end headerimage -->
				</div>
        </div>
        
     <div class="clearbox"></div>
    <!-- end of top-info -->
    </div>
    <div class="top-info768">
    	<div class="datenow"><?php echo strtoupper(date('M d, Y'))?></div>
        <div class="socialboxtop"><a href="#"><img src="<?php echo base_url()?>assets/img/fb.png" alt="facebook"></a> <a href="#"><img src="<?php echo base_url()?>assets/img/twitter.png"  alt="twitter"></a> <a href="#"><img src="<?php echo base_url()?>assets/img/pinterest.png"  alt="pinterest"></a> <a href="#"><img src="<?php echo base_url()?>assets/img/in.png"></a></div>
        <div class="searchmid">
        	<!--div id="searchwrapper"><form action="">
				<input type="text" class="searchbox" name="s" value="" />
				<input type="image" src="<?php echo base_url()?>assets/img/submit_blank.gif" class="searchbox_submit" value="" />
				</form>
            </div-->
        </div>
        
     <div class="clearbox"></div>	
    </div>
    <div class="hidebox"><div class="spacer"></div></div>
    <div class="navbar">
    <div class="navbar-inner" <?php echo (isset($section)) ? 'style="background-color: '.$section['menu_bar_color'].'"' : '';?>>
    <div class="container">
    	<!-- .btn-navbar is used as the toggle for collapsed navbar content -->
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </a>
        
        <div class="nav-collapse">
        <ul class="nav">
<!--        <li class="active">
        <a href="#">Home</a>
        </li>-->
        <li></li>
        <li><a href="<?php echo base_url()?>">HOME</a></li>
        
        <?php foreach($menus as $menu):?>
        	<?php if(count($menu['submenus']) == 0):?>
        	<li><a href="<?php echo $menu['page_url'] ?>"><?php echo strtoupper($menu['menu_name'])?></a></li>
        	<?php else:?>
        	<li class="dropdown">
              <a href="<?php echo $menu['page_url'] ?>" class="dropdown-toggle" data-toggle="dropdown"> <?php echo strtoupper($menu['menu_name'])?> <b class="caret"></b></a>
              <ul class="dropdown-menu">
              	<?php foreach($menu['submenus'] as $submenu):?>
              		<li><a href="<?php echo $submenu['page_url']?>"><?php echo strtoupper($submenu['menu_name'])?></a></li>
              	<?php endforeach;?>
              </ul>
          </li>	
        	<?php endif;?>
        <?php endforeach;?>
        
       
        
        <li class="hidesmall"><a href="<?php echo base_url('/pages/view/Current-Issue')?>">CURRENT ISSUE</a></li>
        <li class="hidesmall"><a href="<?php echo base_url('/pages/view/Advertise')?>">ADVERTISE</a></li>
        <li class="hidesmall"><a href="<?php echo base_url('/pages/view/Contact')?>">CONTACT</a></li>
        <li class="hidesmall"><a href="#"><img src="<?php echo base_url()?>assets/img/fb.png" alt="facebook"></a> <a href="#"><img src="<?php echo base_url()?>assets/img/twitter.png"  alt="twitter"></a> <a href="#"><img src="<?php echo base_url()?>assets/img/pinterest.png"  alt="pinterest"></a> <a href="#"><img src="<?php echo base_url()?>assets/img/in.png"></a></li>
        <li class="hidesmall"><!--div id="searchwrapper"><form action="">
				<input type="text" class="searchbox" name="s" value="" />
				<input type="image" src="<?php echo base_url()?>assets/img/submit_blank.gif" class="searchbox_submit" value="" />
				</form>
            </div--></li>
        <li></li>
        
        </ul>
        </div>
        <div class="clearbox"></div>
    </div>
    </div>
    </div>
    
</header>