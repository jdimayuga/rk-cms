<!DOCTYPE html>
	<!-- Update your html tag to include the itemscope and itemtype attributes -->
<html itemscope itemtype="http://schema.org/Article" lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title><?php echo $title?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- styles -->
    <link href="<?php echo base_url()?>assets/styles/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/styles/custom.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/styles/bootstrap-responsive.min.css" rel="stylesheet">


    <!-- HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <style type="text/css">
            .navbar .nav > li > a {
              font-size:18px;
              padding: 7px 27px 0;
            }
            
            .hidesmall  { display:none !important; }
			.hidebox  { display:none !important; }
            .top-info768 { display:none !important; }
			.logocontainerhide { display:none !important; }
            
            .logocontainer {
            height: auto;
            margin: 0 auto;
            padding: 10px 0;
            width: 465px;
			}
            
            .galsmall { margin-left: -4px; } 
            .galsmall img { padding-left: 5px; 
            }
            
            	.socialboxtop {
                    width:180px;
				}
  		</style>
    <![endif]-->
    
<!--[if gte IE 9]>
  <style type="text/css"> 
    .grad {
       filter: none;
    }
            .hidesmall  { display:none !important; }
			.hidebox  { display:none !important; }
            .top-info768 { display:none !important; }
			.logocontainerhide { display:none !important; }
  </style>
<![endif]-->



<!-- Add the following three tags inside head -->
<meta itemprop="name" content="Baypop">
<meta itemprop="description" content="This would be a description of the content your users are sharing">
<script type="text/javascript">
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "ur-4dc0ce7-920a-17bf-4e98-4fcfec284a05"}); </script>

<script src="<?=base_url()?>assets/js/jquery-1.7.2.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery.cycle.all.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/pinit.js"></script>

</script>
</head>
<body onLoad="MM_preloadImages('<?php echo base_url()?>assets/img/view.jpg')">