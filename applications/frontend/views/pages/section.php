<div class="row">
	<div class="span8">
		<?php if(!empty($articles)):?>
			<?php $article = $articles[0];?>
			 <a href="<?php echo base_url('article/'.url_title($article['article_title']).'/'.$article['article_id'])?>"><img src="<?php echo base_url()?>assets/uploads/articles/thumb/<?php echo $article['article_image'] ?>_thumb<?php echo $article['article_image_ext']?>" class="headerimage"></a>
       <div class="spacer"></div>
       <div>
       	<a class="fstyl" href="<?php echo base_url("/section/".$section['section_name'].'/'.$section['section_id'])?>" style="color:<?php echo $section['menu_bar_color']?>"><?php echo strtoupper($section['section_name']) ?></a>
       	<div class="name_date"><?php echo $article['article_author']?></div>
        <div class="clearbox"></div>
       </div>
       <div class="spacer2"></div>
			 <h1><a href="<?php echo base_url('article/'.url_title($article['article_title']).'/'.$article['article_id'])?>"><?php echo $article['article_title']?></a></h1>
			 <p><?php echo $article['article_summary']?></p>
       <div class="dotborder"></div>
    <?php endif;?>
		<div class="row">
			<?php if(count($articles) > 1):?>
				<?php $article = $articles[1];?>
				<div class="span4">
        	<div class="spacer"></div>
    			<h1><a href="<?php echo base_url('article/'.url_title($article['article_title']).'/'.$article['article_id'])?>"><?php echo $article['article_title']?></a></h1>
          <div class="spacer3"></div>
          <a href="<?php echo base_url('article/'.url_title($article['article_title']).'/'.$article['article_id'])?>"><img src="<?php echo base_url()?>assets/uploads/articles/thumb_2/<?php echo $article['article_image'] ?>_thumb<?php echo $article['article_image_ext']?>"></a>
          <div class="spacer2"></div>
          <p><?php echo $article['article_summary']?></p>
        </div>
     <?php endif;?>
     <?php if(count($articles) > 2):?>
				<?php $article = $articles[2];?>
				<div class="span4">
        	<div class="spacer"></div>
    			<h1><a href="<?php echo base_url('article/'.url_title($article['article_title']).'/'.$article['article_id'])?>"><?php echo $article['article_title']?></a></h1>
          <div class="spacer3"></div>
          <a href="<?php echo base_url('article/'.url_title($article['article_title']).'/'.$article['article_id'])?>"><img src="<?php echo base_url()?>assets/uploads/articles/thumb_2/<?php echo $article['article_image'] ?>_thumb<?php echo $article['article_image_ext']?>"></a>
          <div class="spacer2"></div>
          <p><?php echo $article['article_summary']?></p>
        </div>
     <?php endif;?>
     
		 <div class="span8"><div class="dotborder"></div></div>
     <div class="span8"><div class="spacer"></div></div>

    <!-- end of ROW -->
    </div>
    
   <?php if(count($articles) > 3):?>
   <?php for($i=3; $i<count($articles); $i++):?>  
   	<?php $article = $articles[$i];?>
    <div class="row">
    	<div class="span2">
				<p>
					<a href="<?php echo base_url('article/'.url_title($article['article_title']).'/'.$article['article_id'])?>">
						<img src="<?php echo base_url()?>assets/uploads/articles/thumb_3/<?php echo $article['article_image'] ?>_thumb<?php echo $article['article_image_ext']?>">
					</a>
				</p>
				<div class="spacer"></div>     	
			</div>
     	<div class="span6">
     		<h2><a href="<?php echo base_url('article/'.url_title($article['article_title']).'/'.$article['article_id'])?>"><?php echo $article['article_title']?></a></h2>
     		<div class="spacer"></div>
     		<p><?php echo $article['article_summary']?></p>
     	</div>	
    </div>
    <div class="span8"><div class="spacer"></div></div>
    <?php endfor;?>
   <?php endif;?>
   <!-- end of span8 -->
    </div>
    
    
    <div class="span4">
    	<div class="cntr"> 
    		<?php if($ad_side_bottom['ad_type'] == 'images'):?>
        		<?php if($ad_side_bottom['ad_effects'] == 'single'):?>
        			<a href="#"><img src="<?php echo base_url()?>assets/uploads/ads/<?php echo $ad_side_bottom['ad_image_file']?>" class="rightadcontainer"/></a>
        		<?php else:?>
        			<div id="ad_side_bottom_pics" class="pics"> 
        				<?php foreach($ad_side_bottom['ad_images'] as $image):?>
        					<img src="<?php echo base_url()?>assets/uploads/ads/<?php echo $image['ad_image_file']?>" class="rightadcontainer" width="300">
        				<?php endforeach;?>
      				</div>
      				
      				<script type="text/javascript">
      					<?php ?>
                $(document).ready(function(){
              		$('#ad_side_bottom_pics').cycle('<?php echo $ad_side_bottom['ad_effects'] == 'fade' ? 'fade' : 'blindX';?>');
                });
              </script>
        		<?php endif;?>
        	<?php elseif($ad_side_bottom['ad_type'] == 'flash'):?>
        		<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="300" height="300" id="ad_side_bottom" align="middle">
                <param name="movie" value="<?php echo base_url()?>assets/uploads/flash/<?php echo $ad_side_bottom['ad_flash_file']?>"/>
                <!--[if !IE]>-->
                <object type="application/x-shockwave-flash" data="<?php echo base_url()?>assets/uploads/flash/<?php echo $ad_side_bottom['ad_flash_file']?>" width="300" height="300">
                    <param name="movie" value="<?php echo base_url()?>assets/uploads/flash/<?php echo $ad_side_bottom['ad_flash_file']?>"/>
                <!--<![endif]-->
                    <a href="http://www.adobe.com/go/getflash">
                        <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player"/>
                    </a>
                <!--[if !IE]>-->
                </object>
                <!--<![endif]-->
            </object>
        	<?php endif;?>
    	</div>
      <div class="spacer"></div><div class="spacer"></div>	
       	<?php if(!empty($random_sections) && !empty($random_sections[0]['article'])):?>
       	  <?php $random_section = $random_sections[0];?>
       	  <?php $section_article = $random_section['article'];?>
         	<a class="fstyl" href="<?php echo base_url('/section/'.$random_section['section_slug'].'/'.$random_section['section_id'])?>" style="color:<?php echo $random_section['menu_bar_color']?>"><?php echo strtoupper($random_section['section_name'])?></a>
          <div class="spacer"></div>
  				<h1><a href="<?php echo base_url('article/'.url_title($section_article['article_title']).'/'.$section_article['article_id'])?>"><?php echo $section_article['article_title']?></a></h1>
          <div class="spacer3"></div>
          <a href="<?php echo base_url('article/'.url_title($section_article['article_title']).'/'.$section_article['article_id'])?>">
          	<img src="<?php echo base_url()?>assets/uploads/articles/thumb_2/<?php echo $section_article['article_image'] ?>_thumb<?php echo $section_article['article_image_ext']?>">
          </a>
          <div class="spacer2"></div>
    			<p><?php echo $section_article['article_summary']; ?></p>
        <?php endif;?>
        
        <div class="spacer2"></div>
        <a class="fstyl" href="<?php echo base_url('/galleries')?>">BAYPOP GALLERIES</a></br> 
        <div class="spacer2"></div>
        
    	<?php for($i=0; $i<count($gallery); $i++):?>
    		<?php if($i%2 == 0):?>
    			<div class="galsmall">
    		<?php endif;?>
    		<?php $item = $gallery[$i];?>
    			<span class="pull-left" style="margin-bottom:5px;"><a href="<?php echo base_url('gallery/'.$item['gallery_id'])?>" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image<?php echo $item['gallery_id']?>','','<?php echo base_url()?>assets/img/view.jpg',1)"><img src="<?php echo base_url()?>assets/uploads/gallery_covers/small/<?php echo $item['gallery_cover_small']?>_thumb<?php echo $item['gallery_cover_small_ext']?>" class="gal-image" name="Image<?php echo $item['gallery_id']?>" border="0"></a></span>
    		  <?php if($i==count($gallery)-1):?>
    				<a href="<?php echo base_url('gallery')?>" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image31','','<?php echo base_url()?>assets/img/view.jpg',1)"><img src="<?php echo base_url()?>assets/img/moregallery.png" class="gal-image" name="Image31" border="0"></a>
    		  <?php endif;?>
    		 <?php if($i%2 != 0 || ($i==count($gallery)-1 && $i%2 == 0)):?>
    			<div class="spacer2"></div>
    			</div>
    		<?php endif;?>
   	  <?php endfor;?>
   	  
   	  
  		<div class="spacer"></div>
  		
  		<?php for($i=1; $i<count($random_sections); $i++):?>
  		  <?php if(!empty($random_sections[$i]['article'])):?>
  		  	<?php $random_section = $random_sections[$i];?>
       	  <?php $section_article = $random_section['article'];?>
    			<a class="fstyl" href="<?php echo base_url('/section/'.$random_section['section_slug'].'/'.$random_section['section_id'])?>" style="color:<?php echo $random_section['menu_bar_color']?>"><?php echo strtoupper($random_section['section_name'])?></a>
          <div class="spacer2"></div>
    			<h1><a href="<?php echo base_url('article/'.url_title($section_article['article_title']).'/'.$section_article['article_id'])?>"><?php echo $section_article['article_title']?></a></h1>
        	<div class="spacer3"></div>
          <a href="<?php echo base_url('article/'.url_title($section_article['article_title']).'/'.$section_article['article_id'])?>">
          	<img src="<?php echo base_url()?>assets/uploads/articles/thumb_2/<?php echo $section_article['article_image'] ?>_thumb<?php echo $section_article['article_image_ext']?>">
          </a>
        	<div class="spacer2"></div>
        	<p><?php echo $section_article['article_summary']; ?></p>
        	<div class="spacer"></div>
      	<?php endif;?>
      <?php endfor;?>
		
		</div>
    <!-- end of main "row" -->
  </div>