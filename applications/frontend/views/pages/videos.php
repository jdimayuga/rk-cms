<div class="row">
		
    <div class="span8">
    <?php if($video != null):?>
    		<div class="video"><?php echo $video['video_embed']?></div>
        <div class="spacer"></div><div class="spacer"></div>
        <h2><?php echo $video['video_title']?></h2>
        <div class="spacer3"></div>
        <p><?php echo $video['video_caption']?></p>
        <div class="fltleft"><span class='st_sharethis_large' displayText='ShareThis'></span>
        <span class='st_facebook_large' displayText='Facebook'></span>
        <span class='st_twitter_large' displayText='Tweet'></span>
        <span class='st_linkedin_large' displayText='LinkedIn'></span>
        <span class='st_pinterest_large' displayText='Pinterest'></span>
        <span class='st_email_large' displayText='Email'></span></div>
        <div class="clearbox"></div>
    <!-- end of span8 -->
    <?php endif;?>
    </div>
    
    
    <div class="span4">
      <div class="cntr"> 
      	<?php if($ad_side_bottom['ad_type'] == 'images'):?>
        		<?php if($ad_side_bottom['ad_effects'] == 'single'):?>
        			<a href="#"><img src="<?php echo base_url()?>assets/uploads/ads/<?php echo $ad_side_bottom['ad_image_file']?>" class="rightadcontainer"/></a>
        		<?php else:?>
        			<div id="ad_side_bottom_pics" class="pics"> 
        				<?php foreach($ad_side_bottom['ad_images'] as $image):?>
        					<img src="<?php echo base_url()?>assets/uploads/ads/<?php echo $image['ad_image_file']?>" class="rightadcontainer" width="300">
        				<?php endforeach;?>
      				</div>
      				
      				<script type="text/javascript">
      					<?php ?>
                $(document).ready(function(){
              		$('#ad_side_bottom_pics').cycle('<?php echo $ad_side_bottom['ad_effects'] == 'fade' ? 'fade' : 'blindX';?>');
                });
              </script>
        		<?php endif;?>
        	<?php elseif($ad_side_bottom['ad_type'] == 'flash'):?>
        		<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="300" height="300" id="ad_side_bottom" align="middle">
                <param name="movie" value="<?php echo base_url()?>assets/uploads/flash/<?php echo $ad_side_bottom['ad_flash_file']?>"/>
                <!--[if !IE]>-->
                <object type="application/x-shockwave-flash" data="<?php echo base_url()?>assets/uploads/flash/<?php echo $ad_side_bottom['ad_flash_file']?>" width="300" height="300">
                    <param name="movie" value="<?php echo base_url()?>assets/uploads/flash/<?php echo $ad_side_bottom['ad_flash_file']?>"/>
                <!--<![endif]-->
                    <a href="http://www.adobe.com/go/getflash">
                        <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player"/>
                    </a>
                <!--[if !IE]>-->
                </object>
                <!--<![endif]-->
            </object>
        	<?php endif;?>
      </div>
      
    </div>
    <!-- end of main "row" -->
  </div>
 
  <div class="spacer"></div><div class="spacer4"></div>
  
  <div class="row">
  		<?php 
        $list_size = count($videos);
        $half_point = floor($list_size/2);
        $list1 = array_slice($videos, 0, $half_point);
        $list2 = array_slice($videos, $half_point);
      ?>
      
      <div class="span6">
      		<?php for($i=0; $i<count($list1); $i++):?>
      			<?php $video = $list1[$i];?>
            <div class="row">
                <!--div class="span3"><a href="<?php echo base_url('/videos/view/'.$video['video_id'])?>"><img src="img/video_thumb.jpg"></a></div-->
                <div class="span3">
                <h2><a href="<?php echo base_url('/videos/view/'.$video['video_id'])?>"><?php echo $video['video_title']?></a></h2>
                <div class="spacer2"></div>
                <!--p class="smaller2">3:30 min | Sep-18-2012</p-->
                <p class="smaller1"><?php echo $video['video_caption']?></p>
                </div>
            </div>
            <div class="spacer4"></div>
            
          <?php endfor;?>
      </div>
      
      <div class="span6">
         <?php for($i=0; $i<count($list2); $i++):?>
      			<?php $video = $list2[$i];?>
            <div class="row">
                <!--div class="span3"><a href="<?php echo base_url('/videos/view/'.$video['video_id'])?>"><img src="img/video_thumb.jpg"></a></div-->
                <div class="span3">
                <h2><a href="<?php echo base_url('/videos/view/'.$video['video_id'])?>"><?php echo $video['video_title']?></a></h2>
                <div class="spacer2"></div>
                <!--p class="smaller2">3:30 min | Sep-18-2012</p-->
                <p class="smaller1"><?php echo $video['video_caption']?></p>
                </div>
            </div>
            <div class="spacer4"></div>
            
          <?php endfor;?>
      
      </div>
    <!-- end of main "row" -->
  </div>