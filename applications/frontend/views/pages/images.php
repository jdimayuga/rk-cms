  
  <div class="row">
  <div class="span7"><div class="gallery-title"><h3><?php echo $gallery['gallery_name']?></h3></div></div>
<div class="span5">
  		<div class="return">see:</div>
        <div class="returnlink"><a href="<?php echo base_url('/article/'.url_title($gallery['article_title']).'/'.$gallery['gallery_article_id'])?>"><?php echo $gallery['article_title'] ?></a></div>
        <div class="clearbox"></div>
  </div>
  <!-- end of main "row" -->
  </div>
  
  <div class="spacer"></div>
  
  <div class="row">
    <div class="span8 offset2">
    	<div class="gallery-container">
    	<div class="carousel slide" id="myCarousel">
    			<div class="carousel-inner">
            <?php foreach($images as $i=>$image):?>
    					<div class="item <?php echo $i==0 ? 'active' : ''?>">
    						<img alt="" src="<?php echo base_url().'assets/uploads/gallery/'.$image['image_file']?>"/>
                <div class="carousel-caption">
                  <p><?php echo $image['image_caption']?></p>
                  <div class="fltleft">
                  	<span class='st_sharethis_large' displayText='ShareThis'></span>
										<span class='st_facebook_large' displayText='Facebook'></span>
                    <span class='st_twitter_large' displayText='Tweet'></span>
                    <span class='st_linkedin_large' displayText='LinkedIn'></span>
                    <span class='st_pinterest_large' displayText='Pinterest'></span>
                    <span class='st_email_large' displayText='Email'></span></div>
                </div>
              </div>
    			  <?php endforeach;?>
            </div>
            <a data-slide="prev" href="#myCarousel" class="left carousel-control2">&lsaquo;</a>
            <a data-slide="next" href="#myCarousel" class="right carousel-control2">&rsaquo;</a>
          </div>
          </div>
    </div>
   </div>