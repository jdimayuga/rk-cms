 <div class="row">
    
    <?php 
      $list_size = count($galleries);
      $half_point = floor($list_size/2);
      $list1 = array_slice($galleries, 0, $half_point);
      $list2 = array_slice($galleries, $half_point);
    ?>
    <div class="span6">
    <?php foreach($list1 as $gallery):?>
		<div class="row">
          <div class="span3">
          	<a href="<?php echo base_url('/galleries/view/'.$gallery['gallery_id']);?>"><img  src="<?php echo base_url()?>assets/uploads/gallery_covers/small/<?php echo $gallery['gallery_cover_small']?>_thumb<?php echo $gallery['gallery_cover_small_ext']?>"></a>
          </div>
          <div class="span3">
          <div class="galsec">
          <h2><a href="<?php echo base_url('/galleries/view/'.$gallery['gallery_id']);?>"><?php echo $gallery['gallery_name']?></a></h2>
          <div class="spacer3"></div>
          <p class="smaller1"><?php echo $gallery['gallery_desc']?> <a href="<?php echo base_url('/galleries/view/'.$gallery['gallery_id']);?>">more</a></p>
          </div>
          </div>
      </div>
      <div class="spacer4"></div>
    <?php endforeach;?>
	</div>

	<div class="span6">
    <?php foreach($list2 as $gallery):?>
		<div class="row">
          <div class="span3">
          	<a href="<?php echo base_url('/galleries/view/'.$gallery['gallery_id']);?>"><img src="<?php echo base_url()?>assets/uploads/gallery_covers/small/<?php echo $gallery['gallery_cover_small']?>_thumb<?php echo $gallery['gallery_cover_small_ext']?>"></a>
          </div>
          <div class="span3">
          <div class="galsec">
          <h2><a href="<?php echo base_url('/galleries/view/'.$gallery['gallery_id']);?>"><?php echo $gallery['gallery_name']?></a></h2>
          <div class="spacer3"></div>
          <p class="smaller1"><?php echo $gallery['gallery_desc']?> <a href="<?php echo base_url('/galleries/view/'.$gallery['gallery_id']);?>">more</a></p>
          </div>
          </div>
      </div>
      <div class="spacer4"></div>
    <?php endforeach;?>
	</div>
  <!-- end of main "row" -->
</div>
<?php echo $this->pagination->create_links(); ?>