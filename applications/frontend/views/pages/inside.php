<div class="row">
    <div class="span8">
    <h1><?php echo $article['article_title']?></h1>
    <div class="spacer"></div>
    <img src="<?php echo base_url()?>assets/uploads/articles/thumb/<?php echo $article['article_image'] ?>_thumb<?php echo $article['article_image_ext']?>" class="headerimage">
        
    <div class="spacer"></div>
				<div>
        	<div class="name_name"><?php echo $article['article_author']?></div>
       	  <div class="name_date">posted <?php echo $article['article_date']?></div>
          <div class="clearbox"></div>
        </div>
        <div class="spacer"></div>

      <p><?php echo $article['article_content']?></p>
    	<div class="dotborder"></div>
      <div class="fltleft">
      	<span class='st_sharethis_large' displayText='ShareThis'></span>
        <span class='st_facebook_large' displayText='Facebook'></span>
        <span class='st_twitter_large' displayText='Tweet'></span>
        <span class='st_linkedin_large' displayText='LinkedIn'></span>
        <span class='st_pinterest_large' displayText='Pinterest'></span>
        <span class='st_email_large' displayText='Email'></span>
      </div>
      <div class="clearbox"></div>
      <div class="dotborder"></div>
      <?php $tags = explode(',',$article['article_tags']);?>
      <div class="smaller2">TAGS:
      <?php foreach($tags as $i=>$tag):?>
        <?php if($i != 0 && trim($tag) != ''):?>,<?php endif;?>
      	<a href="<?php echo base_url('articles/tag/'.$tag)?>"><?php echo $tag?></a>
      <?php endforeach?>
       </div>
         
      <div class="spacer"></div> 
      
      <div class="fb-like" data-send="false" data-show-faces="true" data-font="arial"></div>
      <div class="spacer3"></div>
      <div class="fb-comments" data-href="http://baypop.com" data-num-posts="2"></div>
		
		<!-- end of span8 -->
    </div>
    
    
    <div class="span4">
    	<div class="cntr"> 
    		<?php if($ad_side_bottom['ad_type'] == 'images'):?>
        		<?php if($ad_side_bottom['ad_effects'] == 'single'):?>
        			<a href="#"><img src="<?php echo base_url()?>assets/uploads/ads/<?php echo $ad_side_bottom['ad_image_file']?>" class="rightadcontainer"/></a>
        		<?php else:?>
        			<div id="ad_side_bottom_pics" class="pics"> 
        				<?php foreach($ad_side_bottom['ad_images'] as $image):?>
        					<img src="<?php echo base_url()?>assets/uploads/ads/<?php echo $image['ad_image_file']?>" class="rightadcontainer" width="300">
        				<?php endforeach;?>
      				</div>
      				
      				<script type="text/javascript">
      					<?php ?>
                $(document).ready(function(){
              		$('#ad_side_bottom_pics').cycle('<?php echo $ad_side_bottom['ad_effects'] == 'fade' ? 'fade' : 'blindX';?>');
                });
              </script>
        		<?php endif;?>
        	<?php elseif($ad_side_bottom['ad_type'] == 'flash'):?>
        		<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="300" height="300" id="ad_side_bottom" align="middle">
                <param name="movie" value="<?php echo base_url()?>assets/uploads/flash/<?php echo $ad_side_bottom['ad_flash_file']?>"/>
                <!--[if !IE]>-->
                <object type="application/x-shockwave-flash" data="<?php echo base_url()?>assets/uploads/flash/<?php echo $ad_side_bottom['ad_flash_file']?>" width="300" height="300">
                    <param name="movie" value="<?php echo base_url()?>assets/uploads/flash/<?php echo $ad_side_bottom['ad_flash_file']?>"/>
                <!--<![endif]-->
                    <a href="http://www.adobe.com/go/getflash">
                        <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player"/>
                    </a>
                <!--[if !IE]>-->
                </object>
                <!--<![endif]-->
            </object>
        	<?php endif;?>
   	  </div>
      <div class="spacer"></div><div class="spacer"></div>	
      <a class="fstyl" href="<?php echo base_url('/section/'.$article['section_slug'].'/'.$article['article_section'])?>">MORE <?php echo strtoupper($article['section_name'])?></a>
      
      <div class="spacer"></div>
      <?php if(!empty($related)):?>
      <?php $related_article = $related[0];?>
  			<h1><a href="<?php echo base_url('/article/'.url_title($related_article['article_title']).'/'.$related_article['article_id'])?>"><?php echo $related_article['article_title']?></a></h1>
        <div class="spacer3"></div>
        <a href="<?php echo base_url('/article/'.url_title($related_article['article_title']).'/'.$related_article['article_id'])?>">
        	<img src="<?php echo base_url()?>assets/uploads/articles/thumb_2/<?php echo $related_article['article_image'] ?>_thumb<?php echo $related_article['article_image_ext']?>">
        </a>
        <div class="spacer2"></div>
    		<p><?php echo $related_article['article_summary'] ?></p>
  		<?php endif;?>
        
        <div class="spacer2"></div>
        <a class="fstyl" href="<?php echo base_url('/galleries')?>">BAYPOP GALLERIES</a></br> 
        <div class="spacer2"></div>
        
       <?php for($i=0; $i<count($gallery); $i++):?>
    		<?php if($i%2 == 0):?>
    			<div class="galsmall">
    		<?php endif;?>
    		<?php $item = $gallery[$i];?>
    			<span class="pull-left" style="margin-bottom:5px;"><a href="<?php echo base_url('gallery/'.$item['gallery_id'])?>" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image<?php echo $item['gallery_id']?>','','<?php echo base_url()?>assets/img/view.jpg',1)"><img src="<?php echo base_url()?>assets/uploads/gallery_covers/small/<?php echo $item['gallery_cover_small']?>_thumb<?php echo $item['gallery_cover_small_ext']?>" class="gal-image" name="Image<?php echo $item['gallery_id']?>" border="0"></a></span>
    		  <?php if($i==count($gallery)-1):?>
    				<a href="<?php echo base_url('gallery')?>" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image31','','<?php echo base_url()?>assets/img/view.jpg',1)"><img src="<?php echo base_url()?>assets/img/moregallery.png" class="gal-image" name="Image31" border="0"></a>
    		  <?php endif;?>
    		 <?php if($i%2 != 0 || ($i==count($gallery)-1 && $i%2 == 0)):?>
    			<div class="spacer2"></div>
    			</div>
    		<?php endif;?>
   	  <?php endfor;?>
        
    	<div class="spacer"></div>
  		
  		<?php for($i=1; $i < count($related); $i++):?>
  			<?php $related_article = $related[$i]; ?>
    		<h1><a href="<?php echo base_url('/article/'.url_title($related_article['article_title']).'/'.$related_article['article_id'])?>"><?php echo $related_article['article_title']?></a></h1>
        <div class="spacer3"></div>
        <a href="<?php echo base_url('/article/'.url_title($related_article['article_title']).'/'.$related_article['article_id'])?>">
        	<img src="<?php echo base_url()?>assets/uploads/articles/thumb_2/<?php echo $related_article['article_image'] ?>_thumb<?php echo $related_article['article_image_ext']?>">
        </a>
        <div class="spacer2"></div>
        <p><?php echo $related_article['article_summary'] ?></p>
        <div class="spacer"></div>
      <?php endfor;?>
      
    	<div class="spacer"></div>
    	
    	<a class="fstyl" href="<?php echo base_url('videos')?>">BAYPOP TV</a>
        <div class="spacer2"></div>
        <div class="video">
        	<?php echo $video['video_embed']?>
        </div>
    </div>
    <!-- end of main "row" -->
  </div>