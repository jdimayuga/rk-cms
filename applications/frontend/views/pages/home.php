<div class="row">
    
    <div class="span8">
    	<div class="carousel slide" id="myCarousel">
        <div class="carousel-inner">
        	<?php foreach($featured as $i=>$feature):?>
        		<div class="item <?php echo $i == 0 ? 'active' : ''?>">
            	<img alt="" src="<?php echo base_url()?>assets/uploads/articles/thumb/<?php echo $feature['article_image'] ?>_thumb<?php echo $feature['article_image_ext']?>"/>
            	<div class="carousel-caption">
        				<div>
                	<a class="fstyl" href="<?php echo base_url('/section/'.$feature['section_slug'].'/'.$feature['article_section'])?>" style="color:<?php echo $feature['menu_bar_color']?>"><?php echo strtoupper($feature['section_name'])?></a>
               	  <div class="name_date">BY <?php echo $feature['article_author']?></div>
                  <div class="clearbox"></div>
                </div>
                <div class="spacer"></div>
        				<h1><a href="<?php echo base_url('/article/'.url_title($feature['article_title']).'/'.$feature['article_id'])?>"><?php echo $feature['article_title'] ?></a></h1>
        				<p><?php echo $feature['article_summary']?></p>
              </div>
            </div>
        	<?php endforeach;?>
        </div>
        <a data-slide="prev" href="#myCarousel" class="left carousel-control">‹</a>
        <a data-slide="next" href="#myCarousel" class="right carousel-control">›</a>
      </div>
    	<div class="dotborder"></div>
        
      <div class="row">
        <?php foreach($top_featured as $i=>$feature):?>
      		<div class="span4">
          	<div class="spacer"></div>
  					<h1><a href="<?php echo base_url('/article/'.url_title($feature['article_title']).'/'.$feature['article_id'])?>"><?php echo $feature['article_title']?></a></h1>
          	<div class="spacer3"></div>
          	<a href="<?php echo base_url('/article/'.url_title($feature['article_title']).'/'.$feature['article_id'])?>"><img src="<?php echo base_url()?>assets/uploads/articles/thumb_2/<?php echo $feature['article_image'] ?>_thumb<?php echo $feature['article_image_ext']?>"></a>
          	<div class="spacer2"></div>
          	<p><?php echo $feature['article_summary']?></p>
          </div>
      	<?php endforeach;?>
      	
        <div class="span8"><div class="dotborder"></div></div>
        
        <?php foreach($bottom_featured as $feature):?>
          <div class="span4">
            <div class="spacer"></div>
            <a class="fstyl" href="<?php echo base_url('/section/'.$feature['section_slug'].'/'.$feature['article_section'])?>" style="color:<?php echo $feature['menu_bar_color']?>" ><?php echo strtoupper($feature['section_name']) ?></a>
            <div class="spacer"></div>
    				<h1><a href="<?php echo base_url('/article/'.url_title($feature['article_title']).'/'.$feature['article_id'])?>"><?php echo $feature['article_title']?></a></h1>
            <div class="spacer3"></div>
            <a href="<?php echo base_url('/article/'.url_title($feature['article_title']).'/'.$feature['article_id'])?>"><img src="<?php echo base_url()?>assets/uploads/articles/thumb_2/<?php echo $feature['article_image'] ?>_thumb<?php echo $feature['article_image_ext']?>"></a>
            <div class="spacer2"></div>
            <p><?php echo $feature['article_summary']?></p>
    
            <div class="relatedpar">
            <span class="related">RELATED STORIES</span>
            <div class="spacer2"></div>
            <?php foreach($feature['related'] as $related):?>
           		<p><a href="<?php echo base_url('/article/'.url_title($related['article_title']).'/'.$related['article_id'])?>"><?php echo $related['article_title']?></a></p>
            <?php endforeach;?>
            </div>
          </div>
        <?php endforeach;?>
        
             
		<div class="span8"><div class="dotborder"></div></div>

    <!-- end of ROW -->
    </div>
        
    <div class="row">
      <?php foreach($featured_sections as $i=>$feature):?>
    		<?php if($i%2 == 0): ?>
    			<div class="span4">
        		<div class="row">
        <?php endif;?>	
              <div class="span2">
              	<div class="smallstory">
                	<a class="fstyl" href="<?php echo base_url('/section/'.$feature['section_slug'].'/'.$feature['section_id'])?>" style="color:<?php echo $feature['menu_bar_color']?>" ><?php echo strtoupper($feature['section_name']) ?></a><br>
                	<?php foreach($feature['articles'] as $j=>$article):?>
                		<?php if($j==0):?>
                			<a href="<?php echo base_url('/article/'.url_title($article['article_title']).'/'.$article['article_id'])?>"><img src="<?php echo base_url()?>assets/uploads/articles/thumb_3/<?php echo $article['article_image'] ?>_thumb<?php echo $article['article_image_ext']?>"></a>
                  		<div class="spacer"></div>
                		<?php endif;?>
                		<p><a href="<?php echo base_url('/article/'.url_title($article['article_title']).'/'.$article['article_id'])?>"><?php echo $article['article_title']?></a></p>
                	<?php endforeach;?>
                </div>
              </div>
        <?php if($i%2 != 0): ?>
    				</div>
    			</div>
    		<?php endif;?>
    	<?php endforeach;?>
    </div>
    
    <div class="spacer"></div>
    <div class="dotborder"></div>
    <a class="fstyl" href="<?php echo base_url('/videos')?>">BAYPOP TV</a>
    <div class="spacer2"></div>
    <div class="video">
    	<?php echo $video['video_embed']?>
    </div>
		<div class="spacer"></div>
    
    <!-- end of span8 -->
    </div>
    
    
    <div class="span4">
    	
    	<div class="cntr"> 
    	
    		<?php if($ad_side_top['ad_type'] == 'images'):?>
    			<?php if($ad_side_top['ad_effects'] == 'single'):?>
    				<a href="#"><img src="<?php echo base_url()?>assets/uploads/ads/<?php echo $ad_side_top['ad_image_file']?>" class="rightadcontainer"></a>
    			<?php else:?>
    				<div id="ad_side_top_pics" class="pics"> 
      				<?php foreach($ad_side_top['ad_images'] as $image):?>
      					<img src="<?php echo base_url()?>assets/uploads/ads/<?php echo $image['ad_image_file']?>" class="rightadcontainer" width="300">
      				<?php endforeach;?>
    				</div>
    				
    				<script type="text/javascript">
    					<?php ?>
              $(document).ready(function(){
            		$('#ad_side_top_pics').cycle('<?php echo $ad_side_top['ad_effects'] == 'fade' ? 'fade' : 'blindX';?>');
              });
            </script>
    			<?php endif;?>
    		<?php elseif($ad_side_top['ad_type'] == 'flash'):?>
    			<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="300" height="300" id="ad_side_top" align="middle">
              <param name="movie" value="<?php echo base_url()?>assets/uploads/flash/<?php echo $ad_side_top['ad_flash_file']?>"/>
              <!--[if !IE]>-->
              <object type="application/x-shockwave-flash" data="<?php echo base_url()?>assets/uploads/flash/<?php echo $ad_side_top['ad_flash_file']?>" width="300" height="300">
                  <param name="movie" value="<?php echo base_url()?>assets/uploads/flash/<?php echo $ad_side_top['ad_flash_file']?>"/>
              <!--<![endif]-->
                  <a href="http://www.adobe.com/go/getflash">
                      <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player"/>
                  </a>
              <!--[if !IE]>-->
              </object>
              <!--<![endif]-->
          </object>
    		<?php endif;?>
   	  </div>
      
      <div class="spacer2"></div>
      
      <a class="fstyl" href="<?php echo base_url('/galleries')?>">BAYPOP GALLERIES</a></br>
      
      <div class="spacer2"></div>
      <?php if(count($gallery) > 0):?>
      	<?php $main_gallery = $gallery[0]; ?>
      	<a href="<?php echo base_url('/galleries/'.$main_gallery['gallery_id'])?>">
      		<img src="<?php echo base_url()?>assets/uploads/gallery_covers/large/<?php echo $main_gallery['gallery_cover_large']?>_thumb<?php echo $main_gallery['gallery_cover_large_ext'] ?>" class="gal-main-image">
      	</a>
      <?php endif;?>
      
      <div class="spacer2"></div>
 
			<?php for($i=1; $i<count($gallery); $i++):?>
    		<?php if($i%2 != 0):?>
    			<div class="galsmall">
    		<?php endif;?>
    		<?php $item = $gallery[$i];?>
    			<span class="pull-left" style="margin-bottom:5px;"><a href="<?php echo base_url('gallery/'.$item['gallery_id'])?>" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image<?php echo $item['gallery_id']?>','','<?php echo base_url()?>assets/img/view.jpg',1)"><img src="<?php echo base_url()?>assets/uploads/gallery_covers/small/<?php echo $item['gallery_cover_small']?>_thumb<?php echo $item['gallery_cover_small_ext']?>" class="gal-image" name="Image<?php echo $item['gallery_id']?>" border="0"></a></span>
    		  <?php if($i==count($gallery)-1):?>
    				<a href="<?php echo base_url('gallery')?>" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image31','','<?php echo base_url()?>assets/img/view.jpg',1)"><img src="<?php echo base_url()?>assets/img/moregallery.png" class="gal-image" name="Image31" border="0"></a>
    		  <?php endif;?>
    		 <?php if($i%2 == 0 || ($i==count($gallery)-1 && $i%2 != 0)):?>
    			<div class="spacer2"></div>
    			</div>
    		<?php endif;?>
   	  <?php endfor;?>
   	  
		  <div class="spacer"></div><div class="spacer"></div>
        <div class="cntr"> 
        	<?php if($ad_side_bottom['ad_type'] == 'images'):?>
        		<?php if($ad_side_bottom['ad_effects'] == 'single'):?>
        			<a href="#"><img src="<?php echo base_url()?>assets/uploads/ads/<?php echo $ad_side_bottom['ad_image_file']?>" class="rightadcontainer"/></a>
        		<?php else:?>
        			<div id="ad_side_bottom_pics" class="pics"> 
        				<?php foreach($ad_side_bottom['ad_images'] as $image):?>
        					<img src="<?php echo base_url()?>assets/uploads/ads/<?php echo $image['ad_image_file']?>" class="rightadcontainer" width="300">
        				<?php endforeach;?>
      				</div>
      				
      				<script type="text/javascript">
      					<?php ?>
                $(document).ready(function(){
              		$('#ad_side_bottom_pics').cycle('<?php echo $ad_side_bottom['ad_effects'] == 'fade' ? 'fade' : 'blindX';?>');
                });
              </script>
        		<?php endif;?>
        	<?php elseif($ad_side_bottom['ad_type'] == 'flash'):?>
        		<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="300" height="300" id="ad_side_bottom" align="middle">
                <param name="movie" value="<?php echo base_url()?>assets/uploads/flash/<?php echo $ad_side_bottom['ad_flash_file']?>"/>
                <!--[if !IE]>-->
                <object type="application/x-shockwave-flash" data="<?php echo base_url()?>assets/uploads/flash/<?php echo $ad_side_bottom['ad_flash_file']?>" width="300" height="300">
                    <param name="movie" value="<?php echo base_url()?>assets/uploads/flash/<?php echo $ad_side_bottom['ad_flash_file']?>"/>
                <!--<![endif]-->
                    <a href="http://www.adobe.com/go/getflash">
                        <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player"/>
                    </a>
                <!--[if !IE]>-->
                </object>
                <!--<![endif]-->
            </object>
        	<?php endif;?>
      	</div>
        
        <div class="spacer"></div><div class="spacer"></div>
    	<a class="fstyl" href="#">PHOTO OF THE DAY</a>
        <div class="spacer2"></div>
        <?php if(!empty($photo)):?>
        	<img src="<?php echo base_url()?>assets/uploads/gallery/<?php echo $photo['image_file']?>" border="0" class="gal-main-image">
        <?php endif;?>
    </div>
    <!-- end of main "row" -->
  </div>
 
<div class="spacer"></div><div class="spacer"></div><div class="spacer"></div>


<<script type="text/javascript">
$(document).ready(function(){
    $('.carousel').carousel({
        interval: 5000
        });
});
</script>
