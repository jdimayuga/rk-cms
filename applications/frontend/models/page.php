<?php
class Page extends CI_Model {
  
  public function getPageBySlug($page_slug)
  {
    $this->db->from('pages')->where('page_slug', $page_slug);
    $query = $this->db->get();
    return $query->row_array();
  }
  

}
?>