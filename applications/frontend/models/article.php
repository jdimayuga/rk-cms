<?php
class Article extends CI_Model
{
  public function getFeaturedArticles()
  {
    $this->db->select('articles.*', FALSE);
    $this->db->select('section.section_name AS section_name', FALSE);
    $this->db->select('section.section_slug AS section_slug', FALSE);
    $this->db->select('section.menu_bar_color AS menu_bar_color', FALSE);
    $this->db->from('articles')->where('articles.featured','1')->order_by('article_order', 'asc');
    $this->db->join('section', 'articles.article_section = section.section_id', 'left');
    $query = $this->db->get();
    return $query->result_array();
  }

  public function getTopFeaturedArticles()
  {
    $this->db->select('articles.*', FALSE);
    $this->db->select('section.section_name AS section_name', FALSE);
    $this->db->select('section.section_slug AS section_slug', FALSE);
    $this->db->from('articles')->where('articles.sub_featured_1','1')->order_by('article_order', 'asc');
    $this->db->join('section', 'articles.article_section = section.section_id', 'left');
    $query = $this->db->get();
    return $query->result_array();
  }

  public function getBottomFeaturedArticles()
  {
    $this->db->select('articles.*', FALSE);
    $this->db->select('section.section_name AS section_name', FALSE);
    $this->db->select('section.section_slug AS section_slug', FALSE);
    $this->db->select('section.menu_bar_color AS menu_bar_color', FALSE);
    $this->db->from('articles')->where('articles.sub_featured_2','1')->order_by('article_order', 'asc');
    $this->db->join('section', 'articles.article_section = section.section_id', 'left');
    $query = $this->db->get();
    return $query->result_array();
  }
  
  public function getRelatedArticles($section, $article_id) 
  {
    $this->db->from('articles')->where('article_section',$section)->limit(3)->order_by('article_order', 'asc');
    if($article_id != null) {
      $this->db->where_not_in('article_id', $article_id);
    }
    $query = $this->db->get();
    return $query->result_array();
  }
  
  public function getLatestArticles($section) {
    $this->db->from('articles')->where('article_section',$section)->order_by('article_date', 'desc');
    $query = $this->db->get();
    return $query->result_array();
  }
  
  public function getLatestArticle($section) {
    $this->db->from('articles')->where('article_section',$section)->order_by('article_date', 'desc')->limit(1);
    $query = $this->db->get();
    return $query->row_array();
  }
  
  public function getArticleById($article_id)
  {
    $this->db->select('articles.*', FALSE);
    $this->db->select('section.section_name AS section_name', FALSE);
    $this->db->select('section.section_slug AS section_slug', FALSE);
    $this->db->from('articles')->where('article_id', $article_id);
    $this->db->join('section', 'articles.article_section = section.section_id', 'left');
    $query = $this->db->get();
    return $query->row_array();
  }

}
?>