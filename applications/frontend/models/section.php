<?php
class Section extends CI_Model
{
  public function getFeaturedSections()
  {
    $this->db->from('section')->where('featured','1');
    $query = $this->db->get();
    return $query->result_array();
  }


  public function getSectionById($section_id)
  {
    $this->db->from('section')->where('section_id', $section_id);
    $query = $this->db->get();
    return $query->row_array();
  }
  
  public function getRandomSections() {
    $this->db->from('section')->order_by('section_id','random')->limit(4);
    $query = $this->db->get();
    return $query->result_array();
  }
}
?>