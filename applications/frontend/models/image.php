<?php
class Image extends CI_Model
{
  public function getRandomPhotoOfTheDay()
  {
    $this->db->from('images')->order_by('image_id', 'random')->limit(1);
    $query = $this->db->get();
    return $query->row_array();
  }
  
  public function getImages($gallery_id)
  {
    $this->db->from('images')->where('gallery_id', $gallery_id)->order_by('image_id', 'asc');
    $query = $this->db->get();
    return $query->result_array();
  }
}  
?>