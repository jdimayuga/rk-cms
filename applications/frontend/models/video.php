<?php
class Video extends CI_Model
{
  public function getLatestVideo()
  {
    $this->db->from('videos')->order_by('video_id','desc')->limit(1);
    $query = $this->db->get();
    return $query->row_array();
  }

  public function getVideo($video_id)
  {
    $this->db->from('videos')->where('video_id',$video_id);
    $query = $this->db->get();
    return $query->row_array();
  }

  public function getVideos($video_id)
  {
    $this->db->from('videos')->order_by('video_id','desc');
    if($video_id != null) {
      $this->db->where_not_in('video_id', $video_id);
    }
    $query = $this->db->get();
    return $query->result_array();
  }
}
?>