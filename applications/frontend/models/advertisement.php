<?php
class Advertisement extends CI_Model
{
  public function getAdvertisementImageByArea($area)
  {
    $this->db->select('advertisements.*', FALSE);
    $this->db->select('ad_images.ad_image_file AS ad_image_file', FALSE);
    $this->db->select('ad_images.ad_thumb_file AS ad_thumb_file', FALSE);
    $this->db->from('advertisements')
             ->where('ad_area', $area)
             ->limit(1);
    $this->db->join('ad_images', 'ad_images.ad_id = advertisements.ad_id', 'left');
    $query = $this->db->get();
    return $query->row_array();
  }
  
  public function getAdImages($ad_id){
    $this->db->from('ad_images')->where('ad_id', $ad_id);
             
    $query = $this->db->get();
    return $query->result_array();
  }
}
?>