<?php
class Menu extends CI_Model
{
  
  public function getMenus()
  {
    $this->db->from('menus')->order_by('menu_sort', 'asc');
    $this->db->where('menu_parent','0');
    $query = $this->db->get();
    $result = $query->result_array();
    
    for($i=0; $i<count($result); $i++) {
      $menu_id = $result[$i]['menu_id'];
      
      $this->db->from('menus')->order_by('menu_sort', 'asc');
      $this->db->where('menu_parent', $menu_id);
      $query = $this->db->get();

      $result[$i]['submenus'] = $query->result_array();
    }
    
    return $result;
  }
}
?>