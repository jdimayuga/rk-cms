<?php
class Gallery extends CI_Model
{
  public function getLatestGallery()
  {
    $this->db->from('gallery')->order_by('gallery_id', 'desc')->limit(6);
    $query = $this->db->get();
    return $query->result_array();
  }

  public function getAllGallery($num, $offset)
  {
    $this->db->from('gallery')->order_by('gallery_id', 'desc');
    if(!empty($num) && !empty($offset))
    {
      $this->db->limit($num, $offset);
    }
    else if(!empty($num)){
      $this->db->limit($num);
    }
    $query = $this->db->get();
    return $query->result_array();
  }

  public function getGalleryById($gallery_id)
  {
    $this->db->select('gallery.*', FALSE);
    $this->db->select('articles.article_title AS article_title', FALSE);
    $this->db->from('gallery')->where('gallery_id', $gallery_id);
    $this->db->join('articles', 'articles.article_id = gallery.gallery_article_id', 'left');
    $query = $this->db->get();
    return $query->row_array();
     
  }

  public function getGalleryCount()
  {
    return $this->db->count_all('gallery');
  }

  public function getRelatedGallery($section_id)
  {
    $this->db->from('gallery')->where('gallery_section', $section_id)->order_by('gallery_id', 'desc')->limit(3);
    $query = $this->db->get();
    return $query->result_array();
  }
}
?>