<?php
class Pages extends CI_Controller {
  public function __construct()
  {
    parent::__construct();
    $this->load->model('menu','',TRUE);
    $this->load->model('article','',TRUE);
    $this->load->model('section','',TRUE);
    $this->load->model('video','',TRUE);
    $this->load->model('advertisement','',TRUE);
    $this->load->model('gallery','',TRUE);
    $this->load->model('image','',TRUE);
    $this->load->model('page','',TRUE);
    $this->load->helper('form','file','url');
    $this->load->library('form_validation');
  }

  public function index()
  {
    $data['title'] = 'BayPop';
    $data['featured'] = $this->article->getFeaturedArticles();
    $data['top_featured'] = $this->article->getTopFeaturedArticles();
    $data['bottom_featured'] = $this->article->getBottomFeaturedArticles();
    for($i=0; $i<count($data['bottom_featured']); $i++) {
      $article = $data['bottom_featured'][$i];
      $data['bottom_featured'][$i]['related'] = $this->article->getRelatedArticles($article['article_section'], $article['article_id']);  
    }
    $data['featured_sections'] = $this->section->getFeaturedSections();
    for($i=0; $i<count($data['featured_sections']); $i++) {
      $section = $data['featured_sections'][$i];
      $data['featured_sections'][$i]['articles'] = $this->article->getRelatedArticles($section['section_id'], null);  
    }
    $data['video'] = $this->video->getLatestVideo();
    $data['ad_side_top'] = $this->advertisement->getAdvertisementImageByArea('side_top');
    $data['ad_side_top']['ad_images'] = $this->advertisement->getAdImages($data['ad_side_top']['ad_id']);
    
    $data['ad_side_bottom'] = $this->advertisement->getAdvertisementImageByArea('side_bottom');
    $data['ad_side_bottom']['ad_images'] = $this->advertisement->getAdImages($data['ad_side_bottom']['ad_id']);
    
    $data['gallery'] = $this->gallery->getLatestGallery();
    $data['photo'] = $this->image->getRandomPhotoOfTheDay();
    $data = array_merge($data, $this->get_reference_data());
    $this->show_view($data,'pages/home');
  }
  
  public function view(){
    $page =  $this->page->getPageBySlug($this->uri->segment(3));
    $data['title'] = 'BayPop - '.$page['page_name'];
    $data['page'] = $page;
    $data['ad_side_bottom'] = $this->advertisement->getAdvertisementImageByArea('side_bottom');
    $data = array_merge($data, $this->get_reference_data());
    $this->show_view($data,'pages/static');
  }

  private function get_reference_data() {
    $data['menus'] = $this->menu->getMenus();
    $data['ad_page_banner'] = $this->advertisement->getAdvertisementImageByArea('page_banner');
    $data['ad_page_banner']['ad_images'] = $this->advertisement->getAdImages($data['ad_page_banner']['ad_id']);
    
    return $data;
  }

  private function show_view($data, $page)
  {
    $this->load->view('templates/header', $data);
    $this->load->view('templates/pageheader', $data);
    if($page != null) {
      $this->load->view($page, $data);
    }
    $this->load->view('templates/footer',$data);
  }

}