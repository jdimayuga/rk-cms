<?php
class Sections extends CI_Controller {
  public function __construct()
  {
    parent::__construct();
    $this->load->model('menu','',TRUE);
    $this->load->model('article','',TRUE);
    $this->load->model('section','',TRUE);
    $this->load->model('video','',TRUE);
    $this->load->model('advertisement','',TRUE);
    $this->load->model('gallery','',TRUE);
    $this->load->model('image','',TRUE);
    $this->load->helper('form','file','url');
    $this->load->library('form_validation');
  }


  public function view()
  {
    $section =  $this->section->getSectionById($this->uri->segment(3));
    $data['title'] = "Baypop - ".$section['section_name'];
    $data['section'] = $section;
    $data['ad_side_bottom'] = $this->advertisement->getAdvertisementImageByArea('side_bottom');
    $data['ad_side_bottom']['ad_images'] = $this->advertisement->getAdImages($data['ad_side_bottom']['ad_id']);
    
    $data['articles'] = $this->article->getLatestArticles($section['section_id']);
    $data['random_sections'] = $this->section->getRandomSections();
    for($i=0; $i<count($data['random_sections']); $i++) {
      $random_section = $data['random_sections'][$i];
      $data['random_sections'][$i]['article'] = $this->article->getLatestArticle($random_section['section_id']);
    }
    $data['gallery'] = $this->gallery->getRelatedGallery($section['section_id']);
    $data['section_logo'] = $section['section_logo'];
    $data = array_merge($data, $this->get_reference_data());
    $this->show_view($data);
  }

  private function get_reference_data() {
    $data['menus'] = $this->menu->getMenus();
    $data['ad_page_banner'] = $this->advertisement->getAdvertisementImageByArea('page_banner');
    $data['ad_page_banner']['ad_images'] = $this->advertisement->getAdImages($data['ad_page_banner']['ad_id']);
    return $data;
  }

  private function show_view($data)
  {
    $this->load->view('templates/header', $data);
    $this->load->view('templates/pageheader', $data);
    $this->load->view('pages/section',$data);
    $this->load->view('templates/footer',$data);
  }

}