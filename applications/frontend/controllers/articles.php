<?php
class Articles extends CI_Controller {
  public function __construct()
  {
    parent::__construct();
    $this->load->model('menu','',TRUE);
    $this->load->model('article','',TRUE);
    $this->load->model('section','',TRUE);
    $this->load->model('video','',TRUE);
    $this->load->model('advertisement','',TRUE);
    $this->load->model('gallery','',TRUE);
    $this->load->model('image','',TRUE);
    $this->load->helper('form','file','url');
    $this->load->library('form_validation');
  }

  public function view()
  {
    $article =  $this->article->getArticleById($this->uri->segment(3));
    $date = DateTime::createFromFormat('Y-m-d H:i:s', $article['article_date']);
    //$date = new DateTime($article['article_date']);
    $article['article_date'] = $date->format('F d, Y');
    $data['title'] = 'BayPop - '.$article['article_title'];
    $data['article'] = $article;
    $section =  $this->section->getSectionById($article['article_section']);
    $data['section'] = $section;
    $data['ad_side_bottom'] = $this->advertisement->getAdvertisementImageByArea('side_bottom');
    $data['ad_side_bottom']['ad_images'] = $this->advertisement->getAdImages($data['ad_side_bottom']['ad_id']);
    
    $data['related'] = $this->article->getRelatedArticles($article['article_section'], $article['article_id']);
    $data['video'] = $this->video->getLatestVideo();
    $data['gallery'] = $this->gallery->getRelatedGallery($article['article_section']);
    $data = array_merge($data, $this->get_reference_data());
    $this->show_view($data);
  }


  private function get_reference_data() {
    $data['menus'] = $this->menu->getMenus();
    $data['ad_page_banner'] = $this->advertisement->getAdvertisementImageByArea('page_banner');
    $data['ad_page_banner']['ad_images'] = $this->advertisement->getAdImages($data['ad_page_banner']['ad_id']);
    return $data;
  }

  private function show_view($data)
  {
    $this->load->view('templates/header', $data);
    $this->load->view('templates/pageheader', $data);
    $this->load->view('pages/inside',$data);
    $this->load->view('templates/footer',$data);
  }
}

?>