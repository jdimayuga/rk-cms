<?php
class Videos extends CI_Controller {
  public function __construct()
  {
    parent::__construct();
    $this->load->model('menu','',TRUE);
    $this->load->model('article','',TRUE);
    $this->load->model('section','',TRUE);
    $this->load->model('video','',TRUE);
    $this->load->model('advertisement','',TRUE);
    $this->load->model('gallery','',TRUE);
    $this->load->model('image','',TRUE);
    $this->load->helper('form','file','url');
    $this->load->library('form_validation');
  }

  public function index()
  {
    $data['title'] = 'BayPop - Video';
    $data['video'] = $this->video->getLatestVideo();
    $data['videos'] = $this->video->getVideos($data['video']['video_id']);
    $data = array_merge($data, $this->get_reference_data());
    $data['ad_side_bottom'] = $this->advertisement->getAdvertisementImageByArea('side_bottom');
    $data['ad_side_bottom']['ad_images'] = $this->advertisement->getAdImages($data['ad_side_bottom']['ad_id']);
    $this->show_view($data);
  }

  public function view()
  {
    $data['title'] = 'BayPop - Video';
    $data['video'] = $this->video->getVideo($this->uri->segment(3));
    $data['videos'] = $this->video->getVideos($this->uri->segment(3));
    $data = array_merge($data, $this->get_reference_data());
    $data['ad_side_bottom'] = $this->advertisement->getAdvertisementImageByArea('side_bottom');
    $data['ad_side_bottom']['ad_images'] = $this->advertisement->getAdImages($data['ad_side_bottom']['ad_id']);
    $this->show_view($data);
  }

  private function get_reference_data() {
    $data['menus'] = $this->menu->getMenus();
    $data['ad_page_banner'] = $this->advertisement->getAdvertisementImageByArea('page_banner');
    $data['ad_page_banner']['ad_images'] = $this->advertisement->getAdImages($data['ad_page_banner']['ad_id']);
    return $data;
  }
  
  private function show_view($data)
  {
    $this->load->view('templates/header', $data);
    $this->load->view('templates/pageheader', $data);
    $this->load->view('pages/videos',$data);
    $this->load->view('templates/footer',$data);
  }


}
?>