<?php
class Galleries extends CI_Controller {
  public function __construct()
  {
    parent::__construct();
    $this->load->model('menu','',TRUE);
    $this->load->model('article','',TRUE);
    $this->load->model('section','',TRUE);
    $this->load->model('video','',TRUE);
    $this->load->model('advertisement','',TRUE);
    $this->load->model('gallery','',TRUE);
    $this->load->model('image','',TRUE);
    $this->load->helper('form','file','url');
    $this->load->library('form_validation');
    $this->load->library('pagination');
  }

  public function index()
  {
    $data['title'] = 'BayPop - Gallery';
    $data['galleries'] = $this->gallery->getAllGallery(10,$this->uri->segment(3));
    $data = array_merge($data, $this->get_reference_data());
    $this->prepare_pagination();
    $this->show_view($data);
  }
  
  public function view()
  {
    $data['title'] = 'BayPop - Gallery';
    $data['images'] = $this->image->getImages($this->uri->segment(3));
    $data['gallery'] = $this->gallery->getGalleryById($this->uri->segment(3));
    $data = array_merge($data, $this->get_reference_data());
    $this->show_images_view($data);
  }

  private function get_reference_data() {
    $data['menus'] = $this->menu->getMenus();
    $data['ad_page_banner'] = $this->advertisement->getAdvertisementImageByArea('page_banner');
    $data['ad_page_banner']['ad_images'] = $this->advertisement->getAdImages($data['ad_page_banner']['ad_id']);
    return $data;
  }

  private function prepare_pagination()
  {
    $config['base_url'] = base_url('/galleries/index');
    $config['total_rows'] = $this->gallery->getGalleryCount();
    $config['per_page'] = 10;
    $config['full_tag_open'] = '<div align="center" style="margin-top:10px;" class="navigation">';
    $config['full_tag_close'] = '</div>';
    $config['first_link'] = FALSE;
    $config['last_link'] = FALSE;
    $config['next_link'] = 'Next';
    $config['next_tag_open'] = '';
    $config['next_tag_close'] = '';
    $config['prev_link'] = 'Prev';
    $config['prev_tag_open'] = '';
    $config['prev_tag_close'] = '';
    $config['cur_tag_open'] = '<span>';
    $config['cur_tag_close'] = '</span>';
    $config['num_tag_open'] = '';
    $config['num_tag_close'] = '';

    $this->pagination->initialize($config);
  }

  private function show_view($data)
  {
    $this->load->view('templates/header', $data);
    $this->load->view('templates/pageheader', $data);
    $this->load->view('pages/gallery',$data);
    $this->load->view('templates/footer',$data);
  }
  
  private function show_images_view($data)
  {
    $this->load->view('templates/header', $data);
    $this->load->view('templates/pageheader', $data);
    $this->load->view('pages/images',$data);
    $this->load->view('templates/footer',$data);
  }
}