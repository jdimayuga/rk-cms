<?php
class Settings extends Secure_Controller {

  var $page_items = 5;

  public function __construct()
  {
    parent::__construct();
    $this->load->model('web_settings','',TRUE);
    $this->load->model('section','',TRUE);
    $this->load->helper('form');
    $this->load->library('parser');
    $this->load->library('form_validation');
    $this->load->library('pagination');
  }

  public function index()
  {
    $data['title'] = 'RK CMS: Settings';
    $data = array_merge($data, $this->_get_reference_data());
    $sectionData['settings'] = $this->web_settings->getOne();
    $data['settings_section'] = $this->parser->parse('pages/settings/general', $sectionData, TRUE);
    $this->_show_view($data);
  }

  public function save()
  {
    $id = $this->web_settings->save();
    $this->session->set_flashdata('success', TRUE);
    $this->session->set_flashdata('successMessage', 'Successfully saved settings.');
    redirect('settings', 'refresh');
  }

  public function sections()
  {
    $data['title'] = 'RK CMS: Settings - Sections';
    $data = array_merge($data, $this->_get_reference_data());
    $data['active'] = 'sections';
    
    $sectionData['sections'] = $this->section->getAll($this->page_items, $this->uri->segment(3));
    $this->_prepare_pagination('settings/sections', $this->section->count());
    $data['settings_section'] = $this->parser->parse('pages/settings/sections', $sectionData, TRUE);
    $this->_show_view($data);
  }

  public function section_view()
  {
    $section_id = trim($this->input->get('id'));
    $result = $this->section->getById($section_id);
    header('Content-Type: application/json',true);
    echo json_encode($result);
  }
   
  public function save_section()
  {
    $this->form_validation->set_rules('name','Section Name','required');
    if($this->form_validation->run() === TRUE)
    {
      $id = $this->section->save();
      $this->section->uploadImage($id);
      
      $this->session->set_flashdata('success', TRUE);
      $this->session->set_flashdata('successMessage', 'Successfully saved section.');
      redirect('/settings/sections', 'refresh');
    }
    else{
      $this->sections();
    }

  }

  public function delete_section()
  {
    $result = $this->section->delete();
    if($result)
    {
      $this->session->set_flashdata('success', TRUE);
      $this->session->set_flashdata('successMessage', 'Successfully deleted section.');
    }
    redirect('/settings/sections', 'refresh');
  }
  
  public function toggle_featured_section()
  {
    $section_id = trim($this->input->get('id'));
    $status = trim($this->input->get('status'));
    
    if($status == '1' && $this->section->count_featured() == 4){
      $result['status'] = false;
    }
    else {
      $this->section->toggle_featured($section_id, $status);
      $result['status'] = true;
    }
    header('Content-Type: application/json',true);
    echo json_encode($result);
  }
  
  public function home()
  {
    $data['title'] = 'RK CMS: Settings - Home';
    $data = array_merge($data, $this->_get_reference_data());
    $data['active'] = 'home';
    $sectionData = array();
    $data['settings_section'] = $this->parser->parse('pages/settings/home', $sectionData, TRUE);
    $this->_show_view($data);
  }

  private function _get_reference_data() {
    if($this->session->userdata('logged_in')) {
      $session_data = $this->session->userdata('logged_in');
      $data['username'] = $session_data['username'];
    }
    $data['pageActive'] = 'settings';
    return $data;
  }

  private function _show_view($data)
  {
    $this->load->view('templates/header', $data);
    $this->load->view('templates/pageheader', $data);
    $this->load->view('pages/settings',$data);
    $this->load->view('templates/footer',$data);
  }

  private function _prepare_pagination($url, $count)
  {
    $config['base_url'] = base_url().$url;
    $config['total_rows'] = $count;
    $config['per_page'] = $this->page_items;
    $config['full_tag_open'] = '<ul>';
    $config['full_tag_close'] = '</ul>';
    $config['first_link'] = FALSE;
    $config['last_link'] = FALSE;
    $config['next_link'] = 'Next';
    $config['next_tag_open'] = '<li>';
    $config['next_tag_close'] = '</li>';
    $config['prev_link'] = 'Prev';
    $config['prev_tag_open'] = '<li>';
    $config['prev_tag_close'] = '</li>';
    $config['cur_tag_open'] = '<li class="active"><a href="#">';
    $config['cur_tag_close'] = '</a></li>';
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';

    $this->pagination->initialize($config);
  }
}

?>