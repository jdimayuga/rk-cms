<?php
class Menus extends Secure_Controller {

  var $pageItems = 5;

  public function __construct()
  {
    parent::__construct();
    $this->load->model('menu','',TRUE);
    $this->load->helper('form','file','url');
    $this->load->library('form_validation');
    $this->load->library('pagination');
    // $this->output->enable_profiler(TRUE);
  }

  
 public function view()
  {
    $menuId = trim($this->input->get('id'));
    $result = $this->menu->getById($menuId);
    header('Content-Type: application/json',true);
    echo json_encode($result);
  }
  
  public function index()
  {
    $data['title'] = 'RK CMS: Menu Builder';
    $data = array_merge($data, $this->getReferenceData());
    $this->preparePagination();
    $data['menus'] = $this->menu->getAll($this->pageItems,$this->uri->segment(3));
    $this->showView($data);
  }

  public function save()
  {
    $this->form_validation->set_rules('name','Menu Name','required');
    $this->form_validation->set_rules('path','Path','required');
    
    if($this->form_validation->run() === TRUE)
    {
      $id = $this->menu->save();
      $data['success'] = true;
      $data['successMessage'] = "Successfully saved menu: ".$this->input->post('name');

    }
    $data['title'] = 'RK CMS: Menu Builder';
    $data = array_merge($data, $this->getReferenceData());
    $this->preparePagination();
    $data['menus'] = $this->menu->getAll($this->pageItems,$this->uri->segment(3));
    $this->showView($data);
  }
  
  public function delete()
  {
    $result = $this->menu->delete();
    if($result)
    {
      $data['success'] = true;
      $data['successMessage'] = "Successfully deleted menu.";
    }
    $data['title'] = 'RK CMS: Menu Builder';
    $data = array_merge($data, $this->getReferenceData());
    $this->preparePagination();
    $data['menus'] = $this->menu->getAll($this->pageItems,$this->uri->segment(3));
    $this->showView($data);
  }

  private function preparePagination()
  {
    $config['base_url'] = base_url().'menus/index/';
    $config['total_rows'] = $this->menu->count();
    $config['per_page'] = $this->pageItems;
    $config['full_tag_open'] = '<ul>';
    $config['full_tag_close'] = '</ul>';
    $config['first_link'] = FALSE;
    $config['last_link'] = FALSE;
    $config['next_link'] = 'Next';
    $config['next_tag_open'] = '<li>';
    $config['next_tag_close'] = '</li>';
    $config['prev_link'] = 'Prev';
    $config['prev_tag_open'] = '<li>';
    $config['prev_tag_close'] = '</li>';
    $config['cur_tag_open'] = '<li class="active"><a href="#">';
    $config['cur_tag_close'] = '</a></li>';
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';

    $this->pagination->initialize($config);
  }

  private function getReferenceData() {
    if($this->session->userdata('logged_in')) {
      $session_data = $this->session->userdata('logged_in');
      $data['username'] = $session_data['username'];
    }
    $data['pageActive'] = 'menus';
    $data['parents'] = $this->menu->getAllParents();
    $data['parentSelected'] = 0;
    return $data;
  }

  private function showView($data)
  {
    $this->load->view('templates/header', $data);
    $this->load->view('templates/pageheader', $data);
    $this->load->view('pages/menus',$data);
    $this->load->view('templates/footer',$data);
  }

}
?>