<?php
class Downloads extends Secure_Controller {

  var $page_items = 5;

  public function __construct()
  {
    parent::__construct();
    $this->load->library('pagination');
    $this->load->model('form','',TRUE);
    
  }

  public function index()
  {
    $data['title'] = 'RK CMS: Downloads';
    $data = array_merge($data, $this->_get_reference_data());
    $data['forms'] = $this->form->getAll($this->page_items, $this->uri->segment(3));
    $this->_prepare_pagination('downloads/index');
    $this->_show_view($data);
  }

  private function _prepare_pagination($url)
  {
    $config['base_url'] = base_url().$url;
    $config['total_rows'] = $this->form->count();
    $config['per_page'] = $this->page_items;
    $config['full_tag_open'] = '<ul>';
    $config['full_tag_close'] = '</ul>';
    $config['first_link'] = FALSE;
    $config['last_link'] = FALSE;
    $config['next_link'] = 'Next';
    $config['next_tag_open'] = '<li>';
    $config['next_tag_close'] = '</li>';
    $config['prev_link'] = 'Prev';
    $config['prev_tag_open'] = '<li>';
    $config['prev_tag_close'] = '</li>';
    $config['cur_tag_open'] = '<li class="active"><a href="#">';
    $config['cur_tag_close'] = '</a></li>';
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';

    $this->pagination->initialize($config);
  }

  private function _get_reference_data() {
    if($this->session->userdata('logged_in')) {
      $session_data = $this->session->userdata('logged_in');
      $data['username'] = $session_data['username'];
    }
    $data['pageActive'] = 'downloads';
    return $data;
  }

  private function _show_view($data)
  {
    $this->load->view('templates/header', $data);
    $this->load->view('templates/pageheader', $data);
    $this->load->view('pages/downloads',$data);
    $this->load->view('templates/footer',$data);
  }
}

?>