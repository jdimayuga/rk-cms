<?php
class Pages extends Secure_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('page','',TRUE);
    $this->load->model('tag','',TRUE);
    $this->load->helper('form');
    $this->load->library('form_validation');
  }

  public function index() 
  {
    $data['title'] = 'RK CMS: Page Management';
    $data = array_merge($data, $this->getReferenceData());

    $this->showView($data);
  }

  public function view($slug)
  {
    $data['page'] = $this->page->getPageBySlug($slug);
    $data['title'] = 'RK CMS: '.$data['page']['page_name'];
    $data['active'] = $data['page']['page_id'];
    $data = array_merge($data, $this->getReferenceData());
    $this->showView($data);
  }
  
  public function update()
  {
    $data['title'] = 'RK CMS: Update Page';
    
    $this->form_validation->set_rules('title','Title','required|callback_validate_slug');
    if($this->form_validation->run() === TRUE)
    {
      $this->page->updatePage();
      $this->tag->saveTag();
      $data['page'] = $this->page->getPageById();
      $data['success'] = true;
      $data['successMessage'] = "Successfully updated Page: ".$data['page']['page_name'];
    }
    
    $data = array_merge($data, $this->getReferenceData());
    $data['page'] = $this->page->getPageById();
    $data['active'] = $this->input->post('id');
    
    
   
    $this->showView($data);
  }

  public function create()
  {
    $data['title'] = 'RK CMS: Create New Page';
    $data = array_merge($data, $this->getReferenceData());
     
    $this->form_validation->set_rules('title','Title','required|callback_validate_slug');
     
    if($this->form_validation->run() === TRUE)
    {
      $result = $this->page->createPage();
      $this->tag->saveTag();
      if($result)
      {
        $data['success'] = true;
        $data['successMessage'] = "Successfully added new page: ".$this->input->post('title');
      }
      // reload $data
    
      $data = array_merge($data, $this->getReferenceData());
    }
    $this->showView($data);
  }
  
  public function delete($pageId)
  {
    $data['title'] = 'RK CMS: Page Management';
    $result = $this->page->deletePage($pageId);
    if($result)
    {
      $data['success'] = true;
      $data['successMessage'] = "Successfully deleted page.";
    }
    $data = array_merge($data, $this->getReferenceData());
    $this->showView($data);
  }

  public function logout() {
    $this->session->unset_userdata('logged_in');
    //session_destroy();
    redirect(base_url('/'),'refresh');
  }

  public function validate_slug($str)
  {
    $result = $this->page->hasPageBySlug();
    if($result)
    {
      $this->form_validation->set_message('validate_slug', '%s field has already has the same title: '.$str);
      return FALSE;
    }
    return TRUE;
  }

  private function getReferenceData() {
    $data['pages'] = $this->page->getPages('dynamic');
    if($this->session->userdata('logged_in')) {
      $session_data = $this->session->userdata('logged_in');
      $data['username'] = $session_data['username'];
    }
    return $data;
  }
  
  private function showView($data)
  {
    $this->load->view('templates/header', $data);
    $this->load->view('templates/pageheader', $data);
    $this->load->view('pages/pages',$data);
    $this->load->view('templates/footer',$data);
  }
  
}
?>