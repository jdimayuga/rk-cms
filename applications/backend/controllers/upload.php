<?php
class Upload extends CI_Controller {

  protected $path_img_upload_folder;
  protected $path_img_thumb_upload_folder;
  protected $path_url_img_upload_folder;
  protected $path_url_img_thumb_upload_folder;

  protected $delete_img_url;

  function __construct() {
    parent::__construct();
    $this->load->helper(array('form', 'url'));
    $this->load->model('images','',TRUE);

    //Set relative Path with CI Constant
    $this->setPath_img_upload_folder("../assets/uploads/gallery/");
    $this->setPath_img_thumb_upload_folder("../assets/uploads/gallery/thumb/");
    
    //Delete img url
    $this->setDelete_img_url(base_url() . 'upload/deleteImage/');
    
    //Set url img with Base_url()
    $this->setPath_url_img_upload_folder(base_url() . "../assets/uploads/gallery/");
    $this->setPath_url_img_thumb_upload_folder(base_url() . "../assets/uploads/gallery/thumb/");
  }


  // Function called by the form
  public function upload_img() {
    
    header('Content-Type: application/json',true);
    
    //Format the name
    $name = $_FILES['userfile']['name'];

    // replace characters other than letters, numbers and . by _
    $name = preg_replace('/([^.a-z0-9]+)/i', '_', $name);
    
    $galleryId =  $this->input->post('galleryId');

    //Your upload directory, see CI user guide
    $config['upload_path'] = $this->getPath_img_upload_folder();

    $config['allowed_types'] = 'gif|jpg|png|JPG|GIF|PNG';
    $config['max_size'] = '2048';
    $config['file_name'] = $name;

    //Load the upload library
    $this->load->library('upload', $config);

    if ($this->do_upload()) {
      
      $data = $this->upload->data();
      
      //If you want to resize
      $config['new_image'] = $this->getPath_img_thumb_upload_folder();
      $config['image_library'] = 'gd2';
      $config['source_image'] = $data['full_path'];
      $config['create_thumb'] = TRUE;
      $config['maintain_ratio'] = TRUE;
      $config['width'] = 160;
      $config['height'] = 120;

      $this->load->library('image_lib', $config);

      $this->image_lib->resize();
  
      
      $fileExt = $data['file_ext'];
      $rawName = $data['raw_name'];
      $thumb = $rawName.'_thumb'.$fileExt;
  
      //save
      $imageId = $this->images->save($data['file_name'], $thumb, $galleryId);
      
      $info = new stdClass();
      $info->image_id = $imageId;
      $info->name = $data['file_name'];
      $info->size = $data['file_size'];
      $info->type = $data['file_type'];
      $info->url = $this->getPath_url_img_upload_folder() . $name;
      $info->thumbnail_url = $this->getPath_url_img_thumb_upload_folder() . $thumb; //I set this to original file since I did not create thumbs.  change to thumbnail directory if you do = $upload_path_url .'/thumbs' .$name
      $info->delete_url = $this->getDelete_img_url() . $name . '/'.$imageId;
      $info->delete_type = 'DELETE';

      //Return JSON data
      echo json_encode(array($info));

    } else {

      // the display_errors() function wraps error messages in <p> by default and these html chars don't parse in
      // default view on the forum so either set them to blank, or decide how you want them to display.  null is passed.
      $error = array('error' => $this->upload->display_errors('',''));

      echo json_encode(array($error));
    }


  }


  //Function for the upload : return true/false
  public function do_upload() {

    if (!$this->upload->do_upload()) {

      return false;
    } else {
      //$data = array('upload_data' => $this->upload->data());

      return true;
    }
  }


  //Function Delete image
  public function deleteImage() {

    //Get the name in the url
    $file = $this->uri->segment(3);
    $image_id = $this->uri->segment(4);
    
    $this->images->delete($image_id);

    $name =  pathinfo( $file, PATHINFO_FILENAME);
    $ext = $ext = pathinfo( $file, PATHINFO_EXTENSION);
    $thumbfile = $name.'_thumb.'.$ext;

    $success = unlink($this->getPath_img_upload_folder() . $file);
    $success_th = unlink($this->getPath_img_thumb_upload_folder() . $thumbfile);

    //info to see if it is doing what it is supposed to
    $info = new stdClass();
    $info->sucess = $success;
    $info->path = $this->getPath_url_img_upload_folder() . $file;
    $info->file = is_file($this->getPath_img_upload_folder() . $file);

    echo json_encode(array($info));

  }


  //Load the files
  public function get_files() {
    $galleryId = $this->uri->segment(3);
    $images = $this->images->getByGalleryId($galleryId);
    $info = array();
    foreach($images as $image) 
    {
      $file = new stdClass();
      $file->image_id = $image['image_id'];
      $file->name = $image['image_file'];
      $file->size = filesize($this->getPath_img_upload_folder().$image['image_file']);
      $file->url = $this->getPath_url_img_upload_folder() . rawurlencode($image['image_file']);
      $file->thumbnail_url = $this->getPath_url_img_thumb_upload_folder() . rawurlencode($image['thumb_file']);
      //File name in the url to delete
      $file->delete_url = $this->getDelete_img_url() . rawurlencode($image['image_file']).'/'.$image['image_id'];
      $file->delete_type = 'DELETE';
      array_push($info, $file);
    }
    
    header('Content-type: application/json');
    echo json_encode($info);
    //$this->get_scan_files();
  }
  
  public function get_image() {
    $imageId = $this->input->get('imageId'); 
    $result = $this->images->getById($imageId);
    header('Content-Type: application/json',true);
    echo json_encode($result);
  }
  
  public function save_caption(){
    
    $imageId = $this->input->post('imageId'); 
    $image = $this->images->getById($imageId);
    $this->images->save_caption();
    $this->session->set_flashdata('success', TRUE);
    $this->session->set_flashdata('successMessage', 'Successfully saved image caption.');
    redirect(base_url('widgets/galleryupload/'.$image['gallery_id']),'refresh');
  }

  //Get info and Scan the directory
  public function get_scan_files() {

    $file_name = isset($_REQUEST['file']) ?
    basename(stripslashes($_REQUEST['file'])) : null;
    if ($file_name) {
      $info = $this->get_file_object($file_name);
    } else {
      $info = $this->get_file_objects();
    }
    header('Content-type: application/json');
    echo json_encode($info);
  }

  protected function get_file_object($file_name) {
    $file_path = $this->getPath_img_upload_folder() . $file_name;
    if (is_file($file_path) && $file_name[0] !== '.') {

      $file = new stdClass();
      $file->name = $file_name;
      //$file->file_path = $file_path;
      $file->size = filesize($file_path);
      $file->url = $this->getPath_url_img_upload_folder() . rawurlencode($file->name);
      $name =  pathinfo( $file->url, PATHINFO_FILENAME);
      $ext = $ext = pathinfo( $file->url, PATHINFO_EXTENSION);
      $file->thumbnail_url = $this->getPath_url_img_thumb_upload_folder() . rawurlencode($name.'_thumb.'.$ext);
      //File name in the url to delete
      $file->delete_url = $this->getDelete_img_url() . rawurlencode($file->name);
      $file->delete_type = 'DELETE';

      return $file;
    }
    return null;
  }

  //Scan
  protected function get_file_objects() {
    return array_values(array_filter(array_map(
    array($this, 'get_file_object'), scandir($this->getPath_img_upload_folder())
    )));
  }



  // GETTER & SETTER


  public function getPath_img_upload_folder() {
    return $this->path_img_upload_folder;
  }

  public function setPath_img_upload_folder($path_img_upload_folder) {
    $this->path_img_upload_folder = $path_img_upload_folder;
  }

  public function getPath_img_thumb_upload_folder() {
    return $this->path_img_thumb_upload_folder;
  }

  public function setPath_img_thumb_upload_folder($path_img_thumb_upload_folder) {
    $this->path_img_thumb_upload_folder = $path_img_thumb_upload_folder;
  }

  public function getPath_url_img_upload_folder() {
    return $this->path_url_img_upload_folder;
  }

  public function setPath_url_img_upload_folder($path_url_img_upload_folder) {
    $this->path_url_img_upload_folder = $path_url_img_upload_folder;
  }

  public function getPath_url_img_thumb_upload_folder() {
    return $this->path_url_img_thumb_upload_folder;
  }

  public function setPath_url_img_thumb_upload_folder($path_url_img_thumb_upload_folder) {
    $this->path_url_img_thumb_upload_folder = $path_url_img_thumb_upload_folder;
  }

  public function getDelete_img_url() {
    return $this->delete_img_url;
  }

  public function setDelete_img_url($delete_img_url) {
    $this->delete_img_url = $delete_img_url;
  }


}
?>