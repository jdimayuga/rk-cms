<?php
class Widgets extends Secure_Controller {

  var $pageItems = 5;

  public function __construct()
  {
    parent::__construct();
    $this->load->helper('form','file','url');
    $this->load->library('form_validation');
    $this->load->library('pagination');
    $this->load->library('parser');
    $this->load->model('form','',TRUE);
    $this->load->model('gallery','',TRUE);
    $this->load->model('advertisements','',TRUE);
    $this->load->model('section','',TRUE);
    $this->load->model('video','',TRUE);
    $this->load->model('article','',TRUE);
  }

  public function index()
  {
    $data['title'] = 'RK CMS: Widgets';
    $data = array_merge($data, $this->_get_reference_data());
    $sectionData['forms'] = $this->form->getAll($this->pageItems, $this->uri->segment(3));
    $this->_prepare_pagination('widgets/index', $this->form->count());
    $data['widget_section'] = $this->parser->parse('pages/widgets/forms', $sectionData, TRUE);
    $this->_show_view($data);
  }

  public function saveform()
  {
    $this->form_validation->set_rules('name','Form Name','required');
    if($this->form_validation->run() === TRUE)
    {
      $id = $this->form->save();
      $sectionData['success'] = true;
      $sectionData['successMessage'] = "Successfully saved form";
    }

    $data['title'] = 'RK CMS: Widgets';
    $data = array_merge($data, $this->_get_reference_data());
    $sectionData['forms'] = $this->form->getAll($this->pageItems, $this->uri->segment(3));
    $this->_prepare_pagination('widgets/index', $this->form->count());
    $data['widget_section'] = $this->parser->parse('pages/widgets/forms', $sectionData, TRUE);
    $this->_show_view($data);
  }

  public function formview()
  {
    $formId = trim($this->input->get('id'));
    $result = $this->form->getById($formId);
    $preview = form_prep($result['form_content'].' '.$result['form_scripts']);
    $result['form_preview'] = $preview;
    header('Content-Type: application/json',true);
    echo json_encode($result);
  }

  public function deleteform()
  {
    $result = $this->form->delete();
    if($result)
    {
      $sectionData['success'] = true;
      $sectionData['successMessage'] = "Successfully deleted form.";
    }
    $data['title'] = 'RK CMS: Widgets';
    $data = array_merge($data, $this->_get_reference_data());
    $sectionData['forms'] = $this->form->getAll($this->pageItems, $this->uri->segment(3));
    $this->_prepare_pagination('widgets/index', $this->form->count());
    $data['widget_section'] = $this->parser->parse('pages/widgets/forms', $sectionData, TRUE);
    $this->_show_view($data);
  }


  public function gallery()
  {
    $data['title'] = 'RK CMS: Widgets - Gallery';
    $data = array_merge($data, $this->_get_reference_data());
    $data['active'] = 'gallery';
    $sectionData['galleries'] = $this->gallery->getAll($this->pageItems, $this->uri->segment(3));
    $sectionData['sections'] = $this->section->getAllOrderByName();
    $sectionData['articles'] = $this->article->getAllActiveArticles();
    $this->_prepare_pagination('widgets/gallery', $this->gallery->count());
    $data['widget_section'] = $this->parser->parse('pages/widgets/gallery', $sectionData, TRUE);
    $this->_show_view($data);
  }

  public function galleryview()
  {
    $galleryId = trim($this->input->get('id'));
    $result = $this->gallery->getById($galleryId);
    header('Content-Type: application/json',true);
    echo json_encode($result);
  }
   
  public function savegallery()
  {
    $this->form_validation->set_rules('name','Gallery Name','required');
    if($this->form_validation->run() === TRUE)
    {
      $id = $this->gallery->save();
      
      $this->gallery->uploadCovers($id);
      
      $this->session->set_flashdata('success', TRUE);
      $this->session->set_flashdata('successMessage', 'Successfully saved gallery.');
      redirect('/widgets/gallery', 'refresh');
    }
    else{
      $this->gallery();
    }
  }

  public function deletegallery()
  {
    $result = $this->gallery->delete();
    if($result)
    {
      $this->session->set_flashdata('success', TRUE);
      $this->session->set_flashdata('successMessage', 'Successfully deleted gallery.');
    }
    redirect('/widgets/gallery', 'refresh');
  }

  public function galleryupload($galleryId)
  {
    $data['title'] = 'RK CMS: Widgets - Gallery Upload';
    $data = array_merge($data, $this->_get_reference_data());
    $data['active'] = 'gallery';
    $sectionData['gallery'] = $this->gallery->getById($galleryId);
    $this->_prepare_pagination('widgets/galleryupload/'.$galleryId, $this->gallery->count());
    $data['widget_section'] = $this->parser->parse('pages/widgets/photos', $sectionData, TRUE);
    $this->_show_view($data);
  }

  public function advertisements()
  {
    $data['title'] = 'RK CMS: Widgets - Advertisements';
    $data = array_merge($data, $this->_get_reference_data());
    $data['active'] = 'advertisements';
    $sectionData['advertisements'] = $this->advertisements->getAll($this->pageItems, $this->uri->segment(3));
    $this->_prepare_pagination('widgets/advertisements', $this->advertisements->count());
    $data['widget_section'] = $this->parser->parse('pages/widgets/advertisements', $sectionData, TRUE);
    $this->_show_view($data);
  }

  public function save_advertisement()
  {
    $this->form_validation->set_rules('name','Advertisement Name','required');
    if($this->form_validation->run() === TRUE)
    {
      $id = $this->advertisements->save();
      $this->advertisements->uploadFlash($id);
      $this->session->set_flashdata('success', TRUE);
      $this->session->set_flashdata('successMessage', 'Successfully saved advertisement.');
      redirect('/widgets/advertisements', 'refresh');
    }
    else{
      $this->advertisements();
    }
  }

  public function view_advertisement()
  {
    $ad_id = trim($this->input->get('id'));
    $result = $this->advertisements->getById($ad_id);
    header('Content-Type: application/json',TRUE);
    echo json_encode($result);
  }

  public function delete_advertisement()
  {
    $result = $this->advertisements->delete();
    if($result)
    {
      $this->session->set_flashdata('success', TRUE);
      $this->session->set_flashdata('successMessage', 'Successfully deleted advertisement.');
    }
    redirect('/widgets/advertisements', 'refresh');
  }

  public function upload_advertisement($ad_id)
  {
    $data['title'] = 'RK CMS: Widgets - Advertisement Upload';
    $data = array_merge($data, $this->_get_reference_data());
    $data['active'] = 'advertisements';
    $section_data['advertisement'] = $this->advertisements->getById($ad_id);
    $data['widget_section'] = $this->parser->parse('pages/widgets/ad_photos', $section_data, TRUE);
    $this->_show_view($data);
  }

  public function videos()
  {
    $data['title'] = 'RK CMS: Widgets - Videos';
    $data = array_merge($data, $this->_get_reference_data());
    $data['active'] = 'videos';
    $sectionData['videos'] = $this->video->getAll($this->pageItems, $this->uri->segment(3));
    $sectionData['sections'] = $this->section->getAllOrderByName();
    $this->_prepare_pagination('widgets/videos', $this->video->count());
    $data['widget_section'] = $this->parser->parse('pages/widgets/videos', $sectionData, TRUE);
    $this->_show_view($data);
  }

  public function save_video()
  {
    $this->form_validation->set_rules('title','Video Title','required');
    if($this->form_validation->run() === TRUE)
    {
      $id = $this->video->save();
      $this->session->set_flashdata('success', TRUE);
      $this->session->set_flashdata('successMessage', 'Successfully saved video.');
      redirect('/widgets/videos', 'refresh');
    }
    else
    {
      $this->videos();
    }
  }

  public function view_video()
  {
    $formId = trim($this->input->get('id'));
    $result = $this->video->getById($formId);
    header('Content-Type: application/json',true);
    echo json_encode($result);
  }

  public function delete_video()
  {
    $result = $this->video->delete();
    if($result)
    {
      $sectionData['success'] = true;
      $sectionData['successMessage'] = "Successfully deleted video.";
    }
    redirect('/widgets/videos', 'refresh');
  }

  private function _prepare_pagination($url, $count)
  {
    $config['base_url'] = base_url().$url;
    $config['total_rows'] = $count;
    $config['per_page'] = $this->pageItems;
    $config['full_tag_open'] = '<ul>';
    $config['full_tag_close'] = '</ul>';
    $config['first_link'] = FALSE;
    $config['last_link'] = FALSE;
    $config['next_link'] = 'Next';
    $config['next_tag_open'] = '<li>';
    $config['next_tag_close'] = '</li>';
    $config['prev_link'] = 'Prev';
    $config['prev_tag_open'] = '<li>';
    $config['prev_tag_close'] = '</li>';
    $config['cur_tag_open'] = '<li class="active"><a href="#">';
    $config['cur_tag_close'] = '</a></li>';
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';

    $this->pagination->initialize($config);
  }

  private function _get_reference_data() {
    if($this->session->userdata('logged_in')) {
      $session_data = $this->session->userdata('logged_in');
      $data['username'] = $session_data['username'];
    }
    $data['pageActive'] = 'widgets';
    return $data;
  }

  private function _show_view($data)
  {
    $this->load->view('templates/header', $data);
    $this->load->view('templates/pageheader', $data);
    $this->load->view('pages/widgets',$data);
    $this->load->view('templates/footer',$data);
  }

}
?>