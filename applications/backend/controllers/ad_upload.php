<?php
class Ad_Upload extends CI_Controller {

  protected $path_img_upload_folder;
  protected $path_img_thumb_upload_folder;
  protected $path_url_img_upload_folder;
  protected $path_url_img_thumb_upload_folder;

  protected $delete_img_url;

  function __construct() {
    parent::__construct();
    $this->load->helper(array('form', 'url'));
    $this->load->model('ad_images','',TRUE);

    //Set relative Path with CI Constant
    $this->setPath_img_upload_folder("../assets/uploads/ads/");
    $this->setPath_img_thumb_upload_folder("../assets/uploads/ads/thumb/");
    
    //Delete img url
    $this->setDelete_img_url(base_url() . 'ad_upload/deleteImage/');
    
    //Set url img with Base_url()
    $this->setPath_url_img_upload_folder(base_url() . "../assets/uploads/ads/");
    $this->setPath_url_img_thumb_upload_folder(base_url() . "../assets/uploads/ads/thumb/");
  }


  // Function called by the form
  public function upload_img() {
    
    header('Content-Type: application/json',true);
    
    //Format the name
    $name = $_FILES['userfile']['name'];

    // replace characters other than letters, numbers and . by _
    $name = preg_replace('/([^.a-z0-9]+)/i', '_', $name);
    
    $ad_id =  $this->input->post('ad_id');

    //Your upload directory, see CI user guide
    $config['upload_path'] = $this->getPath_img_upload_folder();

    $config['allowed_types'] = 'gif|jpg|png|JPG|GIF|PNG';
    $config['max_size'] = '2048';
    $config['file_name'] = $name;

    //Load the upload library
    $this->load->library('upload', $config);

    if ($this->do_upload()) {
      
      $data = $this->upload->data();
      
      //If you want to resize
      $config['new_image'] = $this->getPath_img_thumb_upload_folder();
      $config['image_library'] = 'gd2';
      $config['source_image'] = $data['full_path'];
      $config['create_thumb'] = TRUE;
      $config['maintain_ratio'] = TRUE;
      $config['width'] = 160;
      $config['height'] = 120;

      $this->load->library('image_lib', $config);

      $this->image_lib->resize();
  
      
      $fileExt = $data['file_ext'];
      $rawName = $data['raw_name'];
      $thumb = $rawName.'_thumb'.$fileExt;
  
      //save
      $imageId = $this->ad_images->save($data['file_name'], $thumb, $ad_id);
      
      $info = new stdClass();

      $info->name = $data['file_name'];
      $info->size = $data['file_size'];
      $info->type = $data['file_type'];
      $info->url = $this->getPath_url_img_upload_folder() . $name;
      $info->thumbnail_url = $this->getPath_url_img_thumb_upload_folder() . $thumb; //I set this to original file since I did not create thumbs.  change to thumbnail directory if you do = $upload_path_url .'/thumbs' .$name
      $info->delete_url = $this->getDelete_img_url() . $name . '/'.$imageId;
      $info->delete_type = 'DELETE';

      
      
      //Return JSON data
      echo json_encode(array($info));

    } else {

      // the display_errors() function wraps error messages in <p> by default and these html chars don't parse in
      // default view on the forum so either set them to blank, or decide how you want them to display.  null is passed.
      $error = array('error' => $this->upload->display_errors('',''));

      echo json_encode(array($error));
    }


  }


  //Function for the upload : return true/false
  public function do_upload() {

    if (!$this->upload->do_upload()) {

      return false;
    } else {
      //$data = array('upload_data' => $this->upload->data());

      return true;
    }
  }


  //Function Delete image
  public function deleteImage() {

    //Get the name in the url
    $file = $this->uri->segment(3);
    $image_id = $this->uri->segment(4);
    
    $this->ad_images->delete($image_id);

    $name =  pathinfo( $file, PATHINFO_FILENAME);
    $ext = $ext = pathinfo( $file, PATHINFO_EXTENSION);
    $thumbfile = $name.'_thumb.'.$ext;

    $success = unlink($this->getPath_img_upload_folder() . $file);
    $success_th = unlink($this->getPath_img_thumb_upload_folder() . $thumbfile);

    //info to see if it is doing what it is supposed to
    $info = new stdClass();
    $info->sucess = $success;
    $info->path = $this->getPath_url_img_upload_folder() . $file;
    $info->file = is_file($this->getPath_img_upload_folder() . $file);

    echo json_encode(array($info));

  }


  //Load the files
  public function get_files() {
    $ad_id = $this->uri->segment(3);
    $images = $this->ad_images->getByAdvertisementId($ad_id);
    $info = array();
    foreach($images as $image) 
    {
      $file = new stdClass();
      $file->name = $image['ad_image_file'];
      $file->size = filesize($this->getPath_img_upload_folder().$image['ad_image_file']);
      $file->url = $this->getPath_url_img_upload_folder() . rawurlencode($image['ad_image_file']);
      $file->thumbnail_url = $this->getPath_url_img_thumb_upload_folder() . rawurlencode($image['ad_thumb_file']);
      //File name in the url to delete
      $file->delete_url = $this->getDelete_img_url() . rawurlencode($image['ad_image_file']).'/'.$image['ad_image_id'];
      $file->delete_type = 'DELETE';
      array_push($info, $file);
    }
    
    header('Content-type: application/json');
    echo json_encode($info);

  }


  // GETTER & SETTER


  public function getPath_img_upload_folder() {
    return $this->path_img_upload_folder;
  }

  public function setPath_img_upload_folder($path_img_upload_folder) {
    $this->path_img_upload_folder = $path_img_upload_folder;
  }

  public function getPath_img_thumb_upload_folder() {
    return $this->path_img_thumb_upload_folder;
  }

  public function setPath_img_thumb_upload_folder($path_img_thumb_upload_folder) {
    $this->path_img_thumb_upload_folder = $path_img_thumb_upload_folder;
  }

  public function getPath_url_img_upload_folder() {
    return $this->path_url_img_upload_folder;
  }

  public function setPath_url_img_upload_folder($path_url_img_upload_folder) {
    $this->path_url_img_upload_folder = $path_url_img_upload_folder;
  }

  public function getPath_url_img_thumb_upload_folder() {
    return $this->path_url_img_thumb_upload_folder;
  }

  public function setPath_url_img_thumb_upload_folder($path_url_img_thumb_upload_folder) {
    $this->path_url_img_thumb_upload_folder = $path_url_img_thumb_upload_folder;
  }

  public function getDelete_img_url() {
    return $this->delete_img_url;
  }

  public function setDelete_img_url($delete_img_url) {
    $this->delete_img_url = $delete_img_url;
  }


}
?>