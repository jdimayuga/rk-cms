<?php
class Tags extends Secure_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('tag','',TRUE);
  }

  public function typeahead()
  {
    $tagName = trim($this->input->get('tag'));
    $result = $this->tag->getTag($tagName);
    header('Content-Type: application/json',true);
    echo json_encode($result);
  }

}
?>