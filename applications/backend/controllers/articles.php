<?php
class Articles extends Secure_Controller {

  var $pageItems = 5;

  public function __construct()
  {
    parent::__construct();
    $this->load->model('article','',TRUE);
    $this->load->model('tag','',TRUE);
    $this->load->model('section','',TRUE);
    $this->load->helper('form','file','url');
    $this->load->library('form_validation');
    $this->load->library('pagination');
  }

  public function index()
  {
    $data['title'] = 'RK CMS: Article Management';
    $data = array_merge($data, $this->getReferenceData());
    $this->preparePagination();
    $data['articles'] = $this->article->getArticles($this->pageItems,$this->uri->segment(3));
    $this->showView($data);
  }



  public function save()
  {
    $data['title'] = 'RK CMS: Saved Article';
    $this->form_validation->set_rules('title','Title','required');
    $this->form_validation->set_rules('date', 'Article Date', 'required');

    if($this->form_validation->run() === TRUE)
    {
      $articleId = $this->article->saveArticle();

      $imageUpload = $this->article->uploadImage($articleId);
      $this->tag->saveTag();
      $data['success'] = true;
      $data['successMessage'] = "Successfully saved Article: ".$this->input->post('title');
    }

    $data = array_merge($data, $this->getReferenceData());
    $this->preparePagination();
    $data['articles'] = $this->article->getArticles($this->pageItems,$this->uri->segment(3));
    $this->showView($data);
  }

  public function view()
  {
    $articleId = trim($this->input->get('id'));
    $result = $this->article->getArticleById($articleId);
    $date = DateTime::createFromFormat('Y-m-d H:i:s', $result['article_date']);
    //$date = new DateTime($result['article_date']);
    $result['article_date'] = $date->format('m/d/Y');
    $result['article_title_slug'] = url_title($result['article_title']);
    header('Content-Type: application/json',true);
    echo json_encode($result);
  }
  
  public function toggle_featured()
  {
    $articleId = trim($this->input->get('id'));
    $type = trim($this->input->get('type'));
    $status = trim($this->input->get('status'));
    
    if($type == 'featured') {
      
      $this->article->toggleFeatured($articleId,$type,$status);
      $result['status'] = true; 
      
    }
    else {
      if($status == '1' && $this->article->countFeatured($type) == 2){
        $result['status'] = false; 
      }
      else {
        $this->article->toggleFeatured($articleId,$type,$status);
        $result['status'] = true; 
      }
    }
    
    header('Content-Type: application/json',true);
    echo json_encode($result);
  }

  public function delete()
  {
    $data['title'] = 'RK CMS: Article Management';
    $result = $this->article->delete();
    if($result)
    {
      $data['success'] = true;
      $data['successMessage'] = "Successfully deleted page.";
    }
    $data = array_merge($data, $this->getReferenceData());
    $this->preparePagination();
    $data['articles'] = $this->article->getArticles($this->pageItems,$this->uri->segment(3));
    $this->showView($data);
  }


  private function getReferenceData() {
    if($this->session->userdata('logged_in')) {
      $session_data = $this->session->userdata('logged_in');
      $data['username'] = $session_data['username'];
    }
    $data['pageActive'] = 'articles';
    $data['sections'] = $this->section->getAllOrderByName();
    return $data;
  }

  private function preparePagination()
  {
    $config['base_url'] = base_url().'articles/index/';
    $config['total_rows'] = $this->article->getArticleCount();
    $config['per_page'] = $this->pageItems;
    $config['full_tag_open'] = '<ul>';
    $config['full_tag_close'] = '</ul>';
    $config['first_link'] = FALSE;
    $config['last_link'] = FALSE;
    $config['next_link'] = 'Next';
    $config['next_tag_open'] = '<li>';
    $config['next_tag_close'] = '</li>';
    $config['prev_link'] = 'Prev';
    $config['prev_tag_open'] = '<li>';
    $config['prev_tag_close'] = '</li>';
    $config['cur_tag_open'] = '<li class="active"><a href="#">';
    $config['cur_tag_close'] = '</a></li>';
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';

    $this->pagination->initialize($config);
  }

  private function showView($data)
  {
    $this->load->view('templates/header', $data);
    $this->load->view('templates/pageheader', $data);
    $this->load->view('pages/articles',$data);
    $this->load->view('templates/footer',$data);
  }

}
?>