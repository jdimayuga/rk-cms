<?php
class Analytics extends Secure_Controller {
  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $data['title'] = 'RK CMS: Analytics';
    $data = array_merge($data, $this->_get_reference_data());
    $this->_show_view($data);
  }
  
 private function _get_reference_data() {
    if($this->session->userdata('logged_in')) {
      $session_data = $this->session->userdata('logged_in');
      $data['username'] = $session_data['username'];
    }
    $data['pageActive'] = 'marketing';
    return $data;
  }

  private function _show_view($data)
  {
    $this->load->view('templates/header', $data);
    $this->load->view('templates/pageheader', $data);
    $this->load->view('pages/analytics',$data);
    $this->load->view('templates/footer',$data);
  }
}

?>