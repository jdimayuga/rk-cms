<div class="container-fluid">
  <div class="row-fluid">
  	<h3>Settings</h3>
  	<hr/>
  	<div class="span3">
  		<ul class="nav nav-tabs nav-stacked">
  			<li <?php echo !isset($active) ? 'class="active"' : ''; ?>><a href="<?php echo base_url('settings/index')?>">General</a></li>
  			<li <?php echo (isset($active) && $active == 'sections') ? 'class="active"' : ''; ?>><a href="<?php echo base_url()?>settings/sections">Sections</a></li>
  			<!--li <?php echo (isset($active) && $active == 'home') ? 'class="active"' : ''; ?>><a href="<?php echo base_url()?>settings/home">Home Page Ads</a></li-->
  		</ul>
  	</div>
  	<?php echo $settings_section; ?>
  </div>
</div>  