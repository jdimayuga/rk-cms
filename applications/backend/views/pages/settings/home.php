<div class="span8">
	<?php if($this->session->flashdata('success')):?>
 	<div class="alert alert-success">
 		<button data-dismiss="alert" class="close">&times;</button>
 		<?php echo $this->session->flashdata('successMessage') ?>
	</div>
 <?php endif;?>
  <?php echo form_open('settings/save_home', array('class'=>'form-horizontal')); ?>
  	<input type="hidden" id="id" name="id" class="input-xlarge span12" value="<?php echo (isset($settings['settings_id'])) ? $settings['settings_id'] : ""; ?>">
  	<fieldset>
  	  
  		<div class="control-group">
        <label for="title" class="control-label">Top Banner Ad</label>
        <div class="controls">
        	<img src="http://placehold.it/364X45"/>
        	<br/><br/>
          <input type="file" id="top_ad" name="top_ad" class="input-xlarge span12" value="<?php echo (isset($settings['title'])) ? $settings['title'] : ""; ?>">
          <p class="help-block">advertisement at the top of the navigation bar (728x90AD)</p>
        </div>
      </div>
      <div class="control-group">
        <label for="start_scripts" class="control-label">Top Right Ad</label>
        <div class="controls">
        	<img src="http://placehold.it/100X158"/>
        	<br/><br/>
          <input type="file" id="top_right_ad" name="top_right_ad" class="input-xlarge span12" value="<?php echo (isset($settings['title'])) ? $settings['title'] : ""; ?>">
          <p class="help-block">advertisement at the top of the right side of homepage (300x475AD)</p>
        </div>
      </div>
      <div class="control-group">
        <label for="start_scripts" class="control-label">Bottom Right Ad</label>
        <div class="controls">
        	<img src="http://placehold.it/150X125"/>
        	<br/><br/>
          <input type="file" id="bottom_right_ad" name="bottom_right_ad" class="input-xlarge span12" value="<?php echo (isset($settings['title'])) ? $settings['title'] : ""; ?>">
          <p class="help-block">advertisement at the bottom of the right side of homepage(300x250AD)</p>
        </div>
      </div>
      <div class="form-actions ">
        <button class="btn btn-primary pull-right" type="submit">Save changes</button>
      </div>
  	</fieldset>
  </form>
  
</div>