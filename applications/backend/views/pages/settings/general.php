<div class="span8">
	<?php if($this->session->flashdata('success')):?>
 	<div class="alert alert-success">
 		<button data-dismiss="alert" class="close">&times;</button>
 		<?php echo $this->session->flashdata('successMessage') ?>
	</div>
 <?php endif;?>
  <?php echo form_open('settings/save', array('class'=>'form-horizontal')); ?>
  	<input type="hidden" id="id" name="id" class="input-xlarge span12" value="<?php echo (isset($settings['settings_id'])) ? $settings['settings_id'] : ""; ?>">
  	<fieldset>
  		<div class="control-group">
        <label for="title" class="control-label">Website Title</label>
        <div class="controls">
          <input type="text" id="title" name="title" class="input-xlarge span12" value="<?php echo (isset($settings['title'])) ? $settings['title'] : ""; ?>">
          <p class="help-block">The title that will appear in the browser</p>
        </div>
      </div>
      <div class="control-group">
        <label for="start_scripts" class="control-label">&lt;head&gt; script(s)</label>
        <div class="controls">
          <textarea rows="6" id="start_scripts" name="start_scripts" class="input-xlarge span12"><?php echo (isset($settings['start_scripts'])) ? $settings['start_scripts'] : ""; ?></textarea>
          <p class="help-block">Paste code that needs to be loaded in the &lt;head&gt; section here.</p>
        </div>
      </div>
      <div class="control-group">
        <label for="end_scripts" class="control-label">End of &lt;body&gt; script(s)</label>
        <div class="controls">
          <textarea rows="6" id="end_scripts" name="end_scripts" class="input-xlarge span12"><?php echo (isset($settings['end_scripts'])) ? $settings['end_scripts'] : ""; ?></textarea>
          <p class="help-block">Paste code that needs to be loaded at the end of the &ltbody&gt; tag here.</p>
        </div>
      </div>
      <div class="form-actions ">
        <button class="btn btn-primary pull-right" type="submit">Save changes</button>
      </div>
  	</fieldset>
  </form>
  
</div>