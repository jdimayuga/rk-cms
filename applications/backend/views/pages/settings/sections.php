<div class="span8">
	 <?php if(validation_errors()):?>
     	<div class="alert alert-error">
     		<button data-dismiss="alert" class="close">&times;</button>
     		<strong>Form Errors:</strong>
  			<?php echo validation_errors() ?> 
  		</div>
     <?php endif;?>
     <?php if($this->session->flashdata('success')):?>
     	<div class="alert alert-success">
     		<button data-dismiss="alert" class="close">&times;</button>
     		<?php echo $this->session->flashdata('successMessage') ?>
  		</div>
		 <?php endif;?>
	<div class="pull-right">
		<a class="btn new" href="#"><i class="icon-plus"></i> New Section</a>
	</div>
	<table class="table table-striped">
  	<thead>
  		<tr>
  			<th>Section ID</th>
  			<th>Logo</th>
  			<th>Section Name</th>
  			<th>Menu Bar</th>
  			<th>Feature</th>
  			<th>Tasks</th>
  		</tr>
  	</thead>
  	<tbody class="files" data-target="#modal-gallery" data-toggle="modal-gallery" data-selector=".gallery-item">
  		<?php if(empty($sections)):?>
  		<tr>
  			<td colspan="4">
  				No section yet.
  			</td>
  		</tr>
  		<?php else:?>
    		<?php foreach($sections as $section):?>
      		<tr>
      			<td><?php echo $section['section_id']?></td>
      			<td>
      				<?php if(!empty($section['section_logo'])):?>
      				<a class="gallery-item" href="<?php echo base_url()?>../assets/uploads/logo/<?php echo $section['section_logo']?>">
    				  	<img class="thumbnail" alt="" src="<?php echo base_url()?>../assets/uploads/logo/<?php echo $section['section_logo']?>" width="100"/>
    					</a>
    					<a class="gallery-item" href="<?php echo base_url()?>../assets/uploads/logo/<?php echo $section['section_logo']?>"><?php echo $section['section_logo']?></a>
    					<?php endif;?>
    				</td>
      			<td><?php echo $section['section_name']?></td>
      			<td><?php echo $section['menu_bar_color']?></td>
      			<td>
      				<button id="<?php echo $section['section_id']?>" class="btn featured <?php echo $section['featured'] == 1 ? 'btn-primary active' : ''?>" data-toggle="button" 
      					data-content="The featured sections at the bottom of the homepage." rel="popover" title="Featured Section">Featured</button>
      			</td>
      			<td>
      				<a class="btn btn-small url" href="#" id="<?php echo $section['section_id']?>"><i class="icon-edit"></i> View URL</a>
      			  <a class="btn btn-small edit" href="#" id="<?php echo $section['section_id']?>"><i class="icon-edit"></i> Edit</a>
        			<a class="btn btn-small delete" href="#" id="<?php echo $section['section_id']?>"><i class="icon-trash"></i> Delete</a>
        		</td>
      		</tr>
    		<?php endforeach;?>
  		<?php endif;?>
  	</tbody>
  </table>
  <div class="pagination pull-right">
    <?php echo $this->pagination->create_links(); ?>
  </div>
</div>

<div id="modal-section" class="modal hide fade">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h3>Section</h3>
    </div>
    <div class="modal-body">
    	<?php echo form_open_multipart('settings/save_section'); ?>
       <input type="hidden" id="section_id"  name="id" value="">
       <label>Section Name</label>
       <input type="text" name="name" id="name" class="span12 <?php echo form_error('name') ? 'red' : ''; ?>"/>
       <label>Menu Color</label>
       <input type="text" name="color" id="color" class="span12"/>
       <label>Site Logo (465px &times; 89px)</label>
       <input type="file" name="image">
       <div class="modal-footer">
          <a href="#" class="btn" data-dismiss="modal">Cancel</a>
          <button class="btn btn-primary" type="submit">Save Changes</button>
       </div>
       </form>
    </div>
</div> 

<div id="modal-confirm-delete" class="modal hide fade">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h3>Delete Section</h3>
    </div>
    <div class="modal-body">
      <p>You are about to delete this section. Are you sure you want to continue?</p>
    </div>
    <div class="modal-footer">
    	<?php echo form_open('settings/delete_section'); ?>
    	<input type="hidden" name="id" id="sectionDeleteId" value="">
    	<button class="btn btn-danger" type="submit">Yes</button>
      <a href="#" class="btn secondary" data-dismiss="modal">No</a>
      </form>
    </div>
</div>

<div id="modal-copy-url" class="modal hide fade">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h3>Page URL</h3>
    </div>
    <div class="modal-body">
      <p id="page_url"></p>
    </div>
    <div class="modal-footer">
      <span style="position:relative">
				<a class="btn btn-small copy-url" href="#"><i class="icon-download"></i> Copy URL</a>
	  	</span>
      <a href="#" class="btn secondary" data-dismiss="modal">Close</a>
    </div>
</div>
<script type="text/javascript">
$(document).on('click', '.new', function(e) {
	e.preventDefault();
	$('#section_id').val('');
	$('#name').val('');
	$('#color').val('');
	
	$('#modal-section').modal({'show':true,'keyboard':false}).css({
        width: '900px',
        'margin-top': function () {
            return -($(this).height() / 2);
        },
        'margin-left': function () {
            return -($(this).width() / 2);
        }
    });
});


$(document).on('click', '.edit', function(e) {
	e.preventDefault();

	var section_id = $(this).attr('id');
	$('#section_id').val(section_id);
	
	$.getJSON('<?php echo base_url()?>settings/section_view', { id: section_id }, function(data) {
			$('#gallery_id').val(data.section_id);
  		$('#name').val(data.section_name);
  		$('#color').val(data.menu_bar_color);
  });
	
	$('#modal-section').modal({'show':true,'keyboard':false}).css({
        width: '900px',
        'margin-top': function () {
            return -($(this).height() / 2);
        },
        'margin-left': function () {
            return -($(this).width() / 2);
        }
    });
});


$(document).on('click', '.delete', function(e) {
	e.preventDefault();

	var section_id = $(this).attr('id');
	$('#sectionDeleteId').val(section_id);
	$('#modal-confirm-delete').modal('show');
});


$(document).on('click', '.url', function(e) {
	e.preventDefault();
	var sectionId = $(this).attr('id');

	$.getJSON('<?php echo base_url()?>settings/section_view', { id: sectionId }, function(data) {
		var url = '<?php echo $this->config->item('frontend_url');?>section/' + data.section_slug + '/' + sectionId;
  	$('#page_url').html(url.toLowerCase());
  });

	$('#modal-copy-url').modal({'show':true,'keyboard':false}).css({
        width: '400px',
        'margin-top': function () {
            return -($(this).height() / 2);
        },
        'margin-left': function () {
            return -($(this).width() / 2);
        }
    });
});


$('#modal-copy-url').on('shown', function(){
	var pageURL = $('#page_url').text();
	$('a.copy-url').zclip({
  		path:"<?=base_url()?>../assets/js/ZeroClipboard.swf",
  		copy:pageURL,
  		afterCopy: function(){
				//alert('Copied Page URL');
	  	}
		});
});


$(".featured").on('hover', function() {
	 $(this).popover('show');
});

$(".featured").on('click', function(e) {
		e.preventDefault();
		var sectionId = $(this).attr('id');
		var element = $(this);
		if(!$(element).hasClass('active')) {
			//activate
			$.getJSON('<?php echo base_url()?>settings/toggle_featured_section', { id: sectionId, status: 1 }, function(data) {
				if(!data.status) {
					alert('There are already four featured sections. Please deactivate a section first if you wish to feature this section');
					$(element).removeClass('active');
				}
				else {
					$(element).addClass('btn-primary');
				}
				
		  });
		}
		else {
			//deactivate
			$.getJSON('<?php echo base_url()?>settings/toggle_featured_section', { id: sectionId, status: 0 }, function(data) {
				$(element).removeClass('btn-primary');
		  });
		}
});


</script>