<div class="span8">
	<?php if(validation_errors()):?>
      <div class="alert alert-error">
        <button data-dismiss="alert" class="close">&times;</button>
        <strong>Form Errors:</strong>
        <?php echo validation_errors() ?> 
      </div>
     <?php endif;?>
     <?php if($this->session->flashdata('success')):?>
      <div class="alert alert-success">
        <button data-dismiss="alert" class="close">&times;</button>
        <?php echo $this->session->flashdata('successMessage') ?>
      </div>
     <?php endif;?>
	<div class="pull-right">
		<a class="btn new" href="#"><i class="icon-plus"></i> New Video</a>
	</div>
	<table class="table table-striped">
  	<thead>
  		<tr>
  			<th>Video ID</th>
  			<th>Video Title</th>
  			<th>Tasks</th>
  		</tr>
  	</thead>
  	<tbody>
  		<?php if(empty($videos)):?>
  		<tr>
  			<td colspan="4">
  				No videos yet.
  			</td>
  		</tr>
  		<?php else:?>
    		<?php foreach($videos as $video):?>
      		<tr>
      			<td><?php echo $video['video_id']?></td>
      			<td><?php echo $video['video_title']?></td>
      			<td>
      			  <a class="btn btn-small edit" href="#" id="<?php echo $video['video_id']?>"><i class="icon-edit"></i> Edit</a>
        			<a class="btn btn-small delete" href="#" id="<?php echo $video['video_id']?>"><i class="icon-trash"></i> Delete</a>
      			</td>
      		</tr>
    		<?php endforeach;?>
  		<?php endif;?>
  	</tbody>
  </table>
  <div class="pagination pull-right">
    <?php echo $this->pagination->create_links(); ?>
  </div>
</div>

<div id="modal-form" class="modal hide fade">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h3>Video</h3>
    </div>
    <div class="modal-body">
    	<?php echo form_open('widgets/save_video'); ?>
       <input type="hidden" id="video_id"  name="id" value="">
       <label>Section</label>
       <select id="section" name="section">
    		<?php foreach ($sections as $section):?>
    			<option value="<?php echo $section['section_id']?>"><?php echo $section['section_name']?></option>
    		<?php endforeach;?>
       </select>
       <label>Video Title</label>
       <input type="text" name="title" id="title" class="span12 <?php echo form_error('title') ? 'red' : ''; ?>"/>
       <label>Video Embed</label>
       <textarea name="embed" rows="5" id="embed" class="span12"></textarea>
       <label>Video Caption</label>
       <textarea name="caption" rows="5" id="caption" class="span12"></textarea>
       
       <div class="modal-footer">
      		<a href="#" class="btn" data-dismiss="modal">Cancel</a>
          <button class="btn btn-primary" type="submit">Save Changes</button>
       </div>
       </form>
    </div>
</div> 

<div id="modal-confirm-delete" class="modal hide fade">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h3>Delete Video</h3>
    </div>
    <div class="modal-body">
      <p>You are about to delete this video. Are you sure you want to continue?</p>
    </div>
    <div class="modal-footer">
    	<?php echo form_open('widgets/delete_video'); ?>
    	<input type="hidden" name="id" id="videoDeleteId" value="">
    	<button class="btn btn-danger" type="submit">Yes</button>
      <a href="#" class="btn secondary" data-dismiss="modal">No</a>
      </form>
    </div>
</div>
<script type="text/javascript">
$(document).on('click', '.new', function(e) {
	e.preventDefault();
	$('#video_id').val('');
	$('#title').val('');
	$('#caption').val('');
	$('#embed').val('');
	$("#section")[0].selectedIndex = 0;

	$('#modal-form').modal({'show':true,'keyboard':false}).css({
        width: '900px',
        'margin-top': function () {
            return -($(this).height() / 2);
        },
        'margin-left': function () {
            return -($(this).width() / 2);
        }
    });
});


$(document).on('click', '.edit', function(e) {
	e.preventDefault();

	var videoId = $(this).attr('id');
	$('#video_id').val(videoId);
	
	$.getJSON('<?php echo base_url()?>widgets/view_video', { id: videoId }, function(data) {
			$('#video_id').val(data.video_id);
  		$('#title').val(data.video_title);
  		$('#caption').val(data.video_caption);
  		$('#embed').val(data.video_embed);
  		$("#section").val(data.video_section);
	});
	
	$('#modal-form').modal({'show':true,'keyboard':false}).css({
        width: '900px',
        'margin-top': function () {
            return -($(this).height() / 2);
        },
        'margin-left': function () {
            return -($(this).width() / 2);
        }
    });
});

$(document).on('click', '.delete', function(e) {
	e.preventDefault();

	var videoId = $(this).attr('id');
	$('#videoDeleteId').val(videoId);
	$('#modal-confirm-delete').modal('show');
});



</script>