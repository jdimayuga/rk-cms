<!--div class="span8  alert alert-block">
	<p>Flash Ads and Slideshow Effects are currently not yet supported. You may only use a single image for your Ads.</p>
</div-->

<div class="span8">
   <?php if(validation_errors()):?>
      <div class="alert alert-error">
        <button data-dismiss="alert" class="close">&times;</button>
        <strong>Form Errors:</strong>
        <?php echo validation_errors() ?> 
      </div>
     <?php endif;?>
     <?php if($this->session->flashdata('success')):?>
      <div class="alert alert-success">
        <button data-dismiss="alert" class="close">&times;</button>
        <?php echo $this->session->flashdata('successMessage') ?>
      </div>
     <?php endif;?>
  <div class="pull-right">
    <a class="btn new" href="#"><i class="icon-plus"></i> New Advertisement</a>
  </div>
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Advertisement ID</th>
        <th>Advertisement Name</th>
        <th>Area</th>
        <th>Type</th>
        <th>Tasks</th>
      </tr>
    </thead>
    <tbody>
      <?php if(empty($advertisements)):?>
      <tr>
        <td colspan="4">
          No ads yet.
        </td>
      </tr>
      <?php else:?>
        <?php foreach($advertisements as $advertisement):?>
          <tr>
            <td><?php echo $advertisement['ad_id']?></td>
            <td><?php echo $advertisement['ad_name']?></td>
            <td>
              <?php 
                switch($advertisement['ad_area']) {
                  case 'page_banner';
                    echo 'Page Banner (728 x 90)';
                    break;
                  case 'side_top';
                    echo 'Side Top (300 x 475)';
                    break;
                  case 'side_bottom';
                    echo 'Side Bottom (300 x 250)';
                    break;
                }
              ?>
            </td>
            <td><?php echo ucfirst($advertisement['ad_type']) ?></td>
            <td>
              <a class="btn btn-small view" href="#" id="<?php echo $advertisement['ad_id']?>"><i class="icon-list-alt"></i> View Code</a>
              <a class="btn btn-small edit" href="#" id="<?php echo $advertisement['ad_id']?>"><i class="icon-edit"></i> Edit</a>
              <a class="btn btn-small delete" href="#" id="<?php echo $advertisement['ad_id']?>"><i class="icon-trash"></i> Delete</a>
              <?php if($advertisement['ad_type'] == 'images'):?>
              <a class="btn btn-small photos" href="<?php echo base_url()?>widgets/upload_advertisement/<?php echo $advertisement['ad_id']?>" id="<?php echo $advertisement['ad_id']?>"><i class="icon-picture"></i>Images</a>
              <?php endif;?>
            </td>
          </tr>
        <?php endforeach;?>
      <?php endif;?>
    </tbody>
  </table>
  <div class="pagination pull-right">
    <?php echo $this->pagination->create_links(); ?>
  </div>
</div>

<div id="modal-ad" class="modal hide fade">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h3>Advertisement</h3>
    </div>
    <div class="modal-body">
      <?php echo form_open_multipart('widgets/save_advertisement'); ?>
       <input type="hidden" id="ad_id"  name="id" value="">
       <label>Ad Name</label>
       <input type="text" name="name" id="name" class="span12 <?php echo form_error('name') ? 'red' : ''; ?>"/>
       
       <label>Ad Type</label>
       <select id="type" name="type" class="span12" autocomplete="off">
       	<option value="flash">Flash</option>
       	<option value="images">Images</option>
       </select>
       
       <div id="flash_content">
         <label>Flash Code</label>
         <textarea id="flash_script" name="flash_script" class="span12"></textarea>
         <label>or</label>
         <label>Flash File</label>
       	 <input type="file" name="flash">
       </div>
       
       <div id="animation_effects" class="hide fade" autocomplete="off">
         <label>Animation Effects</label>
         <select id="effects" name="effects" class="span12">
          <option value="single">Single Image/No Effect</option>
         	<option value="fade">Fade</option>
         	<option value="slide">Slide</option>
         </select>
       </div>
       
       <div id="area_section">
         <label>Area</label>
         <select id="area" name="area" class="span12">
          <option value="page_banner">Page Banner (728 x 90)</option>
         	<option value="side_top">Side Top (300 x 475)</option>
         	<option value="side_bottom">Side Bottom (300 x 250)</option>
         </select>
       </div>
       
       <div class="modal-footer">
          <a href="#" class="btn" data-dismiss="modal">Cancel</a>
          <button class="btn btn-primary" type="submit">Save Changes</button>
       </div>
       </form>
    </div>
</div> 


<div id="modal-ad-view" class="modal hide fade">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h3>Ad Code Preview</h3>
    </div>
    <div class="modal-body">
      <div class="span12">
        <pre id="preview" class="prettyprint"></pre>
        <p class="help-block">Copy this code and place it in site content.</p>
    		<br/>
      </div>
      
      <div class="modal-footer">
          <a href="#" class="btn" data-dismiss="modal">Close</a>
       </div>
    </div>
</div> 

<div id="modal-confirm-delete" class="modal hide fade">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h3>Delete Advertisement</h3>
    </div>
    <div class="modal-body">
      <p>You are about to delete this ad item. Are you sure you want to continue?</p>
    </div>
    <div class="modal-footer">
      <?php echo form_open('widgets/delete_advertisement'); ?>
      <input type="hidden" name="id" id="ad_delete_id" value="">
      <button class="btn btn-danger" type="submit">Yes</button>
      <a href="#" class="btn secondary" data-dismiss="modal">No</a>
      </form>
    </div>
</div>
<script type="text/javascript">
$(document).on('click', '.new', function(e) {
  e.preventDefault();
  $('#ad_id').val('');
  $('#name').val('');
  $("#area")[0].selectedIndex = 0;
  $('#modal-ad').modal({'show':true,'keyboard':false}).css({
        width: '400px',
        'margin-top': function () {
            return -($(this).height() / 2);
        },
        'margin-left': function () {
            return -($(this).width() / 2);
        }
    });
});


$(document).on('click', '.edit', function(e) {
  e.preventDefault();

  var ad_id = $(this).attr('id');
  $('#ad_id').val(ad_id);
  
  $.getJSON('<?php echo base_url()?>widgets/view_advertisement', { id: ad_id }, function(data) {
      $('#ad_id').val(data.ad_id);
      $('#name').val(data.ad_name);
      $('#type').val(data.ad_type);
      $('#area').val(data.ad_area);
      $('#effects').val(data.ad_effects);
      $('#flash_script').text(data.ad_flash_script);
      if(data.ad_type == 'images'){
    		$('#animation_effects').show();
    		$('#animation_effects').removeClass('hide fade');
    		$('#flash_content').hide();
    		$('#flash_content').addClass('hide fade');
      } 
      else {
    	  $('#animation_effects').hide();  
    	  $('#animation_effects').addClass('hide fade');
    	  $('#flash_content').show();
  			$('#flash_content').removeClass('hide fade');
      }
  });
  
  $('#modal-ad').modal({'show':true,'keyboard':false}).css({
        width: '400px',
        'margin-top': function () {
            return -($(this).height() / 2);
        },
        'margin-left': function () {
            return -($(this).width() / 2);
        }
    });
});

$(document).on('click', '.view', function(e) {
  e.preventDefault();

  var ad_id = $(this).attr('id');

	$('#modal-ad-view').modal({'show':true,'keyboard':false}).css({
        width: '300px',
        'margin-top': function () {
            return -($(this).height() / 2);
        },
        'margin-left': function () {
            return -($(this).width() / 2);
        }
    });

  $('#modal-ad-view').on('shown', function(){
	  var script = "<div id=\"ad_slot\"></div>\r\n"; 
	  script += "<script type=\"text/javascript\">\r\n";
	  script += "  $(document).ready(function(){\r\n";
	  script += "    loadAd('ad_slot',"+ad_id+");\r\n";
	  script += "  });\r\n";
	  script += "<\/script>";
	  
	  $('#preview').text(script);
		prettyPrint();
	});
});



$(document).on('click', '.delete', function(e) {
  e.preventDefault();

  var ad_id = $(this).attr('id');
  $('#ad_delete_id').val(ad_id);
  $('#modal-confirm-delete').modal('show');
});

$(document).on('change', 'select[id="type"]', function(e){
	var selected = $('select[id="type"] option:selected').val();
	if(selected == 'images') {
		$('#animation_effects').show();
		$('#animation_effects').removeClass('hide fade');
		$('#flash_content').hide();
		$('#flash_content').addClass('hide fade');
	}
	else {
		$('#animation_effects').hide();
		$('#animation_effects').addClass('hide fade');
		$('#flash_content').show();
		$('#flash_content').removeClass('hide fade');
	}
});

</script>