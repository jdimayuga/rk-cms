<div class="span8">
	 <?php if(validation_errors()):?>
     	<div class="alert alert-error">
     		<button data-dismiss="alert" class="close">&times;</button>
     		<strong>Form Errors:</strong>
  			<?php echo validation_errors() ?> 
  		</div>
     <?php endif;?>
     <?php if(isset($success) && $success):?>
     	<div class="alert alert-success">
     		<button data-dismiss="alert" class="close">&times;</button>
     		<strong>Success:</strong>
  			<?php echo $successMessage; ?>
  		</div>
		 <?php endif;?>
	<div class="pull-right">
		<a class="btn new" href="#"><i class="icon-plus"></i> New Form</a>
	</div>
	<table class="table table-striped">
  	<thead>
  		<tr>
  			<th>Form ID</th>
  			<th>Form Name</th>
  			<th>Tasks</th>
  		</tr>
  	</thead>
  	<tbody>
  		<?php if(empty($forms)):?>
  		<tr>
  			<td colspan="4">
  				No forms yet.
  			</td>
  		</tr>
  		<?php else:?>
    		<?php foreach($forms as $form):?>
      		<tr>
      			<td><?php echo $form['form_id']?></td>
      			<td><?php echo $form['form_name']?></td>
      			<td>
      			  <a class="btn btn-small view" href="#" id="<?php echo $form['form_id']?>"><i class="icon-list-alt"></i> View Code</a>
      				<a class="btn btn-small edit" href="#" id="<?php echo $form['form_id']?>"><i class="icon-edit"></i> Edit</a>
        			<a class="btn btn-small delete" href="#" id="<?php echo $form['form_id']?>"><i class="icon-trash"></i> Delete</a>
      			</td>
      		</tr>
    		<?php endforeach;?>
  		<?php endif;?>
  	</tbody>
  </table>
  <div class="pagination pull-right">
    <?php echo $this->pagination->create_links(); ?>
  </div>
</div>

<div id="modal-form" class="modal hide fade">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h3>Form</h3>
    </div>
    <div class="modal-body">
    	<?php echo form_open('widgets/saveform'); ?>
       <input type="hidden" id="form_id"  name="id" value="">
       <label>Form Name</label>
       <input type="text" name="name" id="name" class="span12 <?php echo form_error('name') ? 'red' : ''; ?>"/>
       <div class="tabbable"> <!-- Only required for left/right tabs -->
          <ul class="nav nav-tabs">
          	<li  class="active"><a href="#tab1" data-toggle="tab">Content</a></li>
            <li><a href="#tab2" data-toggle="tab">Settings</a></li>
          </ul>
          <div class="tab-content">
          	<div class="tab-pane active" id="tab1">
          		<textarea name="content" rows="10" id="content" class="ckeditor span9"></textarea>
            </div>
            <div class="tab-pane" id="tab2">
        			<label>Form Scripts</label>
              <textarea name="scripts" rows="5" id="scripts" class="span12"></textarea>
             	
             	<label>Form Post Message</label>
              <input type="text" name="post_message" id="post_message" class="span12"/>
            </div>
          </div>
       </div>
       <div class="modal-footer">
          <a href="#" class="btn" data-dismiss="modal">Cancel</a>
          <button class="btn btn-primary" type="submit">Save Changes</button>
       </div>
       </form>
    </div>
</div> 


<div id="modal-form-view" class="modal hide fade">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h3>Form Code Preview</h3>
    </div>
    <div class="modal-body">
    	<div class="span12">
    		<pre id="preview" class="prettyprint"></pre>
    		<p class="help-block">Copy this code and place it in site content.</p>
    		<br/>
    	</div>
    	
      <div class="modal-footer">
          <a href="#" class="btn" data-dismiss="modal">Close</a>
       </div>
    </div>
</div> 

<div id="modal-confirm-delete" class="modal hide fade">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h3>Delete Form</h3>
    </div>
    <div class="modal-body">
      <p>You are about to delete this form. Are you sure you want to continue?</p>
    </div>
    <div class="modal-footer">
    	<?php echo form_open('widgets/deleteform'); ?>
    	<input type="hidden" name="id" id="formDeleteId" value="">
    	<button class="btn btn-danger" type="submit">Yes</button>
      <a href="#" class="btn secondary" data-dismiss="modal">No</a>
      </form>
    </div>
</div>
<script type="text/javascript">
$(document).on('click', '.new', function(e) {
	e.preventDefault();
	$('#form_id').val('');
	$('#form_name').val('');
	$('#name').val('');
	$('#scripts').val('');
	$('#post_message').val('');

	CKEDITOR.instances.content.setData('', function()
		    {
		        this.checkDirty();  // true
		    });
	
	
	$('#modal-form').modal({'show':true,'keyboard':false}).css({
        width: '900px',
        'margin-top': function () {
            return -($(this).height() / 2);
        },
        'margin-left': function () {
            return -($(this).width() / 2);
        }
    });
});


$(document).on('click', '.edit', function(e) {
	e.preventDefault();

	var formId = $(this).attr('id');
	$('#form_id').val(formId);
	
	$.getJSON('<?php echo base_url()?>widgets/formview', { id: formId }, function(data) {
			$('#form_id').val(data.form_id);
  		$('#name').val(data.form_name);
  		$('#scripts').val(data.form_scripts);
  		$('#post_message').val(data.form_post_message);

  		CKEDITOR.instances.content.setData(data.form_content, function()
  	    	    {
  	    	        this.checkDirty();  // true
  	    	    });
  		    
  });
	
	$('#modal-form').modal({'show':true,'keyboard':false}).css({
        width: '900px',
        'margin-top': function () {
            return -($(this).height() / 2);
        },
        'margin-left': function () {
            return -($(this).width() / 2);
        }
    });
});

$(document).on('click', '.view', function(e) {
	e.preventDefault();

	var formId = $(this).attr('id');

	//$.getJSON('<?php echo base_url()?>widgets/formview', { id: formId }, function(data) {
		//$('#preview').html(data.form_preview);
	//});
	
	$('#modal-form-view').modal({'show':true,'keyboard':false}).css({
        width: '300px',
        'margin-top': function () {
            return -($(this).height() / 2);
        },
        'margin-left': function () {
            return -($(this).width() / 2);
        }
    });

	$('#modal-form-view').on('shown', function(){
		var script = '<div id="form_slot"></div> \r\n';
		script += '<script type="text/javascript"> \r\n';
		script += '  $(document).ready(function() { \r\n';
		script += '    renderForm(\'form_slot\','+formId+'); \r\n';
		script += '  });\r\n';
		script += '<\/script>';
		$('#preview').text(script);
		prettyPrint();
	});
});

$(document).on('click', '.delete', function(e) {
	e.preventDefault();

	var formId = $(this).attr('id');
	$('#formDeleteId').val(formId);
	$('#modal-confirm-delete').modal('show');
});

$(document).ready(function(){


	
	CKEDITOR.replace( 'content',
	{
			toolbar :
			[
				{ name: 'document', items : [ 'Source'] },
				{ name: 'forms', items : ['Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'HiddenField' ] },
				{ name: 'basicstyles', items : [ 'Bold','Italic','Strike','-' ] }
			],
			enterMode : CKEDITOR.ENTER_BR,
	    shiftEnterMode: CKEDITOR.ENTER_P
	});

	

	
});

</script>