<div class="span8">
	 <?php if(validation_errors()):?>
     	<div class="alert alert-error">
     		<button data-dismiss="alert" class="close">&times;</button>
     		<strong>Form Errors:</strong>
  			<?php echo validation_errors() ?> 
  		</div>
     <?php endif;?>
     <?php if($this->session->flashdata('success')):?>
     	<div class="alert alert-success">
     		<button data-dismiss="alert" class="close">&times;</button>
     		<?php echo $this->session->flashdata('successMessage') ?>
  		</div>
		 <?php endif;?>
	<div class="pull-right">
		<a class="btn new" href="#"><i class="icon-plus"></i> New Gallery</a>
	</div>
	<table class="table table-striped">
  	<thead>
  		<tr>
  			<th>Gallery ID</th>
  			<th>Gallery Name</th>
  			<th>Large Cover</th>
  			<th>Small Cover</th>
  			<th>Tasks</th>
  		</tr>
  	</thead>
  	<tbody class="files" data-target="#modal-gallery" data-toggle="modal-gallery" data-selector=".gallery-item">
  		<?php if(empty($galleries)):?>
  		<tr>
  			<td colspan="4">
  				No gallery yet.
  			</td>
  		</tr>
  		<?php else:?>
    		<?php foreach($galleries as $gallery):?>
      		<tr>
      			<td><?php echo $gallery['gallery_id']?></td>
      			<td><?php echo $gallery['gallery_name']?></td>
      			<td>
      				<a class="gallery-item" href="<?php echo base_url()?>../assets/uploads/gallery_covers/large/<?php echo $gallery['gallery_cover_large'].'_thumb'.$gallery['gallery_cover_large_ext']?>">
    				  	<img class="thumbnail" alt="" src="<?php echo base_url()?>../assets/uploads/gallery_covers/large/<?php echo $gallery['gallery_cover_large'].'_thumb'.$gallery['gallery_cover_large_ext']?>" width="100"/>
    				  </a>
    				  <a class="gallery-item" href="<?php echo base_url()?>../assets/uploads/gallery_covers/large/<?php echo $gallery['gallery_cover_large'].'_thumb'.$gallery['gallery_cover_large_ext']?>"><?php echo $gallery['gallery_cover_large'].$gallery['gallery_cover_large_ext']?></a>
      			</td>
      			<td>
      				<a class="gallery-item" href="<?php echo base_url()?>../assets/uploads/gallery_covers/small/<?php echo $gallery['gallery_cover_small'].'_thumb'.$gallery['gallery_cover_small_ext']?>">
    				  	<img class="thumbnail" alt="" src="<?php echo base_url()?>../assets/uploads/gallery_covers/small/<?php echo $gallery['gallery_cover_small'].'_thumb'.$gallery['gallery_cover_small_ext']?>" width="100"/>
    				  </a>
    				  <a class="gallery-item" href="<?php echo base_url()?>../assets/uploads/gallery_covers/small/<?php echo $gallery['gallery_cover_small'].'_thumb'.$gallery['gallery_cover_small_ext']?>"><?php echo $gallery['gallery_cover_small'].$gallery['gallery_cover_small_ext']?></a>
      			</td>
      			<td>
      			  <a class="btn btn-small view" href="#" id="<?php echo $gallery['gallery_id']?>"><i class="icon-list-alt"></i> View Code</a>
      				<a class="btn btn-small edit" href="#" id="<?php echo $gallery['gallery_id']?>"><i class="icon-edit"></i> Edit</a>
        			<a class="btn btn-small delete" href="#" id="<?php echo $gallery['gallery_id']?>"><i class="icon-trash"></i> Delete</a>
        			<a class="btn btn-small photos" href="<?php echo base_url()?>widgets/galleryupload/<?php echo $gallery['gallery_id']?>" id="<?php echo $gallery['gallery_id']?>"><i class="icon-picture"></i>Photos</a>
      			</td>
      		</tr>
    		<?php endforeach;?>
  		<?php endif;?>
  	</tbody>
  </table>
  <div class="pagination pull-right">
    <?php echo $this->pagination->create_links(); ?>
  </div>
</div>

<div id="modal-gallery-dialog" class="modal hide fade">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h3>Gallery</h3>
    </div>
    <div class="modal-body">
    	<?php  echo form_open_multipart('widgets/savegallery'); ?>
       <input type="hidden" id="gallery_id"  name="id" value="">
       <label>Section</label>
       <select id="section" name="section">
    		<?php foreach ($sections as $section):?>
    			<option value="<?php echo $section['section_id']?>"><?php echo $section['section_name']?></option>
    		<?php endforeach;?>
       </select>
       <label>Gallery Name</label>
       <input type="text" name="name" id="name" class="span12 <?php echo form_error('name') ? 'red' : ''; ?>"/>
       <label>Description</label>
       <textarea rows="4" class="span12" name="gallery_desc" id="gallery_desc"></textarea>
       <label>Article (Optional)</label>
       <select id="article" name="article">
       	 <option value="">None</option>
    		<?php foreach ($articles as $article):?>
    			<option value="<?php echo $article['article_id']?>"><?php echo $article['article_title']?></option>
    		<?php endforeach;?>
       </select>
       <label>Large Cover Photo (315 px by 345 px)</label>
       <input type="file" name="cover_large">
       <label>Small Cover Photo (154 px by 138 px)</label>
       <input type="file" name="cover_small">
       <div class="modal-footer">
          <a href="#" class="btn" data-dismiss="modal">Cancel</a>
          <button class="btn btn-primary" type="submit">Save Changes</button>
       </div>
       </form>
    </div>
</div> 


<div id="modal-gallery-view" class="modal hide fade">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h3>Gallery Code Preview</h3>
    </div>
    <div class="modal-body">
    	<div class="span12">
    		<pre id="preview" class="prettyprint"></pre>
    		<p class="help-block">Copy this code and place it in site content.</p>
    		<br/>
    	</div>
    	
      <div class="modal-footer">
          <a href="#" class="btn" data-dismiss="modal">Close</a>
       </div>
    </div>
</div> 

<div id="modal-confirm-delete" class="modal hide fade">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h3>Delete Gallery</h3>
    </div>
    <div class="modal-body">
      <p>You are about to delete this gallery. Are you sure you want to continue?</p>
    </div>
    <div class="modal-footer">
    	<?php echo form_open('widgets/deletegallery'); ?>
    	<input type="hidden" name="id" id="galleryDeleteId" value="">
    	<button class="btn btn-danger" type="submit">Yes</button>
      <a href="#" class="btn secondary" data-dismiss="modal">No</a>
      </form>
    </div>
</div>
<script type="text/javascript">
$(document).on('click', '.new', function(e) {
	e.preventDefault();
	$('#gallery_id').val('');
	$('#name').val('');
	$('#gallery_desc').val('');
	$("#section")[0].selectedIndex = 0;
	$("#article")[0].selectedIndex = 0;
	
	$('#modal-gallery-dialog').modal({'show':true,'keyboard':false}).css({
        width: '900px',
        'margin-top': function () {
            return -($(this).height() / 2);
        },
        'margin-left': function () {
            return -($(this).width() / 2);
        }
    });
});


$(document).on('click', '.edit', function(e) {
	e.preventDefault();

	var galleryId = $(this).attr('id');
	$('#gallery_id').val(galleryId);
	
	$.getJSON('<?php echo base_url()?>widgets/galleryview', { id: galleryId }, function(data) {
			$('#gallery_id').val(data.gallery_id);
  		$('#name').val(data.gallery_name);
  		$('#gallery_desc').val(data.gallery_desc);
  		$("#section").val(data.gallery_section);
  		$("#article").val(data.gallery_article_id);
  });
	
	$('#modal-gallery-dialog').modal({'show':true,'keyboard':false}).css({
        width: '900px',
        'margin-top': function () {
            return -($(this).height() / 2);
        },
        'margin-left': function () {
            return -($(this).width() / 2);
        }
    });
});

$(document).on('click', '.view', function(e) {
	e.preventDefault();

	var galleryId = $(this).attr('id');

	$('#modal-gallery-view').modal({'show':true,'keyboard':false}).css({
        width: '320px',
        'margin-top': function () {
            return -($(this).height() / 2);
        },
        'margin-left': function () {
            return -($(this).width() / 2);
        }
    });

	$('#modal-gallery-view').on('shown', function(){
		  var script = "<div id=\"gallery_slot\"></div>\r\n"; 
		  script += "<script type=\"text/javascript\">\r\n";
		  script += "  $(document).ready(function(){\r\n";
		  script += "    loadGallery('gallery_slot',"+galleryId+",5);\r\n";
		  script += "  });\r\n";
		  script += "<\/script>";
			
			$('#preview').text(script);
			prettyPrint();
	});
});

$(document).on('click', '.delete', function(e) {
	e.preventDefault();

	var galleryId = $(this).attr('id');
	$('#galleryDeleteId').val(galleryId);
	$('#modal-confirm-delete').modal('show');
});

</script>