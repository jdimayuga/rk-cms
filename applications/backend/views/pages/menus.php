<div class="container-fluid">
  <div class="row-fluid">
  	<h3>Menus</h3>
  	<hr/>
	 <?php if(validation_errors()):?>
   	<div class="alert alert-error">
   		<button data-dismiss="alert" class="close">&times;</button>
   		<strong>Form Errors:</strong>
			<?php echo validation_errors() ?> 
		</div>
   <?php endif;?>
   <?php if(isset($success) && $success):?>
   	<div class="alert alert-success">
   		<button data-dismiss="alert" class="close">&times;</button>
   		<strong>Success:</strong>
			<?php echo $successMessage; ?>
		</div>
	 <?php endif;?>
  	<div class="pull-right">
  		<a class="btn new" href="#"><i class="icon-plus"></i> New Menu</a>
  	</div>
  	<table class="table table-striped">
    	<thead>
    		<tr>
    			<th>Menu[Sort Order]</th>
    			<th>Name</th>
    			<th>Path</th>
    			<th>Tasks</th>
    		</tr>
    	</thead>
    	<tbody>
    		<?php if(empty($menus)):?>
    		<tr>
    			<td colspan="4">
    				No menus yet.
    			</td>
    		</tr>
    		<?php else:?>
    			<?php foreach($menus as $menu):?>
    				<tr>
    					<td><?php echo empty($menu['parent_menu_name']) ? 'Main' : $menu['parent_menu_name'] ?>[<?php echo $menu['menu_sort']?>]</td>
    					<td><?php echo $menu['menu_name']?></td>
    					<td><?php echo $menu['page_url']?></td>
    					<td>
    						<a class="btn btn-small edit" href="#" id="<?php echo $menu['menu_id']?>"><i class="icon-edit"></i> Edit</a>
        				<a class="btn btn-small delete" href="#" id="<?php echo $menu['menu_id']?>"><i class="icon-trash"></i> Delete</a>
    					</td>
    				</tr>
    			<?php endforeach;?>
    		<?php endif;?>
    	</tbody>
    </table>
    <div class="pagination pull-right">
      <?php echo $this->pagination->create_links(); ?>
    </div>
  </div>
</div> 

<div id="modal-menu" class="modal hide fade">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h3>Menu</h3>
    </div>
    <div class="modal-body">
    	<?php echo form_open('menus/save'); ?>
          <input type="hidden" id="menu_id"  name="id" value="">
          
          <label>Menu Name</label>
          <input type="text" id="name" name="name" class="span4 <?php echo form_error('name') ? 'red' : ''; ?>" autocomplete="off" placeholder="" value="">
        	
        	<label>Sort Order</label>
          <input type="number" id="sort" name="sort" class="span1" autocomplete="off" value="0">
        	
        	<label>Head Menu</label>
        	<?php echo form_dropdown('parent',$parents, $parentSelected,'id="parent"')?>
        	
        	<label>Path</label>
          <input type="text" id="path" name="path" class="span4 <?php echo form_error('name') ? 'red' : ''; ?>" autocomplete="off" value="">
        
        <div class="modal-footer">
          <a href="#" class="btn" data-dismiss="modal">Cancel</a>
          <button class="btn btn-primary" type="submit">Save Changes</button>
        </div>
        </form>
    </div>
</div> 

<div id="modal-confirm-delete" class="modal hide fade">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h3>Delete Menu</h3>
    </div>
    <div class="modal-body">
      <p>You are about to delete this menu. Are you sure you want to continue?</p>
    </div>
    <div class="modal-footer">
    	<?php echo form_open('menus/delete'); ?>
    	<input type="hidden" name="id" id="menuDeleteId" value="">
    	<button class="btn btn-danger" type="submit">Yes</button>
      <a href="#" class="btn secondary" data-dismiss="modal">No</a>
      </form>
    </div>
</div>   

<script type="text/javascript">
$(document).on('click', '.new', function(e) {
	e.preventDefault();
	$('#menu_id').val('');
	$('#name').val('');
	$('#sort').val('');
	$('#path').val('');
	$('#parent option:eq(0)').attr('selected', 'selected') 
	
	$('#modal-menu').modal({'show':true,'keyboard':false}).css({
        width: '400px',
        'margin-top': function () {
            return -($(this).height() / 2);
        },
        'margin-left': function () {
            return -($(this).width() / 2);
        }
    });
});


$(document).on('click', '.edit', function(e) {
	e.preventDefault();
	
	var menuId = $(this).attr('id');

	$('#menu_id').val(menuId);
	
	$.getJSON('<?php echo base_url()?>menus/view', { id: menuId }, function(data) {
			$('#menu_id').val(data.menu_id);
  		$('#name').val(data.menu_name);
  		$('#sort').val(data.menu_sort);
  		$('#path').val(data.page_url);
  		$('#parent').val(data.menu_parent);
  		//$('#parent option:eq('+data.menu_parent+')').attr('selected', 'selected') 
	 });

	$('#modal-menu').modal({'show':true,'keyboard':false}).css({
        width: '400px',
        'margin-top': function () {
            return -($(this).height() / 2);
        },
        'margin-left': function () {
            return -($(this).width() / 2);
        }
    });
});


$(document).on('click', '.delete', function(e) {
	e.preventDefault();

	var menuId = $(this).attr('id');
	$('#menuDeleteId').val(menuId);
	$('#modal-confirm-delete').modal('show');
});
</script>