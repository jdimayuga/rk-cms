<div class="container-fluid">
  <div class="row-fluid">
  	<h3>Widgets &raquo; 
  	<?php if(!isset($active)):?>
  		Forms
  	<?php else:?>
  		<?php echo ucfirst($active);?>
  	<?php endif;?>
  		</h3>
  	<hr/>
  	<div class="span3">
  		<ul class="nav nav-tabs nav-stacked">
  			<li <?php echo !isset($active) ? 'class="active"' : ''; ?>><a href="<?php echo base_url()?>widgets/index">Forms</a></li>
  			<li <?php echo (isset($active) && $active == 'gallery') ? 'class="active"' : ''; ?>><a href="<?php echo base_url()?>widgets/gallery">Gallery</a></li>
  			<li <?php echo (isset($active) && $active == 'advertisements') ? 'class="active"' : ''; ?>><a href="<?php echo base_url()?>widgets/advertisements">Advertisements</a></li>
  			<li <?php echo (isset($active) && $active == 'videos') ? 'class="active"' : ''; ?>><a href="<?php echo base_url()?>widgets/videos">Videos</a></li>
  		</ul>
  	</div>
  	<?php echo $widget_section; ?>
  </div>
</div>  

