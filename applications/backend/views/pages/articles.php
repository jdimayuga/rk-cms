<div class="container-fluid">
  <div class="row-fluid">
  	<h3>Articles</h3>
  	<hr/>
  	 <?php if(validation_errors()):?>
     	<div class="alert alert-error">
     		<button data-dismiss="alert" class="close">&times;</button>
     		<strong>Form Errors:</strong>
  			<?php echo validation_errors() ?> 
  		</div>
     <?php endif;?>
     <?php if(isset($success) && $success):?>
     	<div class="alert alert-success">
     		<button data-dismiss="alert" class="close">&times;</button>
     		<strong>Success:</strong>
  			<?php echo $successMessage; ?>
  		</div>
		 <?php endif;?>
  	
  	<div class="pull-right">
  		<a class="btn new" href="#"><i class="icon-plus"></i> New Article</a>
  	</div>
  	
    <table class="table">
    	<thead>
    		<tr>
    			<th>Thumbnail</th>
    			<th>Title</th>
    			<th>Date Posted</th>
    			<th>Sequence Order</th>
    			<th>Published</th>
    			<th>Tags</th>
    			<th>Feature</th>
    			<th>Tasks</th>
    		</tr>
    	</thead>
    	<tbody class="files" data-target="#modal-gallery" data-toggle="modal-gallery" data-selector=".gallery-item">
    		<?php if(!empty($articles)):?>
      		<?php foreach($articles as $article):?>
      			<tr>
      				<td>
      					 <?php if(!empty($article['article_image'])):?>
      				  	<a class="gallery-item" href="<?php echo base_url()?>../assets/uploads/articles/thumb/<?php echo $article['article_image'].$article['article_image_ext']?>">
        				  	<img class="thumbnail" alt="" src="<?php echo base_url()?>../assets/uploads/articles/thumb_2/<?php echo $article['article_image_thumb'].$article['article_image_ext']?>"/>
        				  </a>
        				  <a class="gallery-item" href="<?php echo base_url()?>../assets/uploads/articles/<?php echo $article['article_image'].$article['article_image_ext']?>"><?php echo $article['article_image'].$article['article_image_ext']?></a>
        				<?php else:?>
      				  	<a  href="">
        				  	<img class="thumbnail" alt="" src="http://placehold.it/160x120/160x120">
        				  </a>
        				<?php endif;?>
      				</td>
        			<td><?php echo $article['article_title'] ?></td>
        			<td><?php echo $article['date_created'] ?></td>
        			<td><?php echo $article['article_order'] ?></td>
        			<td><?php echo $article['article_display'] == 1 ? '<i class="icon-ok"></i>' : '' ?></td>
        			<td><?php echo $article['article_tags'] ?></td>
        			<td>
        				<div class="btn-group" data-toggle="buttons-checkbox">
                  <button id="<?php echo $article['article_id']?>" class="btn main_featured <?php echo $article['featured'] == 1 ? 'btn-primary active' : ''?>" data-content="The main featured article at the homepage." rel="popover" title="Main Featured Article">Main</button>
                  <button id="<?php echo $article['article_id']?>" class="btn sub_featured_1 <?php echo $article['sub_featured_1'] == 1 ? 'btn-primary active' : ''?>" data-content="The two sub featured article below the main featured." rel="popover" title="Sub Featured Article">Top</button>
                  <button id="<?php echo $article['article_id']?>" class="btn sub_featured_2 <?php echo $article['sub_featured_2'] == 1 ? 'btn-primary active' : ''?>" data-content="The two sub featured article at the bottom." rel="popover" title="Sub Featured Article">Bottom</button>
                </div>
        			</td>
        			<td>
        			  <a class="btn btn-small url" href="#" id="<?php echo $article['article_id']?>"><i class="icon-edit"></i> View URL</a>
        			  <a class="btn btn-small edit" href="#" id="<?php echo $article['article_id']?>"><i class="icon-edit"></i> Edit</a>
        				<a class="btn btn-small delete" href="#" id="<?php echo $article['article_id']?>"><i class="icon-trash"></i> Delete</a>
        			</td>
        		</tr>
        		
      		<?php endforeach;?>
    		<?php else:?>
    			<tr>
    				<td colspan="8">
    					No articles yet.
    				</td>
    			</tr>
    		<?php endif;?>
    		
    	</tbody>
    </table>
    <div class="pagination pull-right">
      <?php echo $this->pagination->create_links(); ?>
    </div>
  </div>
</div>

<div id="modal-article" class="modal hide fade">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h3>Article</h3>
    </div>
    <div class="modal-body">
    	<?php echo form_open_multipart('articles/save'); ?>
          <label>Article Title</label>
          <input type="hidden" id="article_id"  name="id" value="">
        	<input type="text" id="title" name="title" class="span9 <?php echo form_error('title') ? 'red' : ''; ?>" autocomplete="off" placeholder="" value="">
          <div class="tabbable"> <!-- Only required for left/right tabs -->
              <ul class="nav nav-tabs">
              	<li class="active"><a href="#tab1" data-toggle="tab">Summary</a></li>
                <li><a href="#tab2" data-toggle="tab">Content</a></li>
                <li><a href="#tab3" data-toggle="tab">Settings</a></li>
              </ul>
              <div class="tab-content">
              	<div class="tab-pane active" id="tab1">
              		<textarea name="summary" rows="10" id="summary" class="ckeditor span9"></textarea>
                </div>
                <div class="tab-pane" id="tab2">
                	<label>Article Image</label>
              		<input type="file" name="image">
                  <textarea name="content" rows="10" id="content" class="ckeditor span9"></textarea>
                </div>
                <div class="tab-pane" id="tab3">
                	<label>Section</label>
                	<select id="section" name="section">
                		<?php foreach ($sections as $section):?>
                			<option value="<?php echo $section['section_id']?>"><?php echo $section['section_name']?></option>
                		<?php endforeach;?>
                	</select>
                	<label>Author</label>
                	<input id="author" name="author" class="span9" type="text"/>
                	<label>Publish Date</label>
                	<input id="date" name="date" class="span2 datepicker" type="text" data-date-format="mm/dd/yyyy" value="<?php echo date('m/d/Y') ?>">
                	<label>Tags (comma separated)</label>
                  <input type="text" id="tags" data-provide="typeahead" name="tags" class="span9" autocomplete="off" value=""/>
                  
                  <label>SEO</label>
                  <textarea name="seo" rows="5" id="seo" class="span9"></textarea>
                  
                  <label class="checkbox">
                  	<input type="checkbox" name="visible" id="visible"> Publish Page
                  </label>
                  
                  <label>Order (1-9999)</label>
                  <input type="number" id="order" name="order" class="span1" autocomplete="off" value=""/>
                </div>
              </div>
        	</div>
        <div class="modal-footer">
          <a href="#" class="btn" data-dismiss="modal">Cancel</a>
          <button class="btn btn-primary" type="submit">Save Changes</button>
        </div>
        </form>
    </div>
</div> 

<div id="modal-confirm-delete" class="modal hide fade">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h3>Delete Article</h3>
    </div>
    <div class="modal-body">
      <p>You are about to delete this article. Are you sure you want to continue?</p>
    </div>
    <div class="modal-footer">
    	<?php echo form_open('articles/delete'); ?>
    	<input type="hidden" name="id" id="articleDeleteId" value="">
    	<button class="btn btn-danger" type="submit">Yes</button>
      <a href="#" class="btn secondary" data-dismiss="modal">No</a>
      </form>
    </div>
</div>

<div id="modal-copy-url" class="modal hide fade">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h3>Page URL</h3>
    </div>
    <div class="modal-body">
      <p id="page_url"></p>
    </div>
    <div class="modal-footer">
      <span style="position:relative">
				<a class="btn btn-small copy-url" href="#"><i class="icon-download"></i> Copy URL</a>
	  	</span>
      <a href="#" class="btn secondary" data-dismiss="modal">Close</a>
    </div>
</div>
   

<script type="text/javascript">

$(document).on('click', '.new', function(e) {
	e.preventDefault();
	$('#article_id').val('');
	$('#tags').val('');
	$('#seo').val('');
	$('#title').val('');
	$('#order').val('');
	$('#author').val('');
	$("#section")[0].selectedIndex = 0;
	$('#visible').removeAttr('checked');
	$('#date').val('<?php echo date('m/d/Y') ?>');
	$('#date').datepicker().on('changeDate', function(ev){
		$(this).datepicker('hide');
	});
	
	CKEDITOR.instances.summary.setData('', function()
    {
        this.checkDirty();  // true
    });
  
	CKEDITOR.instances.content.setData('', function()
  	    {
  	        this.checkDirty();  // true
  	    });
	
	$('#modal-article').modal({'show':true,'keyboard':false}).css({
        width: '900px',
        'margin-top': function () {
            return -($(this).height() / 2);
        },
        'margin-left': function () {
            return -($(this).width() / 2);
        }
    });
});


$(document).on('click', '.url', function(e) {
	e.preventDefault();
	var articleId = $(this).attr('id');

	$.getJSON('<?php echo base_url()?>articles/view', { id: articleId }, function(data) {
		var url = '<?php echo base_url('articles')?>/view/' + data.article_title_slug + '/' + articleId;
  	$('#page_url').html(url);
  });

	$('#modal-copy-url').modal({'show':true,'keyboard':false}).css({
        width: '400px',
        'margin-top': function () {
            return -($(this).height() / 2);
        },
        'margin-left': function () {
            return -($(this).width() / 2);
        }
    });
});


$('#modal-copy-url').on('shown', function(){
	var pageURL = $('#page_url').text();
	$('a.copy-url').zclip({
  		path:"<?=base_url()?>../assets/js/ZeroClipboard.swf",
  		copy:pageURL,
  		afterCopy: function(){
				//alert('Copied Page URL');
	  	}
		});
});

$(".main_featured").on('hover', function() {
	 $(this).popover('show');
});

$(".main_featured").on('click', function(e) {
		e.preventDefault();
		var articleId = $(this).attr('id');
		var element = $(this);
		if(!$(element).hasClass('active')) {
			//activate
			$.getJSON('<?php echo base_url()?>articles/toggle_featured', { id: articleId, type: "featured", status: 1 }, function(data) {
				if(!data.status) {
					alert('There is already a main featured article. Please deactivate that article first if you wish to feature this article');
					$(element).removeClass('active');
				}
				else {
					$(element).addClass('btn-primary');
				}
				
		  });
		}
		else {
			//deactivate
			$.getJSON('<?php echo base_url()?>articles/toggle_featured', { id: articleId, type: "featured", status: 0 }, function(data) {
				$(element).removeClass('btn-primary');
		  });
		}
});

$(".sub_featured_1").on('hover', function() {
	 $(this).popover('show');
});

$(".sub_featured_1").on('click', function(e) {
	e.preventDefault();
	var articleId = $(this).attr('id');
	var element = $(this);
	if(!$(element).hasClass('active')) {
		//activate
		$.getJSON('<?php echo base_url()?>articles/toggle_featured', { id: articleId, type: "sub_featured_1", status: 1 }, function(data) {
			if(!data.status) {
				alert('There is already two featured top article. Please deactivate a top article first if you wish to feature this article');
				$(element).removeClass('active');
			}
			else {
				$(element).addClass('btn-primary');
			}
			
	  });
	}
	else {
		//deactivate
		$.getJSON('<?php echo base_url()?>articles/toggle_featured', { id: articleId, type: "sub_featured_1", status: 0 }, function(data) {
			$(element).removeClass('btn-primary');
	  });
	}
});

$(".sub_featured_2").on('hover', function() {
	 $(this).popover('show');
});

$(".sub_featured_2").on('click', function(e) {
	e.preventDefault();
	var articleId = $(this).attr('id');
	var element = $(this);
	if(!$(element).hasClass('active')) {
		//activate
		$.getJSON('<?php echo base_url()?>articles/toggle_featured', { id: articleId, type: "sub_featured_2", status: 1 }, function(data) {
			if(!data.status) {
				alert('There is already two featured bottom article. Please deactivate a bottom article first if you wish to feature this article');
				$(element).removeClass('active');
			}
			else {
				$(element).addClass('btn-primary');
			}
			
	  });
	}
	else {
		//deactivate
		$.getJSON('<?php echo base_url()?>articles/toggle_featured', { id: articleId, type: "sub_featured_2", status: 0 }, function(data) {
			$(element).removeClass('btn-primary');
	  });
	}
});



$(document).on('click', '.edit', function(e) {
	e.preventDefault();
	
	var articleId = $(this).attr('id');

	$('#article_id').val(articleId);
	
	$.getJSON('<?php echo base_url()?>articles/view', { id: articleId }, function(data) {
	  	
	  	$('#tags').val(data.article_tags);
	  	$('#seo').val(data.article_seo);
	  	$('#title').val(data.article_title);
	  	$('#order').val(data.article_order);
	  	$('#date').val(data.article_date);
	  	$("#section").val(data.article_section);
	  	$('#author').val(data.article_author);
	  	$('#date').datepicker().on('changeDate', function(ev){
  			$(this).datepicker('hide');
  		});
	  	if(data.article_display == 1) {
		  	$('#visible').attr('checked','checked');
	  	}
	  	else {
	  		$('#visible').removeAttr('checked');
	  	}
	  	
	  	CKEDITOR.instances.summary.setData(data.article_summary, function()
    	    {
    	        this.checkDirty();  // true
    	    });
	    
	  	CKEDITOR.instances.content.setData(data.article_content, function()
	    	    {
	    	        this.checkDirty();  // true
	    	    });
	 });
			
	
	$('#modal-article').modal({'show':true,'keyboard':false}).css({
        width: '900px',
        'margin-top': function () {
            return -($(this).height() / 2);
        },
        'margin-left': function () {
            return -($(this).width() / 2);
        }
    });
});

$(document).on('click', '.delete', function(e) {
	e.preventDefault();

	var articleId = $(this).attr('id');
	$('#articleDeleteId').val(articleId);
	$('#modal-confirm-delete').modal('show');
});

$(document).ready(function(){
	$('#tags').typeahead({
	    source: function(typeahead, query) {
	      var term = $.trim(query.split(',').pop());
	      if (term == '') return [];
	      $.getJSON('<?php echo base_url()?>tags/typeahead', { tag: term }, function(data) {
	        typeahead.process(data);
	      });
	    }
	  , onselect: function(item, previous_items) {
	      terms = previous_items.split(',');
	      terms.pop();
	      terms.push(item);
	      terms.push('');
	      $.each(terms, function(idx, val) { terms[idx] = $.trim(val); });
	      $('#tags').val(terms.join(', '));
	    }
	  // Matcher always returns true since there are multiple comma-seperated terms in the input box and the server ensures only matching terms are returned
	  , matcher: function() { return true; }
	  // Autoselect is disabled so that users can enter new tags
	  , autoselect: false
	  });


	CKEDITOR.replace( 'summary',
  {
    filebrowserBrowseUrl :'<?php echo $this->config->item('frontend_url')?>assets/ckeditor/filemanager/browser/default/browser.html?Connector=<?php echo $this->config->item('frontend_url')?>assets/ckeditor/filemanager/connectors/php/connector.php',
    filebrowserImageBrowseUrl : '<?php echo $this->config->item('frontend_url')?>assets/ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=<?php echo $this->config->item('frontend_url')?>assets/ckeditor/filemanager/connectors/php/connector.php',
    filebrowserFlashBrowseUrl :'<?php echo $this->config->item('frontend_url')?>assets/ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=<?php echo $this->config->item('frontend_url')?>assets/ckeditor/filemanager/connectors/php/connector.php',
  	filebrowserUploadUrl  :'<?php echo $this->config->item('frontend_url')?>assets/ckeditor/filemanager/connectors/php/upload.php?Type=File',
  	filebrowserImageUploadUrl : '<?php echo $this->config->item('frontend_url')?>assets/ckeditor/filemanager/connectors/php/upload.php?Type=Image',
  	filebrowserFlashUploadUrl : '<?php echo $this->config->item('frontend_url')?>assets/ckeditor/filemanager/connectors/php/upload.php?Type=Flash'
  });

	CKEDITOR.replace( 'content',
  {
    filebrowserBrowseUrl :'<?php echo $this->config->item('frontend_url')?>assets/ckeditor/filemanager/browser/default/browser.html?Connector=<?php echo $this->config->item('frontend_url')?>assets/ckeditor/filemanager/connectors/php/connector.php',
    filebrowserImageBrowseUrl : '<?php echo $this->config->item('frontend_url')?>assets/ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=<?php echo $this->config->item('frontend_url')?>assets/ckeditor/filemanager/connectors/php/connector.php',
    filebrowserFlashBrowseUrl :'<?php echo $this->config->item('frontend_url')?>assets/ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=<?php echo $this->config->item('frontend_url')?>assets/ckeditor/filemanager/connectors/php/connector.php',
  	filebrowserUploadUrl  :'<?php echo $this->config->item('frontend_url')?>assets/ckeditor/filemanager/connectors/php/upload.php?Type=File',
  	filebrowserImageUploadUrl : '<?php echo $this->config->item('frontend_url')?>assets/ckeditor/filemanager/connectors/php/upload.php?Type=Image',
  	filebrowserFlashUploadUrl : '<?php echo $this->config->item('frontend_url')?>assets/ckeditor/filemanager/connectors/php/upload.php?Type=Flash'
  });
	 
});



</script>
