<style type="text/css">
  /* Override some defaults */
  html, body {
    background-color: #eee;
  }
  body {
    padding-top: 100px; 
  }
  .container {
  
    width: 300px;
  }

  /* The white background content wrapper */
  .container > .content {
    background-color: #fff;
    padding: 20px;
    margin: 0 -20px; 
    -webkit-border-radius: 10px 10px 10px 10px;
       -moz-border-radius: 10px 10px 10px 10px;
            border-radius: 10px 10px 10px 10px;
    -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.15);
       -moz-box-shadow: 0 1px 2px rgba(0,0,0,.15);
            box-shadow: 0 1px 2px rgba(0,0,0,.15);
  }

 .login-form {
   margin-left: 65px;
 }
</style>


<div class="container">
    <div class="content">
        <div class="row">
            <div class="login-form">
                <h2>Login</h2>
                <?php if(validation_errors()): ?>
                	<div style="color:red">
                		<?php echo validation_errors();?>
                	</div>
                <?php endif;?>
                <?php echo form_open('login/auth'); ?>
                    <fieldset>
                        <div class="clearfix">
                            <input type="text" name="username" placeholder="Username">
                        </div>
                        <div class="clearfix">
                            <input type="password" name="password" placeholder="Password">
                        </div>
                        <button class="btn btn-primary" type="submit">Sign in</button>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div> <!-- /container -->
