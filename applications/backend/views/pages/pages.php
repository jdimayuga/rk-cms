<div class="container-fluid">
  <div class="row-fluid">
  	
    <div class="span3">
      <div id="accordion2" class="accordion ">
          <div class="accordion-group">
            <div class="accordion-heading">
              <a  data-target="#collapseOne"  data-toggle="collapse" class="accordion-toggle">
               <h4>Pages</h4>
              </a>
            </div>
            <div class="accordion-body in collapse" id="collapseOne" style="height: auto;">
              <div class="accordion-inner">
                 <ul class="nav nav-list">
                 	<?php foreach($pages as $pageItem):?>
                  	<li class="<?php echo (isset($active) && $active === $pageItem['page_id'])  ? 'active' : '' ?>"><a href="<?echo base_url()?>pages/view/<?php echo $pageItem['page_slug']?>"><?php echo $pageItem['page_name'];?></a></li>
                  <?php endforeach;?>
                   <li class="<?php echo !isset($active) ? 'active' : ''?>"><a href="<?php echo base_url()?>"><i class="icon-plus <?php echo !isset($active) ? 'icon-white' : ''?>"></i>New Page</a></li>
                 </ul>
              </div>
            </div>
          </div>
          <br/>
       </div>
    </div><!--/span-->
    <div class="span9">
      <div class="clearfix"></div>
       <?php if(validation_errors()):?>
       	<div class="alert alert-error">
       		<button data-dismiss="alert" class="close">&times;</button>
       		<strong>Form Errors:</strong>
    			<?php echo validation_errors() ?> 
    		</div>
       <?php endif;?>
       <?php if(isset($success) && $success):?>
       	<div class="alert alert-success">
       		<button data-dismiss="alert" class="close">&times;</button>
       		<strong>Success:</strong>
    			<?php echo $successMessage; ?>
    		</div>
			 <?php endif;?>
			 <?php if(isset($page)):?>
       	<?php echo form_open('pages/update'); ?>
       <?php else:?>
       	<?php echo form_open('pages/create'); ?>
       <?php endif;?>
       	<label>Page Title</label>
        <input type="text" name="title" class="span12 <?php echo form_error('title') ? 'red' : ''; ?>" autocomplete="off" placeholder="" value="<?php echo isset($page) ? $page['page_name'] : ''; ?>">
      	<?php if(isset($page)):?>
      		<input type="hidden" name="id" value="<?php echo $page['page_id']?>"/>
      	<?php endif;?>
      	<div class="tabbable"> <!-- Only required for left/right tabs -->
          <ul class="nav nav-tabs">
            <li class="pull-right"><a href="#tab2" data-toggle="tab">Settings</a></li>
            <li class="active pull-right"><a href="#tab1" data-toggle="tab">Content</a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab1">
              <textarea name="content" rows="15" id="textarea" class="ckeditor span12"><?php echo isset($page) ? $page['page_content'] : '' ?></textarea>
            </div>
            <div class="tab-pane" id="tab2">
            	<label>Tags (comma separated)</label>
              <input type="text" id="page_tags" data-provide="typeahead" name="tags" class="span12" autocomplete="off" value="<?php echo isset($page) ? $page['page_tags'] : ''; ?>"/>
              
              <label>SEO</label>
              <textarea name="seo" rows="10" id="textarea2" class="span12"><?php echo isset($page) ? $page['page_seo'] : '' ?></textarea>
              
              <label class="checkbox">
              	<input type="checkbox" name="visible" <?php echo (isset($page) && $page['page_visible'] == 1) ? 'checked="checked"' : ''; ?>> Publish Page
              </label>
            </div>
          </div>
          <?php if(isset($page)):?>
          	<p class="well"><strong>Page URL:</strong> <a href="<?php echo $this->config->item('frontend_url')?>pages/view/<?php echo $page['page_slug'] ?>" target="_blank"><?php echo $this->config->item('frontend_url')?>pages/view/<?php echo $page['page_slug'] ?></a></p>
          <?php endif;?>
          <br/>
          <?php if (isset($page)): ?>
          <div id="modal-confirm-delete" class="modal hide fade">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3>Delete Page</h3>
              </div>
              <div class="modal-body">
                <p>You are about to delete this page. Are you sure you want to continue?</p>
              </div>
              <div class="modal-footer">
                <a href="<?php echo base_url()?>pages/delete/<?php echo $page['page_id']?>" class="btn danger">Yes</a>
                <a href="#" class="btn secondary" data-dismiss="modal">No</a>
              </div>
          </div>
          
          <div class="pull-left">
          	<a class="btn btn-danger delete" href="javascript:void(0)">
          		<i class="icon-trash icon-white"></i> Delete
						</a>
          </div>
          <?php endif;?>
          <div class="pull-right">
          	<button class="btn btn-primary" type="submit">Save Changes</button>
          </div>
        </div>
        <input type="hidden" name="type" value="dynamic"/>
     		</form>
   </div><!--/span-->
  </div><!--/row-->

 

</div><!--/.fluid-container-->
<script type="text/javascript">
$(document).on('click', '.delete', function(e) {
	e.preventDefault();
	$('#modal-confirm-delete').modal('show');
});
$(document).ready(function(){

	$('#page_tags').typeahead({
	    source: function(typeahead, query) {
	      var term = $.trim(query.split(',').pop());
	      if (term == '') return [];
	      $.getJSON('<?php echo base_url()?>tags/typeahead', { tag: term }, function(data) {
	        typeahead.process(data);
	      });
	    }
	  , onselect: function(item, previous_items) {
	      terms = previous_items.split(',');
	      terms.pop();
	      terms.push(item);
	      terms.push('');
	      $.each(terms, function(idx, val) { terms[idx] = $.trim(val); });
	      $('#page_tags').val(terms.join(', '));
	    }
	  // Matcher always returns true since there are multiple comma-seperated terms in the input box and the server ensures only matching terms are returned
	  , matcher: function() { return true; }
	  // Autoselect is disabled so that users can enter new tags
	  , autoselect: false
	  });

	CKEDITOR.replace( 'content',
  {
    filebrowserBrowseUrl :'<?php echo $this->config->item('frontend_url')?>assets/ckeditor/filemanager/browser/default/browser.html?Connector=<?php echo $this->config->item('frontend_url')?>assets/ckeditor/filemanager/connectors/php/connector.php',
    filebrowserImageBrowseUrl : '<?php echo $this->config->item('frontend_url')?>assets/ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=<?php echo $this->config->item('frontend_url')?>assets/ckeditor/filemanager/connectors/php/connector.php',
    filebrowserFlashBrowseUrl :'<?php echo $this->config->item('frontend_url')?>assets/ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=<?php echo $this->config->item('frontend_url')?>assets/ckeditor/filemanager/connectors/php/connector.php',
  	filebrowserUploadUrl  :'<?php echo $this->config->item('frontend_url')?>assets/ckeditor/filemanager/connectors/php/upload.php?Type=File',
  	filebrowserImageUploadUrl : '<?php echo $this->config->item('frontend_url')?>assets/ckeditor/filemanager/connectors/php/upload.php?Type=Image',
  	filebrowserFlashUploadUrl : '<?php echo $this->config->item('frontend_url')?>assets/ckeditor/filemanager/connectors/php/upload.php?Type=Flash'
  });
	  
});
</script>