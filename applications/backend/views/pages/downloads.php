<div class="container-fluid">
  <div class="row-fluid">
  	<h3>Downloads</h3>
  	<hr/>
  	<div class="span3">
  		<ul class="nav nav-tabs nav-stacked">
  			<li <?php echo !isset($active) ? 'class="active"' : ''; ?>><a href="#">Forms</a></li>
  		</ul>
  	</div>
  	<div class="span8">
      <table class="table table-striped">
      	<thead>
      		<tr>
      			<th>Form ID</th>
      			<th>Form Name</th>
      			<th>Tasks</th>
      		</tr>
      	</thead>
      	<tbody>
      		<?php if(empty($forms)):?>
      		<tr>
      			<td colspan="4">
      				No forms yet.
      			</td>
      		</tr>
      		<?php else:?>
        		<?php foreach($forms as $form):?>
          		<tr>
          			<td><?php echo $form['form_id']?></td>
          			<td><?php echo $form['form_name']?></td>
          			<td>
          			  <a class="btn btn-small view" href="#" id="<?php echo $form['form_id']?>"><i class="icon-list-alt"></i> View Submissions</a>
          				<a class="btn btn-small download" href="#" id="<?php echo $form['form_id']?>"><i class="icon-download"></i> Download</a>
            		</td>
          		</tr>
        		<?php endforeach;?>
      		<?php endif;?>
      	</tbody>
      </table>
      <div class="pagination pull-right">
        <?php echo $this->pagination->create_links(); ?>
      </div>
  	</div>
  </div>
</div>  

<div id="modal-form-view" class="modal hide fade">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h3>Form View</h3>
    </div>
    <div class="modal-body">
    	No form submissions yet.
    	
      <div class="modal-footer">
          <a href="#" class="btn" data-dismiss="modal">Close</a>
       </div>
    </div>
</div> 
<script type="text/javascript">
$(document).on('click', '.view', function(e) {
	e.preventDefault();

	var formId = $(this).attr('id');

	$('#modal-form-view').modal({'show':true,'keyboard':false}).css({
        width: '600px',
        'margin-top': function () {
            return -($(this).height() / 2);
        },
        'margin-left': function () {
            return -($(this).width() / 2);
        }
    });
});

$(document).on('click', '.download', function(e) {
	e.preventDefault();

	var formId = $(this).attr('id');

	$('#modal-form-view').modal({'show':true,'keyboard':false}).css({
        width: '600px',
        'margin-top': function () {
            return -($(this).height() / 2);
        },
        'margin-left': function () {
            return -($(this).width() / 2);
        }
    });
});
</script>