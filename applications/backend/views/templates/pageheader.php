<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container-fluid">
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <a class="brand" style="padding: 6px 0px; margin-left:0" href="http://reardenkillion.com" target="_blank"><img src="<?php echo base_url()?>../assets/img/rk_logo_white.png" width="60%"/></a>
      <div class="btn-group pull-right">
        <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
          <i class="icon-user"></i> <?php echo $username ?>
          <span class="caret"></span>
        </a>
        <ul class="dropdown-menu">
          <li><a href="#">Profile</a></li>
          <li class="divider"></li>
          <li><a href="<?php echo base_url()?>logout">Sign Out</a></li>
        </ul>
      </div>
      <div class="nav-collapse">
        <ul class="nav">
          <li class="<?php echo (!isset($pageActive) || $pageActive == 'pages')  ? 'active' : '' ?>"><a href="<?php echo base_url("/")?>">Pages</a></li>
          <li class="<?php echo (isset($pageActive) && $pageActive == 'articles')  ? 'active' : '' ?>"><a href="<?php echo base_url("articles")?>">Articles</a></li>
          <li class="<?php echo (isset($pageActive) && $pageActive == 'menus')  ? 'active' : '' ?>"><a href="<?php echo base_url("menus")?>">Menu Builder</a></li>
          <li class="<?php echo (isset($pageActive) && $pageActive == 'widgets')  ? 'active' : '' ?> dropdown">
          	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Widgets<b class="caret"></b></a>
          	<ul class="dropdown-menu">
          		<li><a href="<?php echo base_url("widgets")?>">Forms</a></li>
          		<li><a href="<?php echo base_url("widgets/gallery")?>">Gallery</a></li>
          		<li><a href="<?php echo base_url("widgets/advertisements")?>">Advertisements</a></li>
          		<li><a href="<?php echo base_url("widgets/videos")?>">Videos</a></li>
          	</ul>
          </li>
          <li class="<?php echo (isset($pageActive) && $pageActive == 'marketing')  ? 'active' : '' ?>"><a href="<?php echo base_url("analytics")?>">Marketing</a></li>
          <li class="<?php echo (isset($pageActive) && $pageActive == 'downloads')  ? 'active' : '' ?>"><a href="<?php echo base_url("downloads")?>">Downloads</a></li>
          <li class="<?php echo (isset($pageActive) && $pageActive == 'settings')  ? 'active' : '' ?> dropdown">
          	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Settings<b class="caret"></b></a>
          	<ul class="dropdown-menu">
          		<li><a href="<?php echo base_url("settings")?>">General</a></li>
          		<li><a href="<?php echo base_url("settings/sections")?>">Sections</a></li>
          		<!--li><a href="<?php echo base_url("settings/home")?>">Home Page</a></li-->
          	</ul>
          </li>
        </ul>
      </div><!--/.nav-collapse -->
    </div>
  </div>
</div>

<!-- modal-gallery is the modal dialog used for the image gallery -->
  <div id="modal-gallery" class="modal modal-gallery hide fade" data-filter=":odd">
      <div class="modal-header">
          <a class="close" data-dismiss="modal">&times;</a>
          <h3 class="modal-title"></h3>
      </div>
      <div class="modal-body"><div class="modal-image"></div></div>
      <div class="modal-footer">
          <a class="btn modal-download" target="_blank">
              <i class="icon-download"></i>
              <span>View URL</span>
          </a>
          <a class="btn btn-success modal-play modal-slideshow" data-slideshow="5000">
              <i class="icon-play icon-white"></i>
              <span>Slideshow</span>
          </a>
          <a class="btn btn-info modal-prev">
              <i class="icon-arrow-left icon-white"></i>
              <span>Previous</span>
          </a>
          <a class="btn btn-primary modal-next">
              <span>Next</span>
              <i class="icon-arrow-right icon-white"></i>
          </a>
      </div>
  </div>