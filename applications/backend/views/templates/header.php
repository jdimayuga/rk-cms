<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo $title?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
   	<meta name="author" content="">

    <!-- Le styles -->
    <link href="<?=base_url()?>../assets/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }
    </style>
    <link href="<?=base_url()?>../assets/css/bootstrap-responsive.css" rel="stylesheet">
    <link href="<?=base_url()?>../assets/css/datepicker.css" rel="stylesheet">
    <link href="<?=base_url()?>../assets/css/style.css" rel="stylesheet">
    <link href="<?=base_url()?>../assets/css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="stylesheet">
    <link href="<?=base_url()?>../assets/js/google-code-prettify/prettify.css" type="text/css" rel="stylesheet" />
		<link rel="stylesheet" href="<?=base_url()?>../assets/css/bootstrap-image-gallery.min.css">
		<link href="<?=base_url()?>../assets/css/jquery.fileupload-ui.css" rel="stylesheet">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script src="<?=base_url()?>../assets/ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="<?=base_url()?>../assets/js/jquery-1.7.2.min.js"></script>
    <script src="<?=base_url()?>../assets/js/jquery-ui-1.8.21.custom.min.js"></script>
    <script src="<?=base_url()?>../assets/js/jquery.zclip.js"></script>
    <script src="<?=base_url()?>../assets/js/bootstrap.js"></script>
    <script src="<?=base_url()?>../assets/js/bootstrap-datepicker.js"></script>
    <script src="<?=base_url()?>../assets/js/google-code-prettify/prettify.js"></script>
    <script type="text/javascript">
    	$(document).ready(function(){
    		$(".accordion-heading").collapse();
    	});
    </script>
    
    <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
    <script src="<?php echo base_url()?>../assets/js/jquery.ui.widget.js"></script>
    <!-- The Templates plugin is included to render the upload/download listings -->
    <script src="<?php echo base_url()?>../assets/js/tmpl.min.js"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="<?php echo base_url()?>../assets/js/load-image.min.js"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="<?php echo base_url()?>../assets/js/canvas-to-blob.min.js"></script>
    <script src="<?php echo base_url()?>../assets/js/bootstrap-image-gallery.js"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="<?php echo base_url()?>../assets/js/jquery.iframe-transport.js"></script>
    <!-- The basic File Upload plugin -->
    <script src="<?php echo base_url()?>../assets/js/jquery.fileupload.js"></script>
    <!-- The File Upload file processing plugin -->
    <script src="<?php echo base_url()?>../assets/js/jquery.fileupload-fp.js"></script>
    <!-- The File Upload user interface plugin -->
    <script src="<?php echo base_url()?>../assets/js/jquery.fileupload-ui.js"></script>
    <!-- The localization script -->
    <script src="<?php echo base_url()?>../assets/js/locale.js"></script>
    
   
    
    
  </head>
  <body>
