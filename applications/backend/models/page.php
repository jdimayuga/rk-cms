<?php
class Page extends CI_Model 
{
  
  public function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
  }
  
  public function getPages($type) 
  {
    $query = $this->db->get_where('pages',array('page_type'=>$type));
    return $query->result_array();
  }
  
  public function hasPageBySlug()
  {
    $slug = url_title($this->input->post('title'));
    $pageId = $this->input->post('id');
    $this->db->from('pages');
    $this->db->where('page_slug = "'.$slug.'"');
    if($pageId) {
      $this->db->where('page_id != "'.$pageId.'"');
    }
    $this->db->limit(1);
    
    $query = $this->db->get();
    if($query->num_rows() == 1)
    {
      return $query->result();
    }
    else{
      return false;
    }
  }
  
  public function getPageBySlug($slug)
  {
    $this->db->from('pages');
    $this->db->where('page_slug = "'.$slug.'"');
    $this->db->limit(1);
    
    $query = $this->db->get();
    return $query->row_array();
  }
  
  public function getPageById()
  {
    $id = $this->input->post('id');
    $this->db->from('pages');
    $this->db->where('page_id = "'.$id.'"');
    $this->db->limit(1);
    
    $query = $this->db->get();
    return $query->row_array();
  }
  
  public function createPage()
  {
    $slug = url_title($this->input->post('title'));
    $data = array('page_name'=>$this->input->post('title'), 
    							'page_slug'=>$slug, 
    							'page_content'=>$this->input->post('content'),
    							'page_seo'=>$this->input->post('seo'), 
                  'page_tags'=>$this->input->post('tags'),
    							'page_visible'=>($this->input->post('visible')) ? '1' : '0',  
    							'page_type'=>$this->input->post('type'));
    
    return $this->db->insert('pages',$data);
  }
  
  public function updatePage()
  {
    $id = $this->input->post('id');
    $slug = url_title($this->input->post('title'));
    $data = array('page_name'=>$this->input->post('title'), 
    							'page_slug'=>$slug, 
    							'page_content'=>$this->input->post('content'),
    							'page_seo'=>$this->input->post('seo'), 
                  'page_tags'=>$this->input->post('tags'),
    							'page_visible'=>($this->input->post('visible')) ? '1' : '0',  
    							'page_type'=>$this->input->post('type'));
    
    $this->db->where('page_id', $id);
    $this->db->update('pages', $data); 
  }
  
  public function deletePage($pageId)
  {
    return $this->db->delete('pages', array('page_id' => $pageId)); 
  }
 
}
?>