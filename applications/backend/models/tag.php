<?php 
class Tag extends CI_Model 
{
  public function getTag($keyword)
  {
    $this->db->select('tag_name');
    $this->db->from('tags');
    $this->db->where('tag_name LIKE "'.$keyword.'%"');
    $this->db->order_by("tag_name", "asc"); 
    $query = $this->db->get();
    
    $return = array();
    foreach ($query->result() as $row)
    {
        array_push($return, $row->tag_name);
    }
    return $return;
  }
  
  public function saveTag()
  {
    $tags = $this->input->post('tags');
    $tagsArray = explode(',',$tags);
    foreach($tagsArray as $tag) {
      if(!empty($tag)) {
        $data = array('tag_name'=>trim($tag));
        $insert_query = $this->db->insert_string('tags', $data);
        $insert_query = str_replace('INSERT INTO','INSERT IGNORE INTO',$insert_query);
        $this->db->query($insert_query);
      }  
    }
    
    
  }
}
?>
