<?php
class Images extends CI_Model
{
  public function getAll($num, $offset)
  {
    $this->db->from('images')->order_by('image_id', 'asc');
    if(!empty($num) && !empty($offset))
    {
      $this->db->limit($num, $offset);
    }
    $query = $this->db->get();
    return $query->result_array();
  }

  public function count()
  {
    return $this->db->count_all('images');
  }

  public function getById($id)
  {
    $query = $this->db->get_where('images',array('image_id'=>$id));
    return $query->row_array();
  }
  
  public function getByGalleryId($galleryId)
  {
    $query = $this->db->get_where('images',array('gallery_id'=>$galleryId));
    return $query->result_array();
  }

  public function delete($id)
  {
    return $this->db->delete('images', array('image_id' => $id));
  }

  public function save( $file, $thumb, $galleryId )
  {
    $data = array('image_file'=>$file, 'thumb_file'=>$thumb, 'gallery_id'=>$galleryId);
    $this->db->insert('images',$data);
    return $this->db->insert_id();
  }
  
  public function save_caption()
  {
    $caption = $this->input->post('caption');
    $id = $this->input->post('imageId');
    $data = array('image_caption'=>$caption);
    $this->db->where('image_id', $id);
    $this->db->update('images', $data);
  }

}
?>