<?php
class Gallery extends CI_Model
{
  public function getAll($num, $offset)
  {
    $this->db->from('gallery')->order_by('gallery_id', 'asc');
    if(!empty($num) && !empty($offset))
    {
      $this->db->limit($num, $offset);
    }
    $query = $this->db->get();
    return $query->result_array();
  }

  public function count()
  {
    return $this->db->count_all('gallery');
  }

  public function getById($id)
  {
    $query = $this->db->get_where('gallery',array('gallery_id'=>$id));
    return $query->row_array();
  }

  public function delete()
  {
    $gallery_id = $this->input->post('id');

    $query = $this->db->get_where('images',array('gallery_id'=>$gallery_id));
    $images = $query->result_array();

    foreach($images as $image)
    {
      $success = unlink("./assets/uploads/gallery/" . $image['image_file']);
      $success_th = unlink("./assets/uploads/gallery/thumb/" . $image['thumb_file']);
      $this->db->delete('images', array('image_id' => $image['image_id']));
    }

    return $this->db->delete('gallery', array('gallery_id' => $gallery_id));
  }

  public function save()
  {
    $id = $this->input->post('id');

    $data = array('gallery_name'=>$this->input->post('name'),
    							'gallery_desc'=>$this->input->post('gallery_desc'),
    							'gallery_section'=>$this->input->post('section'),
    							'gallery_article_id'=>$this->input->post('article'));
    if(empty($id))
    {
      $this->db->insert('gallery',$data);
      return $this->db->insert_id();
    }
    else
    {
      $this->db->where('gallery_id', $id);
      $this->db->update('gallery', $data);
      return $id;
    }
  }

  public function uploadCovers($galleryId)
  {
    $config['upload_path'] = '../assets/uploads/gallery_covers/';
    $config['allowed_types'] = 'gif|jpg|png';
    $config['max_size']	= '2048';

    $this->load->library('upload', $config);

    if ( $this->upload->do_upload('cover_large'))
    {

      $info = $this->upload->data();

      $configThumb['image_library'] = 'gd2';
      $configThumb['source_image'] = '';
      $configThumb['create_thumb'] = TRUE;
      $configThumb['maintain_ratio'] = TRUE;
      $configThumb['width'] = 315;
      $configThumb['height'] = 345;
      $configThumb['source_image'] = $info['full_path'];
      $configThumb['new_image'] = '../assets/uploads/gallery_covers/large/'.$info['raw_name'].$info['file_ext'];
      $this->load->library('image_lib');
      $this->image_lib->initialize($configThumb);
      $this->image_lib->resize();

      $data = array('gallery_cover_large'=>$info['raw_name'],
                    'gallery_cover_large_ext'=>$info['file_ext']);

      $this->db->where('gallery_id', $galleryId);
      $this->db->update('gallery', $data);
    }

    if ( $this->upload->do_upload('cover_small'))
    {

      $info = $this->upload->data();

      $configThumb['image_library'] = 'gd2';
      $configThumb['source_image'] = '';
      $configThumb['create_thumb'] = TRUE;
      $configThumb['maintain_ratio'] = TRUE;
      $configThumb['width'] = 154;
      $configThumb['height'] = 138;
      $configThumb['source_image'] = $info['full_path'];
      $configThumb['new_image'] = '../assets/uploads/gallery_covers/small/'.$info['raw_name'].$info['file_ext'];
      $this->load->library('image_lib');
      $this->image_lib->initialize($configThumb);
      $this->image_lib->resize();

      $data = array('gallery_cover_small'=>$info['raw_name'],
                    'gallery_cover_small_ext'=>$info['file_ext']);

      $this->db->where('gallery_id', $galleryId);
      $this->db->update('gallery', $data);
    }
  }

}
?>