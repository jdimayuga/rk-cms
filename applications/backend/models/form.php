<?php
class Form extends CI_Model
{
  public function getAll($num, $offset)
  {
    $this->db->from('forms')->order_by('form_id', 'asc');
    if(!empty($num) && !empty($offset))
    {
      $this->db->limit($num, $offset);
    }
    $query = $this->db->get();
    return $query->result_array();
  }

  public function count()
  {
    return $this->db->count_all('forms');
  }

  public function getById($id)
  {
    $query = $this->db->get_where('forms',array('form_id'=>$id));
    return $query->row_array();
  }

  public function delete()
  {
    $this->input->post('id');
    return $this->db->delete('forms', array('form_id' => $this->input->post('id')));
  }

  public function save()
  {
    $id = $this->input->post('id');
    
    $data = array('form_name'=>$this->input->post('name'),
    							'form_content'=>$this->input->post('content'),
      						'form_scripts'=>$this->input->post('scripts'),
      						'form_post_message'=>$this->input->post('post_message'));
    if(empty($id))
    {
      $this->db->insert('forms',$data);
      return $this->db->insert_id();
    }
    else
    {
      $this->db->where('form_id', $id);
      $this->db->update('forms', $data);
      return $id;
    }
  }

}
?>