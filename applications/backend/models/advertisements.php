<?php
class Advertisements extends CI_Model
{
  public function getAll($num, $offset)
  {
    $this->db->from('advertisements')->order_by('ad_id', 'asc');
    if(!empty($num) && !empty($offset))
    {
      $this->db->limit($num, $offset);
    }
    $query = $this->db->get();
    return $query->result_array();
  }

  public function count()
  {
    return $this->db->count_all('advertisements');
  }

  public function getById($id)
  {
    $query = $this->db->get_where('advertisements',array('ad_id'=>$id));
    return $query->row_array();
  }

  public function delete()
  {
    $this->input->post('id');
    return $this->db->delete('advertisements', array('ad_id' => $this->input->post('id')));
  }

  public function save()
  {
    $id = $this->input->post('id');
    
    $data = array('ad_name'=>$this->input->post('name'),
    							'ad_type'=>$this->input->post('type'),
      						'ad_effects'=>$this->input->post('effects'),
    							'ad_flash_script'=>$this->input->post('flash_script'),
    							'ad_area'=>$this->input->post('area'));
    if(empty($id))
    {
      $this->db->insert('advertisements',$data);
      return $this->db->insert_id();
    }
    else
    {
      $this->db->where('ad_id', $id);
      $this->db->update('advertisements', $data);
      return $id;
    }
  }
  
  public function uploadFlash($advertisement_id)
  {
    $config['upload_path'] = '../assets/uploads/flash/';
    $config['allowed_types'] = 'swf|flv';
    $config['max_size']	= '2048';

    $this->load->library('upload', $config);

    if ( $this->upload->do_upload('flash'))
    {
      $info = $this->upload->data();
      $data = array('ad_flash_file'=>$info['raw_name'].$info['file_ext']);

      $this->db->where('ad_id', $advertisement_id);
      return $this->db->update('advertisements', $data);
    }
    else
    {
      return false;
    }
  }

}
?>