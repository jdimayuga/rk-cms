<?php
class Article extends CI_Model
{
  public function getArticles($num, $offset)
  {
    $this->db->from('articles')->order_by('date_created', 'desc');
    if(!empty($num) && !empty($offset))
    {
      $this->db->limit($num, $offset);
    }
    else if(!empty($num)){
      $this->db->limit($num);
    }
    $query = $this->db->get();
    return $query->result_array();
  }

  public function getAllActiveArticles()
  {
    $this->db->from('articles')->where('article_display','1')->order_by('article_title', 'asc');
    $query = $this->db->get();
    return $query->result_array();
  }

  public function getArticleCount()
  {
    return $this->db->count_all('articles');
  }

  public function getArticleById($id)
  {
    $query = $this->db->get_where('articles',array('article_id'=>$id));
    return $query->row_array();
  }

  public function countFeatured($type)
  {
    $this->db->where($type, '1');
    $this->db->from('articles');
    return $this->db->count_all_results();
  }

  public function toggleFeatured($id, $type, $status)
  {
    $data = array($type=>$status);
    $this->db->where('article_id', $id);
    $this->db->update('articles', $data);
  }

  public function saveArticle()
  {
    $id = $this->input->post('id');

    /*$inputdate = $this->input->post('date');
     $strf = strptime($inputdate,'%m/%d/%Y');
     $ymd = sprintf('%04d-%02d-%02d',$strf['tm_year'] + 1900, $strf['tm_mon'] + 1, $strf['tm_mday']);
     $date = new DateTime($ymd);*/

    $date = DateTime::createFromFormat('m/d/Y', $this->input->post('date'));

    $order = $this->input->post('order');
    $data = array('article_title'=>$this->input->post('title'),
      							'article_summary'=>$this->input->post('summary'), 
      							'article_content'=>$this->input->post('content'),
      							'article_seo'=>$this->input->post('seo'), 
                    'article_tags'=>$this->input->post('tags'),
    								'article_author'=>$this->input->post('author'),
    								'article_section'=>$this->input->post('section'),
      							'article_date'=>$date->format('Y-m-d'),
      							'article_display'=>($this->input->post('visible')) ? '1' : '0',
      							'article_order'=>(empty($order)) ? 0 : $order);
    if(empty($id))
    {
      $this->db->insert('articles',$data);
      return $this->db->insert_id();
    }
    else
    {
      $this->db->where('article_id', $id);
      $this->db->update('articles', $data);
      return $id;
    }
  }

  public function uploadImage($articleId)
  {
    $config['upload_path'] = '../assets/uploads/articles/';
    $config['allowed_types'] = 'gif|jpg|png';
    $config['max_size']	= '2048';

    $this->load->library('upload', $config);

    if ( $this->upload->do_upload('image'))
    {

      $info = $this->upload->data();

      $configThumb['image_library'] = 'gd2';
      $configThumb['source_image'] = '';
      $configThumb['create_thumb'] = TRUE;
      $configThumb['maintain_ratio'] = TRUE;
      $configThumb['width'] = 651;
      $configThumb['height'] = 305;
      $configThumb['source_image'] = $info['full_path'];
      $configThumb['new_image'] = '../assets/uploads/articles/thumb/'.$info['raw_name'].$info['file_ext'];
      $this->load->library('image_lib');
      $this->image_lib->initialize($configThumb);
      $this->image_lib->resize();

      $configThumb['width'] = 315;
      $configThumb['height'] = 148;
      $configThumb['new_image'] = '../assets/uploads/articles/thumb_2/'.$info['raw_name'].$info['file_ext'];
      $this->load->library('image_lib');
      $this->image_lib->initialize($configThumb);
      $this->image_lib->resize();

      $configThumb['width'] = 147;
      $configThumb['height'] = 68;
      $configThumb['new_image'] = '../assets/uploads/articles/thumb_3/'.$info['raw_name'].$info['file_ext'];
      $this->load->library('image_lib');
      $this->image_lib->initialize($configThumb);
      $this->image_lib->resize();

      $data = array('article_image'=>$info['raw_name'],
                    'article_image_ext'=>$info['file_ext'],
                    'article_image_thumb'=>$info['raw_name'].'_thumb');

      $this->db->where('article_id', $articleId);
      return $this->db->update('articles', $data);
    }
    else
    {
      return false;
    }
  }

  public function delete()
  {
    return $this->db->delete('articles', array('article_id' => $this->input->post('id')));
  }
}