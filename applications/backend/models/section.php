<?php
class Section extends CI_Model {

  public function getAll($num, $offset)
  {
    $this->db->from('section')->order_by('section_id', 'asc');
    if(!empty($num) && !empty($offset))
    {
      $this->db->limit($num, $offset);
    }
    else if(!empty($num)){
      $this->db->limit($num);
    }
    $query = $this->db->get();
    return $query->result_array();
  }

  public function getAllOrderByName()
  {
    $this->db->from('section')->order_by('section_name', 'asc');
    $query = $this->db->get();
    return $query->result_array();
  }

  public function count()
  {
    return $this->db->count_all('section');
  }

  public function count_featured()
  {
    $this->db->where('featured', '1');
    $this->db->from('section');
    return $this->db->count_all_results();
  }

  public function toggle_featured($id, $status)
  {
    $data = array('featured'=>$status);
    $this->db->where('section_id', $id);
    $this->db->update('section', $data);
  }

  public function getById($id)
  {
    $query = $this->db->get_where('section',array('section_id'=>$id));
    return $query->row_array();
  }

  public function delete()
  {
    $section_id = $this->input->post('id');
    return $this->db->delete('section', array('section_id' => $section_id));
  }

  public function save()
  {
    $id = $this->input->post('id');

    $data = array('section_name'=>$this->input->post('name'),
                  'menu_bar_color'=>$this->input->post('color'),
                  'section_slug'=>url_title($this->input->post('name')));
    if(empty($id))
    {
      $this->db->insert('section',$data);
      return $this->db->insert_id();
    }
    else
    {
      $this->db->where('section_id', $id);
      $this->db->update('section', $data);
      return $id;
    }
  }

  public function uploadImage($section_id)
  {
    $config['upload_path'] = '../assets/uploads/logo/';
    $config['allowed_types'] = 'gif|jpg|png';
    $config['max_size']	= '2048';

    $this->load->library('upload', $config);

    if ( $this->upload->do_upload('image'))
    {
      $info = $this->upload->data();
      $data = array('section_logo'=>$info['raw_name'].$info['file_ext']);

      $this->db->where('section_id', $section_id);
      return $this->db->update('section', $data);
    }
    else
    {
      return false;
    }
  }

}
?>