<?php
class Video extends CI_Model
{
  public function getAll($num, $offset)
  {
    $this->db->from('videos')->order_by('video_id', 'asc');
    if(!empty($num) && !empty($offset))
    {
      $this->db->limit($num, $offset);
    }
    $query = $this->db->get();
    return $query->result_array();
  }

  public function count()
  {
    return $this->db->count_all('videos');
  }

  public function getById($id)
  {
    $query = $this->db->get_where('videos',array('video_id'=>$id));
    return $query->row_array();
  }

  public function delete()
  {
    $this->input->post('id');
    return $this->db->delete('videos', array('video_id' => $this->input->post('id')));
  }

  public function save()
  {
    $id = $this->input->post('id');
    
    $data = array('video_title'=>$this->input->post('title'),
    							'video_caption'=>$this->input->post('caption'),
      						'video_embed'=>$this->input->post('embed'),
    							'video_section'=>$this->input->post('section'));
    if(empty($id))
    {
      $this->db->insert('videos',$data);
      return $this->db->insert_id();
    }
    else
    {
      $this->db->where('video_id', $id);
      $this->db->update('videos', $data);
      return $id;
    }
  }

}
?>