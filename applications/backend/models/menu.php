<?php
class Menu extends CI_Model
{

  public function getAll($num, $offset)
  {
    $this->db->select('menus.*', FALSE);
    $this->db->select('parent.menu_name as parent_menu_name', FALSE);
    $this->db->from('menus')->order_by('menu_name', 'asc');
    if(!empty($num) && !empty($offset))
    {
      $this->db->limit($num, $offset);
    }
    else if(!empty($num)){
      $this->db->limit($num);
    }
    $this->db->join('menus AS parent', 'menus.menu_parent = parent.menu_id', 'left');
    $query = $this->db->get();
    return $query->result_array();
  }

  public function getAllParents()
  {
    $this->db->select('menu_name');
    $this->db->select('menu_id');
    $this->db->distinct();
    $this->db->from('menus')->order_by('menu_name', 'asc');
    $query = $this->db->get();
    $data=array();
    $data[0] = 'Main';
    foreach ($query->result() as $row)
    {
      $data[$row->menu_id] = $row->menu_name;
    }
    return $data;


  }

  public function count()
  {
    return $this->db->count_all('menus');
  }

  public function getById($id)
  {
    $query = $this->db->get_where('menus',array('menu_id'=>$id));
    return $query->row_array();
  }

  public function delete()
  {
    return $this->db->delete('menus', array('menu_id' => $this->input->post('id')));
  }

  public function save()
  {
    $id = $this->input->post('id');
    $sort = $this->input->post('sort');
    $data = array('menu_name'=>$this->input->post('name'),
      							'menu_sort'=>empty($sort) ? 0 : $sort, 
      							'page_url'=>$this->input->post('path'),
      							'menu_parent'=>$this->input->post('parent'));
    if(empty($id))
    {
      $this->db->insert('menus',$data);
      return $this->db->insert_id();
    }
    else
    {
      $this->db->where('menu_id', $id);
      $this->db->update('menus', $data);
      return $id;
    }
  }

}
?>