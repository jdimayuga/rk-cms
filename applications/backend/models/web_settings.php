<?php
class Web_Settings extends CI_Model
{
  public function getAll($num, $offset)
  {
    $this->db->from('settings')->order_by('settings_id', 'asc');
    if(!empty($num) && !empty($offset))
    {
      $this->db->limit($num, $offset);
    }
    $query = $this->db->get();
    return $query->result_array();
  }
  
  public function getOne()
  {
    $this->db->from('settings')->limit(1, 0);
    $query = $this->db->get();
    return $query->row_array();
  }

  public function count()
  {
    return $this->db->count_all('settings');
  }

  public function getById($id)
  {
    $query = $this->db->get_where('settings',array('settings_id'=>$id));
    return $query->row_array();
  }

  public function delete()
  {
    $settings_id = $this->input->post('id');



    return $this->db->delete('settings', array('settings_id' => $settings_id));
  }

  public function save()
  {
    $id = $this->input->post('id');

    $data = array('title'=>$this->input->post('title'),
                  'start_scripts'=>$this->input->post('start_scripts'),
    							'end_scripts'=>$this->input->post('end_scripts'));
    if(empty($id))
    {
      $this->db->insert('settings',$data);
      return $this->db->insert_id();
    }
    else
    {
      $this->db->where('settings_id', $id);
      $this->db->update('settings', $data);
      return $id;
    }
  }

}
?>