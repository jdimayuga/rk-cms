<?php
class Ad_Images extends CI_Model
{
  public function getAll($num, $offset)
  {
    $this->db->from('ad_images')->order_by('ad_image_id', 'asc');
    if(!empty($num) && !empty($offset))
    {
      $this->db->limit($num, $offset);
    }
    $query = $this->db->get();
    return $query->result_array();
  }

  public function count()
  {
    return $this->db->count_all('ad_images');
  }

  public function getById($id)
  {
    $query = $this->db->get_where('ad_images',array('ad_image_id'=>$id));
    return $query->row_array();
  }
  
  public function getByAdvertisementId($ad_id)
  {
    $query = $this->db->get_where('ad_images',array('ad_id'=>$ad_id));
    return $query->result_array();
  }

  public function delete($id)
  {
    return $this->db->delete('ad_images', array('ad_image_id' => $id));
  }

  public function save( $file, $thumb, $ad_id )
  {
    $data = array('ad_image_file'=>$file, 'ad_thumb_file'=>$thumb, 'ad_id'=>$ad_id);
    $this->db->insert('ad_images',$data);
    return $this->db->insert_id();
  }

}
?>