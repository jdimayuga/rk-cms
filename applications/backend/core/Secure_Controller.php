<?php
class Secure_Controller extends CI_Controller
{
  public function __construct(){
    parent::__construct();
    $this->load->helper('url');
    if(!$this->session->userdata('logged_in')){
     redirect('login','refresh');
    }
  }
}
?>